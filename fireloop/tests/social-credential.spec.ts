var should    = require('chai').should();
var supertest = require('supertest');
var api       = supertest('http://localhost:3000/api');

describe('SocialCredential unit tests:', () => {
    it('Should create a SocialCredential instance', (done: Function) => {
        api.post('/socialCredentials').send({
            facebookId: 'test'
        }).expect(200, done);
    });
});
