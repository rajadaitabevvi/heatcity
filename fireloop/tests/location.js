'use strict';
var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000/api');

describe('Location relation with LocationFix', function() {
    it('POST location.locationFixes should work.', function(done) {
        var token, fixedId;
        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations')
                .send({
                    location: {
                        type: 'Point',
                        coordinates: [-103.398025, 20.656948]
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createLocation2);
        }

        function createLocation2(err, res) {
            if (err) {
                return done(err);
            }
            fixedId = res.body.id;
            api.post('/locations')
                .send({
                    location: {
                        type: 'Point',
                        coordinates: [-103.398025, 20.656948]
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createLocationFix);
        }

        function createLocationFix(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations/' + res.body.id + '/fixes')
                .send({
                    incorrectId: res.body.id
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });

    it('GET location.locationFixes should work.', function(done) {
        var token, locationId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations')
                .send({
                    location: {
                        type: 'Point',
                        coordinates: [-103.398025, 20.656948]
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createLocationFix);
        }

        function createLocationFix(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations/' + res.body.id + '/fixes')
                .send({
                    incorrectId: res.body.id
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }

        function getLocationFixes(err, res) {
            if (err) {
                return done(err);
            }
            api.get('/locations/' + locationId + '/fixes')
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });

    it('GET locationFix.fixed should work.', function(done) {
        var token;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations')
                .send({
                    location: {
                        type: 'Point',
                        coordinates: [-103.398025, 20.656948]
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createLocationFix);
        }

        function createLocationFix(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations/' + res.body.id + '/fixes')
                .send({
                    incorrectId: res.body.id
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }

        function getLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.get('/locationFixes/' + res.body.id + '/fixed')
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });
});

describe('Location relation with LocationActivity', function() {
    it('PUT locations.activities should work.',
    function(done) {
        var token, userId, locationId, visitId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    userId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations')
                .send({
                    location: {
                        type: 'Point',
                        coordinates: [-103.398025, 20.656948]
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createVisit);
        }

        function createVisit(err, res) {
            if (err) {
                return done(err);
            }
            locationId = res.body.id;
            api.post('/locations/' + locationId + '/visits')
                .send({
                    weather: {
                        temp: 21,
                        status: 'cloudy'
                    },
                    accountId: userId,
                    entry: new Date()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createActivity);
        }

        function createActivity(err, res) {
            if (err) {
                return done(err);
            }
            visitId = res.body.id;
            api.post('/activities')
                .send({
                    name: Date.now()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createLocationActivity);
        }

        function createLocationActivity(err, res) {
            if (err) {
                return done(err);
            }
            api.put('/locations/' + locationId + '/activities/rel/' +
                res.body.id)
                .send({
                    visitId: visitId
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });

    it('GET location.activities should work.', function(done) {
        var token, userId, locationId, visitId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    userId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations')
                .send({
                    location: {
                        type: 'Point',
                        coordinates: [-103.398025, 20.656948]
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createVisit);
        }

        function createVisit(err, res) {
            if (err) {
                return done(err);
            }
            locationId = res.body.id;
            api.post('/locations/' + locationId + '/visits')
                .send({
                    weather: {
                        temp: 21,
                        status: 'cloudy'
                    },
                    accountId: userId,
                    entry: new Date()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createActivity);
        }

        function createActivity(err, res) {
            if (err) {
                return done(err);
            }
            visitId = res.body.id;
            api.post('/activities')
                .send({
                    name: Date.now()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createLocationActivity);
        }

        function createLocationActivity(err, res) {
            if (err) {
                return done(err);
            }
            api.put('/locations/' + locationId + '/activities/rel/' +
                res.body.id)
                .send({
                    visitId: visitId
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, getActivities);
        }

        function getActivities(err, res) {
            if (err) {
                return done(err);
            }
            api.get('/locations/' + locationId + '/activities')
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });

    it('PUT locations.activities' +
        'without visitId should\'nt work.', function(done) {
        var token, userId, locationId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    userId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations')
                .send({
                    location: {
                        type: 'Point',
                        coordinates: [-103.398025, 20.656948]
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createActivity);
        }

        function createActivity(err, res) {
            if (err) {
                return done(err);
            }
            locationId = res.body.id;
            api.post('/activities')
                .send({
                    name: Date.now()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createLocationActivity);
        }

        function createLocationActivity(err, res) {
            if (err) {
                return done(err);
            }
            api.put('/locations/' + locationId + '/activities/rel/' +
                res.body.id)
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(422, inspectError);
        }

        function inspectError(err, res) {
            if (err) return done(err);
            var errorCodes = JSON.parse(res.error.text).error.details.codes;
            errorCodes.should.have.property('visitId');
            done();
        }
    });
});

describe('Location relation with Visit', function() {
    it('POST location.visits should work.', function(done) {
        var token, userId, locationId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    userId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations')
                .send({
                    location: {
                        type: 'Point',
                        coordinates: [-103.398025, 20.656948]
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createVisit);
        }

        function createVisit(err, res) {
            if (err) {
                return done(err);
            }
            locationId = res.body.id;
            api.post('/locations/' + locationId + '/visits')
                .send({
                    weather: {
                        temp: 21,
                        status: 'cloudy'
                    },
                    accountId: userId,
                    entry: new Date()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });

    it('POST location.visits without accountId should\'nt work.',
    function(done) {
        var token, userId, locationId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    userId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations')
                .send({
                    location: {
                        type: 'Point',
                        coordinates: [-103.398025, 20.656948]
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createVisit);
        }

        function createVisit(err, res) {
            if (err) {
                return done(err);
            }
            locationId = res.body.id;
            api.post('/locations/' + locationId + '/visits')
                .send({
                    weather: {
                        temp: 21,
                        status: 'cloudy'
                    },
                    entry: new Date()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(422, inspectError);
        }

        function inspectError(err, res) {
            if (err) return done(err);
            var errorCodes = JSON.parse(res.error.text).error.details.codes;
            errorCodes.should.have.property('accountId');
            done();
        }
    });

    it('GET location.visits should work.', function(done) {
        var token, userId, locationId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    userId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations')
                .send({
                    location: {
                        type: 'Point',
                        coordinates: [-103.398025, 20.656948]
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createVisit);
        }

        function createVisit(err, res) {
            if (err) {
                return done(err);
            }
            locationId = res.body.id;
            api.post('/locations/' + locationId + '/visits')
                .send({
                    weather: {
                        temp: 21,
                        status: 'cloudy'
                    },
                    accountId: userId,
                    entry: new Date()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, getVisits);
        }

        function getVisits(err, res) {
            if (err) {
                return done(err);
            }
            api.get('/locations/' + locationId + '/visits')
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });

    it('GET visit.account should work.', function(done) {
        var token, userId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    userId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations')
                .send({
                    location: {
                        type: 'Point',
                        coordinates: [-103.398025, 20.656948]
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createVisit);
        }

        function createVisit(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/' + userId + '/visits')
                .send({
                    weather: {
                        temp: 21,
                        status: 'cloudy'
                    },
                    entry: new Date(),
                    locationId: res.body.id
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, getAccount);
        }

        function getAccount(err, res) {
            if (err) {
                return done(err);
            }
            api.get('/visits/' + res.body.id + '/account')
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });
});

describe('Activity relation with LocationActivity', function() {
    it('PUT activities.locations should work.',
    function(done) {
        var token, userId, locationId, visitId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    userId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations')
                .send({
                    location: {
                        type: 'Point',
                        coordinates: [-103.398025, 20.656948]
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createVisit);
        }

        function createVisit(err, res) {
            if (err) {
                return done(err);
            }
            locationId = res.body.id;
            api.post('/locations/' + locationId + '/visits')
                .send({
                    weather: {
                        temp: 21,
                        status: 'cloudy'
                    },
                    accountId: userId,
                    entry: new Date()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createActivity);
        }

        function createActivity(err, res) {
            if (err) {
                return done(err);
            }
            visitId = res.body.id;
            api.post('/activities')
                .send({
                    name: Date.now()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createLocationActivity);
        }

        function createLocationActivity(err, res) {
            if (err) {
                return done(err);
            }
            api.put('/activities/' + res.body.id + '/locations/rel/' +
            locationId)
                .send({
                    visitId: visitId
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });

    it('GET activity.locations should work.', function(done) {
        var token, userId, locationId, visitId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    userId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations')
                .send({
                    location: {
                        type: 'Point',
                        coordinates: [-103.398025, 20.656948]
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createVisit);
        }

        function createVisit(err, res) {
            if (err) {
                return done(err);
            }
            locationId = res.body.id;
            api.post('/locations/' + locationId + '/visits')
                .send({
                    weather: {
                        temp: 21,
                        status: 'cloudy'
                    },
                    accountId: userId,
                    entry: new Date()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createActivity);
        }

        function createActivity(err, res) {
            if (err) {
                return done(err);
            }
            visitId = res.body.id;
            api.post('/activities')
                .send({
                    name: Date.now()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createLocationActivity);
        }

        function createLocationActivity(err, res) {
            if (err) {
                return done(err);
            }
            api.put('/activities/' + res.body.id + '/locations/rel/' +
            locationId)
                .send({
                    visitId: visitId
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, getLocation);
        }

        function getLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.get('/activities/' + res.body.activityId + '/locations')
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });

    it('PUT activities.locations without' +
    'visitId should\'nt work.', function(done) {
        var token, userId, locationId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    userId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations')
                .send({
                    location: {
                        type: 'Point',
                        coordinates: [-103.398025, 20.656948]
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createActivity);
        }

        function createActivity(err, res) {
            if (err) {
                return done(err);
            }
            locationId = res.body.id;
            api.post('/activities')
                .send({
                    name: Date.now()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createLocationActivity);
        }

        function createLocationActivity(err, res) {
            if (err) {
                return done(err);
            }
            api.put('/activities/' + res.body.id + '/locations/rel/' +
            locationId)
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(422, done);
        }

        function inspectError(err, res) {
            if (err) return done(err);
            var errorCodes = JSON.parse(res.error.text).error.details.codes;
            errorCodes.should.have.property('visitId');
            done();
        }
    });
});

describe('Activity relation with ActivityCategory', function() {
    it('POST activityCategory.activities should work.', function(done) {
        var token, userId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    userId = res.body.userId;
                    createActivityCategory(err, res);
                });
        }

        function createActivityCategory(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/activityCategories')
                .send({
                    name: Date.now()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createActivity);
        }

        function createActivity(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/activityCategories/' + res.body.id + '/activities')
                .send({
                    name: Date.now()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });

    it('GET activityCategory.activities should work.', function(done) {
        var token, userId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    userId = res.body.userId;
                    createActivityCategory(err, res);
                });
        }

        function createActivityCategory(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/activityCategories')
                .send({
                    name: Date.now()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createActivity);
        }

        function createActivity(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/activityCategories/' + res.body.id + '/activities')
                .send({
                    name: Date.now()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, getActivityCategory);
        }

        function getActivityCategory(err, res) {
            if (err) {
                return done(err);
            }
            api.get('/activities/' + res.body.id + '/activityCategory')
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });
});
