var should    = require('chai').should();
var supertest = require('supertest');
var api       = supertest('http://localhost:3000/api');

describe('Device unit tests:', () => {
    it('Should create a Device instance', (done: Function) => {
        api.post('/devices').send({
            uuid: 'test',
            deletedAt: 'Mon Jan 30 2017 09:53:26 GMT-0600 (CST)'
        }).expect(200, done);
    });
});
