'use strict';
var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000/api');

describe('FavoriteListFollower relations', function() {
    describe('FavoriteListFollower belongs to Account', function() {
        it('error if GET favoriteListFollower.account dont work',
        function(done) {
            var account, secondAccount, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, createSecondAccount);

            function createSecondAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts')
                .send({email: Date.now() + '@test2.com', password: '123'})
                .expect(200, loginAccount);
            }

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                secondAccount = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavoriteListFollower);
            }

            function createFavoriteListFollower(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/favoriteLists/' + res.body.id + '/followers')
                .send({
                    accountId: secondAccount.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getAccountFromFavoriteListFollower);
            }

            function getAccountFromFavoriteListFollower(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('accountId');
                api.get('/favoriteListFollowers/' + res.body.id + '/account')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
    describe('FavoriteListFollower belongs to FavoriteList', function() {
        it('error if GET favoriteListFollower.favoriteList dont work',
        function(done)  {
            var account, secondAccount, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, createSecondAccount);

            function createSecondAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts')
                .send({email: Date.now() + '@test2.com', password: '123'})
                .expect(200, loginAccount);
            }

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                secondAccount = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavoriteListFollower);
            }

            function createFavoriteListFollower(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/favoriteLists/' + res.body.id + '/followers')
                .send({
                    accountId: secondAccount.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getFavoriteListFromFavoriteListFollower);
            }

            function getFavoriteListFromFavoriteListFollower(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('favoriteListId');
                api.get(
                    '/favoriteListFollowers/' + res.body.id + '/favoriteList'
                )
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
});
