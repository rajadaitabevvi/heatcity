'use strict';
var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000/api');

describe('Account relation with ExternalFriend', function() {
    it('POST account.externalFriends should work.', function(done) {
        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, createExternalFriend);
        }

        function createExternalFriend(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/' + res.body.userId + '/externalFriends')
                .send({
                    provider: Date.now()
                })
                .set({'Authorization': res.body.id})
                .query({'access_token': res.body.id})
                .expect(200, done);
        }
    });

    it('GET account.externalFriends should work.', function(done) {
        var accountId;
        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, createExternalFriend);
        }

        function createExternalFriend(err, res) {
            if (err) {
                return done(err);
            }
            accountId = res.body.userId;
            api.post('/accounts/' + accountId + '/externalFriends')
                .send({
                    provider: Date.now()
                })
                .set({'Authorization': res.body.id})
                .query({'access_token': res.body.id})
                .expect(200, getExternalFriends);
        }

        function getExternalFriends(err, res) {
            if (err) {
                return done(err);
            }
            api.get('/accounts/' + accountId + '/externalFriends')
                .set({'Authorization': res.request.qs.access_token})
                .query({'access_token': res.request.qs.access_token})
                .expect(200, done);
        }
    });

    it('GET externalFriend.account should work', function(done) {
        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, createExternalFriend);
        }

        function createExternalFriend(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/' + res.body.userId + '/externalFriends')
                .send({
                    provider: Date.now()
                })
                .set({'Authorization': res.body.id})
                .query({'access_token': res.body.id})
                .expect(200, getAccount);
        }

        function getAccount(err, res) {
            if (err) {
                return done(err);
            }
            api.get('/externalFriends/' + res.body.id + '/account')
                .set({'Authorization': res.request.qs.access_token})
                .query({'access_token': res.request.qs.access_token})
                .expect(200, done);
        }
    });
});

describe('Account relation with Friend', function() {
    it('PUT accounts.friends should work', function(done) {
        var friendId;
        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, function(err, res) {
                if (err) {
                    return done(err);
                }
                friendId = res.body.id;
                createAccount(null, res);
            });

        function createAccount(err, res) {
            api.post('/accounts')
                .send({
                    email: Date.now() + '@test.com',
                    password: '123'
                })
                .expect(200, login);
        }

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, createFriend);
        }

        function createFriend(err, res) {
            if (err) {
                return done(err);
            }
            api.put('/accounts/' + res.body.userId + '/friends/rel/' + friendId)
                .set({'Authorization': res.body.id})
                .query({'access_token': res.body.id})
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    res.body.friendId.should.equal(friendId);
                    done();
                });
        }
    });

    it('GET account.friends should work', function(done) {
        var friendId;
        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, function(err, res) {
                if (err) {
                    return done(err);
                }
                friendId = res.body.id;
                createAccount(null, res);
            });

        function createAccount(err, res) {
            api.post('/accounts')
                .send({
                    email: Date.now() + '@test.com',
                    password: '123'
                })
                .expect(200, login);
        }

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, createFriendRelation);
        }

        function createFriendRelation(err, res) {
            if (err) {
                return done(err);
            }
            api.put('/accounts/' + res.body.userId + '/friends/rel/' + friendId)
                .set({'Authorization': res.body.id})
                .query({'access_token': res.body.id})
                .expect(200, getAccountFriendRelation);
        }

        function getAccountFriendRelation(err, res) {
            if (err) {
                return done(err);
            }
            api.get('/accounts/' + res.body.accountId + '/friends/')
                .set({'Authorization': res.request.qs.access_token})
                .query({'access_token': res.request.qs.access_token})
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    res.body[0].id.should.equal(friendId);
                    done();
                });
        }
    });
});

describe('Account relation with ChatChannel:', function() {
    it('POST account.chatChannels should work.', function(done) {
        var token, accountId;
        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    accountId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            api.post('/locations')
                .send({
                    location: {
                        lat: 20.656948,
                        lng: -103.398025
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createChatChannel);
        }

        function createChatChannel(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/' + accountId + '/chatChannels')
                .send({
                    name: Date.now(),
                    description: Date.now(),
                    locationId: res.body.id
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });

    it('POST account.messages should work.', function(done) {
        var token, accountId;
        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    accountId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            api.post('/locations')
                .send({
                    location: {
                        lat: 20.656948,
                        lng: -103.398025
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createChatChannel);
        }

        function createChatChannel(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/' + accountId + '/chatChannels')
                .send({
                    name: Date.now(),
                    description: Date.now(),
                    locationId: res.body.id
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createMessage);
        }

        function createMessage(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/' + accountId + '/messages')
                .send({
                    name: Date.now(),
                    description: Date.now(),
                    chatChannelId: res.body.id
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });

    it('POST account.chatChannels without locationId should\'nt work',
    function(done) {
        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, createChatChannel);
        }

        function createChatChannel(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/' + res.body.userId + '/chatChannels')
                .send({
                    name: Date.now(),
                    description: Date.now()
                })
                .set({'Authorization': res.body.id})
                .query({'access_token': res.body.id})
                .expect(422, inspectError);
        }

        function inspectError(err, res) {
            if (err) return done(err);
            var errorCodes = JSON.parse(res.error.text).error.details.codes;
            errorCodes.should.have.property('locationId');
            done();
        }
    });

    it('POST account.messages without chatChannelId should\'nt work',
    function(done) {
        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, createMessage);
        }

        function createMessage(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/' + res.body.userId + '/messages')
                .send({
                    name: Date.now(),
                    description: Date.now()
                })
                .set({'Authorization': res.body.id})
                .query({'access_token': res.body.id})
                .expect(422, inspectError);
        }

        function inspectError(err, res) {
            if (err) return done(err);
            var errorCodes = JSON.parse(res.error.text).error.details.codes;
            errorCodes.should.have.property('chatChannelId');
            done();
        }
    });
});

describe('Account relation with Visit:', function() {
    it('POST account.visits should work.', function(done) {
        var token, userId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    userId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/locations')
                .send({
                    location: {
                        type: 'Point',
                        coordinates: [-103.398025, 20.656948]
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createVisit);
        }

        function createVisit(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/' + userId + '/visits')
                .send({
                    weather: {
                        temp: 21,
                        status: 'cloudy'
                    },
                    entry: new Date(),
                    locationId: res.body.id
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });

    it('POST account.visit without locationId should\'nt work.',
    function(done) {
        var token, userId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, createVisit);
        }

        function createVisit(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/' + res.body.userId + '/visits')
                .send({
                    entry: new Date()
                })
                .set({'Authorization': res.body.id})
                .query({'access_token': res.body.id})
                .expect(500, done);
        }
    });
});

describe('Account relation with Establishment:', function() {
    it('error if POST account.establishments dont work', function(done) {
        var account, token;
        api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

        function loginAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            account = res.body;
            api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
        }

        function createLocation(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            token = res.body;
            api.post('/locations')
                .send({
                    location: {
                        lat: 1,
                        lng: 2
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
        }

        function createEstablishment(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/accounts/' + account.id + '/establishments')
                .send({
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for all the relations of' +
                        'estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
        }
    });

    it('error if GET account.establishments dont work', function(done) {
        var account, token;
        api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

        function loginAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            account = res.body;
            api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
        }

        function createLocation(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            token = res.body;
            api.post('/locations')
                .send({
                    location: {
                        lat: 1,
                        lng: 2
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
        }

        function createEstablishment(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/accounts/' + account.id + '/establishments')
                .send({
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for all the relations of' +
                        'estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getEstablisments);
        }

        function getEstablisments(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.get('/accounts/' + account.id + '/establishments')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
        }
    });
});

describe('Account relation with Event:', function() {
    it('error if POST account.events dont work', function(done) {
        var account, token;
        api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

        function loginAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            account = res.body;
            api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
        }

        function createLocation(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            token = res.body;
            api.post('/locations')
                .send({
                    location: {
                        lat: 44,
                        lng: 56
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
        }

        function createEstablishment(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for all the relations of' +
                        'estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEvent);
        }

        function createEvent(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/accounts/' + account.id + '/events')
                .send({
                    establishmentId: res.body.id,
                    name: 'Event-' + Date.now(),
                    description: 'Test event'
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
        }
    });

    it('error if GET accounts.events dont work', function(done) {
        var account, token;
        api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

        function loginAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            account = res.body;
            api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
        }

        function createLocation(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            token = res.body;
            api.post('/locations')
                .send({
                    location: {
                        lat: 44,
                        lng: 56
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
        }

        function createEstablishment(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for all the relations of' +
                        'estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEvent);
        }

        function createEvent(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/accounts/' + account.id + '/events')
                .send({
                    establishmentId: res.body.id,
                    name: 'Event-' + Date.now(),
                    description: 'Test event'
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getEvents);
        }

        function getEvents(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.get('/accounts/' + account.id + '/events')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
        }
    });
});

describe('Account relation with EstablishmentPhotos', function() {
    it('error if POST account.establishmentPhotos dont work',
    function(done) {
        var account, token;
        api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

        function loginAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            account = res.body;
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, createLocation);
        }

        function createLocation(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            token = res.body;
            api.post('/locations')
                .send({
                    location: {
                        lat: 3,
                        lng: 4
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
        }

        function createEstablishment(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for all the relations of' +
                        'estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishmentPhoto);
        }

        function createEstablishmentPhoto(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            var fileName = 'file-' + Date.now();
            api.post('/accounts/' + account.id + '/establishmentPhotos')
                .send({
                    expiresAt: new Date(),
                    file: {file: fileName, path: '/path/to/file/' + fileName},
                    establishmentId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
        }
    });

    it('error if GET account.establishmentPhotos dont work',
    function(done) {
        var account, token;
        api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

        function loginAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            account = res.body;
            api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
        }

        function createLocation(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            token = res.body;
            api.post('/locations')
                .send({
                    location: {
                        lat: 5,
                        lng: 6
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
        }

        function createEstablishment(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for all the relations of' +
                        'estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishmentPhoto);
        }

        function createEstablishmentPhoto(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            var fileName = 'file-' + Date.now();
            api.post('/accounts/' + account.id + '/establishmentPhotos')
                .send({
                    expiresAt: new Date(),
                    file: {file: fileName, path: '/path/to/file/' + fileName},
                    establishmentId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getEstablishmentPhotoFromAccount);
        }

        function getEstablishmentPhotoFromAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.get('/accounts/' + account.id + '/establishmentPhotos')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
        }
    });
});

describe('Account relation with MusicList', function() {
    it('error if POST account.musicList dont work', function(done) {
        var account, token;
        api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

        function loginAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            account = res.body;
            api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
        }

        function createLocation(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            token = res.body;
            api.post('/locations')
                .send({
                    location: {
                        lat: 63,
                        lng: 70
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
        }

        function createEstablishment(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for all the relations of' +
                        'estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createMusicList);
        }

        function createMusicList(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/accounts/' + account.id + '/musicLists')
                .send({
                    establishmentId: res.body.id,
                    name: 'Music List - ' + Date.now()
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
        }
    });

    it('error if GET account.musicList dont work', function(done) {
        var account, token;
        api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

        function loginAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            account = res.body;
            api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
        }

        function createLocation(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            token = res.body;
            api.post('/locations')
                .send({
                    location: {
                        lat: 63,
                        lng: 70
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
        }

        function createEstablishment(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for all the relations of' +
                        'estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createMusicList);
        }

        function createMusicList(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/accounts/' + account.id + '/musicLists')
                .send({
                    establishmentId: res.body.id,
                    name: 'Music List - ' + Date.now()
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getMusicListFromAccount);
        }

        function getMusicListFromAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.get('/accounts/' + res.body.id + '/musicLists')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
        }
    });
});

describe('Account relation with Shares', function() {
    it('error if GET account.shares dont work', function(done) {
        var account, friendAccount, token;
        api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, createSecondAccount);

        function createSecondAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            account = res.body;
            api.post('/accounts')
                .send({email: Date.now() + '@test2.com', password: '123'})
                .expect(200, loginAccount);
        }

        function loginAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            friendAccount = res.body;
            api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
        }

        function createLocation(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            token = res.body;
            api.post('/locations')
                .send({
                    location: {
                        lat: 1,
                        lng: 21
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
        }

        function createEstablishment(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for all the relations of share',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createShare);
        }

        function createShare(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/shares')
                .send({
                    accountId: account.id,
                    friendId: friendAccount.id,
                    establishmentId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getShareFromAccount);
        }

        function getShareFromAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            res.body.should.have.property('accountId');
            api.get('/accounts/' + account.id + '/shares')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
        }
    });
});

describe('Account relation with FavoriteLists', function() {
    it('error if POST account.favoriteLists dont work', function(done) {
        var account, token;
        api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

        function loginAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            account = res.body;
            api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
        }

        function createFavoriteList(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            token = res.body;
            api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
        }
    });

    it('error if GET account.favoriteLists dont work', function(done) {
        var account, token;
        api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

        function loginAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            account = res.body;
            api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
        }

        function createFavoriteList(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            token = res.body;
            api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getFavoriteListFromAccount);
        }

        function getFavoriteListFromAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.get('/accounts/' + account.id + '/favoriteLists')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
        }
    });
});

describe('Account relation with favorites', function() {
    it('error if POST account.favorites dont work', function(done) {
        var account, token, establishment;
        api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

        function loginAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            account = res.body;
            api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
        }

        function createLocation(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            token = res.body;
            api.post('/locations')
                .send({
                    location: {
                        lat: 56,
                        lng: 16
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
        }

        function createEstablishment(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for all the relations of' +
                        'estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavoriteList);
        }

        function createFavoriteList(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            establishment = res.body;
            api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavorite);
        }

        function createFavorite(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/accounts/' + account.id + '/favorites')
                .send({
                    establishmentId: establishment.id,
                    favoriteListId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, function(err, res) {
                    if (err) return done(err);
                    res.body.should.have.property('id');
                    res.body.should.have.property('accountId');
                    done();
                });
        }
    });

    it('error if GET account.favorites dont work', function(done) {
        var account, token, establishment;
        api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

        function loginAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            account = res.body;
            api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
        }

        function createLocation(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            token = res.body;
            api.post('/locations')
                .send({
                    location: {
                        lat: 56,
                        lng: 16
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
        }

        function createEstablishment(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for all the relations of' +
                        'estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavoriteList);
        }

        function createFavoriteList(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            establishment = res.body;
            api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavorite);
        }

        function createFavorite(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/accounts/' + account.id + '/favorites')
                .send({
                    establishmentId: establishment.id,
                    favoriteListId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getFavoriteFromAccount);
        }

        function getFavoriteFromAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            res.body.should.have.property('accountId');
            api.get('/accounts/' + account.id + '/favorites')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
        }
    });
});

describe('Account relation with favoriteListFollowers', function() {
    it('error if GET account.favoriteListFollowers dont work',
    function(done) {
        var account, secondAccount, token;
        api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, createSecondAccount);

        function createSecondAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            account = res.body;
            api.post('/accounts')
                .send({email: Date.now() + '@test2.com', password: '123'})
                .expect(200, loginAccount);
        }

        function loginAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            secondAccount = res.body;
            api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
        }

        function createFavoriteList(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            token = res.body;
            api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavoriteListFollower);
        }

        function createFavoriteListFollower(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/accounts/' + secondAccount.id + '/followedLists')
                .send({
                    favoriteListId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getFavoriteListFollowerFromAccount);
        }

        function getFavoriteListFollowerFromAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            res.body.should.have.property('accountId');
            api.get('/accounts/' + account.id + '/followedLists')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
        }
    });

    it('error if POST account.favoriteListFollowers dont work',
    function(done) {
        var account, secondAccount, token;
        api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, createSecondAccount);

        function createSecondAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            account = res.body;
            api.post('/accounts')
                .send({email: Date.now() + '@test2.com', password: '123'})
                .expect(200, loginAccount);
        }

        function loginAccount(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            secondAccount = res.body;
            api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
        }

        function createFavoriteList(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            token = res.body;
            api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavoriteListFollower);
        }

        function createFavoriteListFollower(err, res) {
            if (err) return done(err);
            res.body.should.have.property('id');
            api.post('/accounts/' + secondAccount.id + '/followedLists')
                .send({
                    favoriteListId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
        }
    });
});
