'use strict';
var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000/api');

describe('FavoriteList relations', function() {
    describe('FavoriteList belongs to Account', function() {
        it('error if GET favoriteList.account dont work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getAccountFromFavoriteList);
            }

            function getAccountFromFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.get('/favoriteLists/' + res.body.id + '/account')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
    describe('FavoriteList has many Favorite', function() {
        it('error if POST favoriteList.favorites dont work', function(done) {
            var account, token, favoriteList;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                favoriteList = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 1,
                        lng: 2
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for favorite-list-test',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavorite);
            }

            function createFavorite(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/favoriteLists/' + favoriteList.id + '/favorites')
                .send({
                    accountId: account.id,
                    establishmentId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });

        it('error if POST favoriteList.favorites work', function(done) {
            var account, token, favoriteList;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavorite);
            }

            function createFavorite(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/favoriteLists/' + res.body.id + '/favorites')
                .send({})
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(422, inspectError);
            }

            function inspectError(err, res) {
                if (err) return done(err);
                var errorCodes = JSON.parse(res.error.text).error.details.codes;
                errorCodes.should.have.property('accountId');
                errorCodes.should.have.property('establishmentId');
                done();
            }
        });

        it('error if GET favoriteList.favorites dont work', function(done) {
            var account, token, favoriteList;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                favoriteList = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 1,
                        lng: 2
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for favorite-list-test',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavorite);
            }

            function createFavorite(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/favoriteLists/' + favoriteList.id + '/favorites')
                .send({
                    accountId: account.id,
                    establishmentId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getFavoritesFromFavoritesList);
            }

            function getFavoritesFromFavoritesList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.get('/favoriteLists/' + favoriteList.id + '/favorites')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
    describe('FavoriteList has many FavoriteListFollower', function() {
        it('error if POST favoriteList.followers dont work', function(done) {
            var account, secondAccount, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, createSecondAccount);

            function createSecondAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts')
                .send({email: Date.now() + '@test2.com', password: '123'})
                .expect(200, loginAccount);
            }

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                secondAccount = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavoriteListFollower);
            }

            function createFavoriteListFollower(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/favoriteLists/' + res.body.id + '/followers')
                .send({
                    accountId: secondAccount.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });

        it('error if POST favoriteList.followers work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavoriteListFollower);
            }

            function createFavoriteListFollower(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/favoriteLists/' + res.body.id + '/followers')
                .send({})
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(422, inspectError);
            }

            function inspectError(err, res) {
                if (err) return done(err);
                var errorCodes = JSON.parse(res.error.text).error.details.codes;
                errorCodes.should.have.property('accountId');
                done();
            }
        });

        it('error if GET favoriteList.followers dont work', function(done) {
            var account, secondAccount, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, createSecondAccount);

            function createSecondAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts')
                .send({email: Date.now() + '@test2.com', password: '123'})
                .expect(200, loginAccount);
            }

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                secondAccount = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavoriteListFollower);
            }

            function createFavoriteListFollower(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/favoriteLists/' + res.body.id + '/followers')
                .send({
                    accountId: secondAccount.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getFavoriteListFollowesFromFavoriteList);
            }

            function getFavoriteListFollowesFromFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('favoriteListId');
                api.get(
                    '/favoriteLists/' + res.body.favoriteListId + '/followers'
                )
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
    describe('FavoriteList has many Share', function() {
        it('error if POST favoriteList.shares dont work', function(done) {
            var account, friendAccount, token, favoriteList;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, createSecondAccount);

            function createSecondAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts')
                .send({email: Date.now() + '@test2.com', password: '123'})
                .expect(200, loginAccount);
            }

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                friendAccount = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                favoriteList = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 81,
                        lng: 36
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for favoriteList',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavorite);
            }

            function createFavorite(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/accounts/' + account.id + '/favorites')
                .send({
                    favoriteListId: favoriteList.id,
                    establishmentId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createShare);
            }

            function createShare(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('establishmentId');
                api.post('/favoriteLists/' + favoriteList.id + '/shares')
                .send({
                    accountId: account.id,
                    friendId: friendAccount.id,
                    establishmentId: res.body.establishmentId
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });

        it('error if POST favoriteList.shares work', function(done) {
            var account, friendAccount, token, favoriteList;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createShare);
            }

            function createShare(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/favoriteLists/' + res.body.id + '/shares')
                .send({})
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(422, inspectError);
            }

            function inspectError(err, res) {
                if (err) return done(err);
                var errorCodes = JSON.parse(res.error.text).error.details.codes;
                errorCodes.should.have.property('accountId');
                errorCodes.should.have.property('friendId');
                errorCodes.should.have.property('establishmentId');
                done();
            }
        });

        it('error if GET favoriteList.shares dont work', function(done) {
            var account, friendAccount, token, favoriteList;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, createSecondAccount);

            function createSecondAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts')
                .send({email: Date.now() + '@test2.com', password: '123'})
                .expect(200, loginAccount);
            }

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                friendAccount = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                favoriteList = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 81,
                        lng: 36
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for favoriteList',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavorite);
            }

            function createFavorite(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/accounts/' + account.id + '/favorites')
                .send({
                    favoriteListId: favoriteList.id,
                    establishmentId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createShare);
            }

            function createShare(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('establishmentId');
                api.post('/favoriteLists/' + favoriteList.id + '/shares')
                .send({
                    accountId: account.id,
                    friendId: friendAccount.id,
                    establishmentId: res.body.establishmentId
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getShareFromFavoriteList);
            }

            function getShareFromFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('favoriteListId');
                api.get('/favoriteLists/' + favoriteList.id + '/shares')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
});
