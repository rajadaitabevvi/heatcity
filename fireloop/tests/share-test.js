'use strict';
var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000/api');

describe('Share relations', function() {
    describe('Share belongs to Account', function() {
        it('error if GET share.account dont work', function(done) {
            var account, friendAccount, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, createSecondAccount);

            function createSecondAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts')
                .send({email: Date.now() + '@test2.com', password: '123'})
                .expect(200, loginAccount);
            }

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                friendAccount = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 1,
                        lng: 21
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for all the relations of share',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createShare);
            }

            function createShare(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/shares')
                .send({
                    accountId: account.id,
                    friendId: friendAccount.id,
                    establishmentId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getAccountFromShare);
            }

            function getAccountFromShare(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('accountId');
                api.get('/shares/' + res.body.id + '/account')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });

        it('error if GET share.friend dont work', function(done) {
            var account, friendAccount, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, createSecondAccount);

            function createSecondAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts')
                .send({email: Date.now() + '@test2.com', password: '123'})
                .expect(200, loginAccount);
            }

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                friendAccount = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 43,
                        lng: 87
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for all the relations of share',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createShare);
            }

            function createShare(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/shares')
                .send({
                    accountId: account.id,
                    friendId: friendAccount.id,
                    establishmentId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getFriendAccountFromShare);
            }

            function getFriendAccountFromShare(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('friendId');
                api.get('/shares/' + res.body.id + '/friend')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
    describe('Share belongs to Establishment', function() {
        it('error if GET share.establishment dont work', function(done) {
            var account, friendAccount, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, createSecondAccount);

            function createSecondAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts')
                .send({email: Date.now() + '@test2.com', password: '123'})
                .expect(200, loginAccount);
            }

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                friendAccount = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 66,
                        lng: 33
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for all the relations of share',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createShare);
            }

            function createShare(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/shares')
                .send({
                    accountId: account.id,
                    friendId: friendAccount.id,
                    establishmentId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getEstablishmentFromShare);
            }

            function getEstablishmentFromShare(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('establishmentId');
                api.get('/shares/' + res.body.id + '/establishment')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
    describe('Share belongs to FavoriteList', function() {
        it('error if GET share.favoriteList dont work', function(done) {
            var account, friendAccount, token, favoriteList;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, createSecondAccount);

            function createSecondAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts')
                .send({email: Date.now() + '@test2.com', password: '123'})
                .expect(200, loginAccount);
            }

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                friendAccount = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                favoriteList = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 81,
                        lng: 36
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for all the relations of share',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavorite);
            }

            function createFavorite(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/accounts/' + account.id + '/favorites')
                .send({
                    favoriteListId: favoriteList.id,
                    establishmentId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createShare);
            }

            function createShare(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('establishmentId');
                api.post('/shares')
                .send({
                    accountId: account.id,
                    friendId: friendAccount.id,
                    establishmentId: res.body.establishmentId,
                    favoriteListId: favoriteList.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getFavoriteListFromShare);
            }

            function getFavoriteListFromShare(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('favoriteListId');
                api.get('/shares/' + res.body.id + '/favoriteList')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
});
