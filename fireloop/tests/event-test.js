'use strict';
var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000/api');

describe('Event relations', function() {
    describe('Event belongs to Establishment', function() {
        it('error if GET event.establishment dont work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 39,
                        lng: 46
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEvent);
            }

            function createEvent(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/events')
                .send({
                    accountId: account.id,
                    name: 'Event-' + Date.now(),
                    description: 'Test event'
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getEstablishmentFromEvent);
            }

            function getEstablishmentFromEvent(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.get('/events/' + res.body.id + '/establishment')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });

    describe('Event belongs to Account', function() {
        it('error if GET event.account dont work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 39,
                        lng: 46
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEvent);
            }

            function createEvent(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/events')
                .send({
                    accountId: account.id,
                    name: 'Event-' + Date.now(),
                    description: 'Test event'
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getAccountFromEvent);
            }

            function getAccountFromEvent(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.get('/events/' + res.body.id + '/account')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
});
