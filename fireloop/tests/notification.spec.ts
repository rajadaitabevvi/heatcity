var should    = require('chai').should();
var supertest = require('supertest');
var api       = supertest('http://localhost:3000/api');

describe('Notification unit tests:', () => {
    it('Should create a Notification instance', (done: Function) => {
        api.post('/notifications').send({
            text: 'test'
        }).expect(200, done);
    });
});
