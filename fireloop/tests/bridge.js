'use strict';
/*

TODO: Migrate TESTS TO NEW TRANSPORTATION  (JC)

var should = require('chai').should();
var supertest = require('supertest');
var kafka = require('kafka-node');
var config = require('../server/config.json');
var api = supertest('http://localhost:3000/api');
var Producer = kafka.Producer;
var Consumer = kafka.Consumer;
var emit = 0;
var Client = kafka.Client;
var app = require('../server/server');
var topics = {
    request: 'requestUpdatetesting',
    response: 'responseUpdatetesting',
    update: 'updateAreatesting'
};
describe('Bridge Test', function() {
    it('Recieve and save establishments', function(done) {
        this.timeout(30000);
        var client = new Client(config.kafka.url, config.kafka.namespace);
        var token, accountId;

        app.on('test:update', function() {
            if (emit === 0) {
                done();
                emit = 1;
            }
        });

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    accountId = res.body.userId;
                    createArea();
                });
        }
        function createArea() {
            api.post('/areas')
                .send({
                    name: 'tlaquepaque'
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    var data = [{
                        name: 'El Aguachile PerfecKto',
                        areaId: res.body.id,
                        visits: {foursquare: 42},
                        location: {
                            lat: 20.60209185607838,
                            lng: -103.40795516967773
                        },
                        categories: ['Seafood Restaurant'],
                        photos: []
                    }];
                    var producer = new Producer(client);
                    producer.on('ready', function() {
                        producer.send([{
                            topic: topics.response,
                            messages: JSON.stringify(data)
                        }], function(err, result) {
                            should.not.exist(err);
                            should.exist(result);
                            client.close(function() {});
                        });
                    });
                });
        }
    });
    it('Recieve and update the area', function(done) {
        this.timeout(30000);
        var client = new Client(config.kafka.url, config.kafka.namespace);
        var token, accountId;
        app.on('test:requestUpdate', function(data) {
            var area = JSON.parse(data);
            area.should.be.a('object');
            area.should.have.property('id');
            area.should.have.property('pulledAt');
            area.should.have.property('updates');
            area.updates.should.equal(1);
            done();
        });

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    accountId = res.body.userId;
                    createArea();
                });
        }

        function createArea() {
            api.post('/areas')
                .send({
                    name: 'zapopan'
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    var data = {id: res.body.id};
                    var producer = new Producer(client);
                    producer.on('ready', function() {
                        producer.send([{
                            topic: topics.update,
                            messages: JSON.stringify(data)
                        }], function(err, result) {
                            should.not.exist(err);
                            should.exist(result);
                            client.close(function() {});
                        });
                    });
                });
        }
    });

    it('Send GeoGrid to Micro Service', function(done) {
        this.timeout(30000);
        var token, accountId;
        var emit2 = 0;

        app.on('test:sendLocations', function(message) {
            if (emit2 == 0) {
                emit2 = 1;
                var data = JSON.parse(message);
                data.should.be.a('object');
                data.should.have.property('foursquare');
                data.foursquare.should.have.property('keys');
                data.foursquare.should.have.property('photosLimit');
                data.should.have.property('geogrid');
                data.should.have.property('areaId');
                done();
            }
        });
        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    accountId = res.body.userId;
                    findArea();
                });
        }

        function findArea() {
            api.get('/areas')
                .set({'Authorization': token})
                .query('filter[where][name]=guadalajara')
                .expect(200, function(err, res) {
                    should.not.exist(err);
                    should.exist(res.body.id);
                    // app.emit('test:bridge');
                });
        }
    });
});
*/
