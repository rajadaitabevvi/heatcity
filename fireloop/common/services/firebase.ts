
const FCM = require('fcm-node');
import { FirebasePayloadInterface } from '../interfaces/firebase-payload.interface';
import * as async from 'async';

export class FirebaseMessage {
    constructor(public model: any) {}

    public send(notification: FirebasePayloadInterface, accountId: string, next: Function): void {
        async.waterfall([
            (cb: Function) => {
                this.findDevicesByAccountId(accountId, cb)
            },
            (devices: Array<any>, cb: Function) => {
                notification.registration_ids = devices.map((device: any) => device.uuid);
                this.sendNotification(notification, cb)
            }
        ], (err: Error) => {
            if (err) {
                console.log('Error sending push notification ', err);
            }
            return next();
        })
    }

    private sendNotification(notification: FirebasePayloadInterface, next: Function): void {
        let fcm = new FCM(this.model.app.get('firebaseKey'));
        fcm.send(notification, (err: Error, res: any) => {
            if (err) {
                console.log('error', err);
                return next(err);
            }
            console.log('send success', res);
            return next()
        })
    }
    
    private findDevicesByAccountId(accountId: string, next: Function): void {
        let deviceModel = this.model.app.loopback.getModel('Device');
        let filter = {
            where: {
                accountId: accountId
            }
        };
        deviceModel.find(filter, (error: Error, devices: Array<any>) => {
            if (error) {
                return next(error);
            }
            return next(null, devices);
        });
    }
}