export interface TopicInterface { 
    update: string,
    request: string,
    response: string,
    [key: string]: string
};