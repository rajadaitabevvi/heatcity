export interface FirebasePayloadInterface {
    notification?: Notification;
    to?: string;
    registration_ids?: Array<string>;
    data?: any;
    priority?: string;
    content_available?: boolean;
}

interface Notification {
    title?: string;
    body?: string;
    sound?: string;
    badge?: string;
}
