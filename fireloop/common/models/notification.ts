import { Model } from '@mean-expert/model';
import * as async from 'async';
import { FirebasePayloadInterface } from '../interfaces/firebase-payload.interface';
import { FirebaseMessage } from '../services/firebase';
/**
 * @module Notification
 * @description
 * Write a useful Notification Model description.
 * Register hooks and remote methods within the
 * Model Decorator
 **/
@Model({
  hooks: {
    afterSave: { name: 'after save', type: 'operation' },
  },
  remotes: {
    myRemote: {
      returns : { arg: 'result', type: 'array' },
      http    : { path: '/my-remote', verb: 'get' }
    }
  }
})

class Notification {
  // LoopBack model instance is injected in constructor
  constructor(public model: any) {}

  afterSave(ctx: any, next: Function): void {
    if (ctx && !ctx.isNewInstance) {
      return next();
    }
    if (ctx.instance && process.env.NODE_ENV != 'testing') {
      this.buildPushNotification(ctx.instance, next);
    } else {
      next();
    }
  }

  myRemote(next: Function): void {
    this.model.find(next);
  }

  buildPushNotification(instance: any, next: Function): void {
    async.waterfall([
      //Get instance includes
      (cb: Function) => {
        this.getNotificationIncludes(instance, cb)
      },
      //Verify if user enabled the notifications setting
      (notificationRelation: any, cb: Function) => {
        if(notificationRelation.owner &&
          notificationRelation.owner.settings &&
          notificationRelation.owner.settings.notifications) {
            cb(null, notificationRelation);
        } else {
          cb('notifications setting is off in this Account');
        }
      },
      //Configure notification payload for firebase
      (notificationRelation: any, cb: Function) => {
        this.configureNotification(notificationRelation, cb);
      },
      //Update instance text property to match payload text
      (payload: FirebasePayloadInterface, cb: Function) => {
        instance.updateAttributes({text: payload.notification.body}, (err: Error, res: any) => {
          if(err) return cb(err);
          cb(null, payload);
        })
      }
    ], (err: Error, payload: FirebasePayloadInterface) => {
      if (err) {
        console.log('Error building notification: ', err);
        return next();
      }
      if (payload) {
        console.log('Im here!!');
        let firebaseMessage = new FirebaseMessage(this.model);
        firebaseMessage.send(payload, instance.ownerId, next)
      }else {
        return next();
      }
    });
  }

  getNotificationIncludes(instance: any, next: Function): void {
    let filter = {
      include: ['owner', 'friend', 'share', 'establishment', 'visit', 'account']
    }
    this.model.findById(instance.id, filter, (err: Error, notification: any) => {
      if (err) {
        return next(err);
      }
      let data = notification.toJSON();
      return next(null, data);
    })
  }

  configureNotification(notification: any, next: Function): void {
    let notificationTypes = this.model.app.get('notificationTypes');
    let payload: FirebasePayloadInterface;
    switch (notification.type) {
      case notificationTypes.friendRequest:
      case notificationTypes.friendResponse:
        payload = this.configureFriendNotification(notification);
        break;
      case notificationTypes.visit:
      case notificationTypes.friendVisit:
        payload = this.configureVisitNotification(notification);
        break;
      case notificationTypes.establishmentShare:
        payload = this.configureShareNotification(notification);
        break;
      default:
        console.log('Notification not found to deliver');
        break;
    }
    return next(null, payload);
  }

  configureShareNotification(notification: any): FirebasePayloadInterface {
    let notificationTypes = this.model.app.get('notificationTypes');
    let payload: FirebasePayloadInterface = {
      content_available: true,
      priority: 'high'
    };
    if (
      notification && notification.establishment && notification.account
    ) {
      payload.notification = {
        badge: '2',
        sound: 'default',
        body: `${notification.account.firstName || notification.account.displayName} shared ${notification.establishment.name}.`,
        title: 'Establishment shared with you.'
      }
      let data = {
        id: notification.id,
        establishmentId: notification.establishmentId,
        account: {
          id: notification.account.id,
          photo: notification.account.photo
        },
        updatedAt: notification.updatedAt,
        createdAt: notification.createdAt,
      }
      payload.data = {
        instance: data,
        type: notificationTypes.establishmentShare
      }
    } else {
      payload = null;
    }
    return payload;
  }

  configureVisitNotification(notification: any): FirebasePayloadInterface {
    let notificationTypes = this.model.app.get('notificationTypes');
    let payload: FirebasePayloadInterface = {
      content_available: true,
      priority: 'high'
    };
    if (
      !(notification && notification.visit
      && notification.establishment && notification.account)
    ) {
      return null;
    }
    let data = {
      id: notification.id,
      establishmentId: notification.establishment.id,
      account: {
        id: notification.account.id,
        photo: notification.account.photo
      },
      createdAt: notification.createdAt,
      updatedAt: notification.updatedAt
    }
    let body, type, title;
    if(notification.type == notificationTypes.friendVisit) {
      body = `${notification.account.firstName || notification.account.displayName} at ${notification.establishment.name}.`;
      type = notificationTypes.friendVisit;
      title = 'Friend activity';
    } else {
      body = `${notification.establishment.name} want to share?`;
      type = notificationTypes.visit;
      title = 'Visit.';
    }
    payload.notification = {
      badge: '3',
      sound: 'default',
      body: body,
      title: title
    }
    payload.data = {
      instance: data,
      type: type
    }
    return payload;
  }

  configureFriendNotification(notification: any): FirebasePayloadInterface {
    let notificationTypes = this.model.app.get('notificationTypes');
    let payload: FirebasePayloadInterface = {
      content_available: true,
      priority: 'high'
    };
    if (
      !(notification && notification.friend
      && notification.account)
    ) {
      return null;
    }
    let data = {
      id: notification.id,
      friendId: notification.friendId,
      account: {
        id: notification.account.id,
        photo: notification.account.photo
      }
    }
    payload.data = {
      instance: data
    }
    if (notification && notification.friend && notification.friend.approved) {
      payload.notification = {
        badge: '1',
        sound: 'default',
        body: `${notification.account.firstName || notification.account.displayName} accepted your friend request.`,
        title: 'Friend request accepted.'
      }
      payload.data.type = notificationTypes.friendResponse
    }else {
      payload.notification = {
        badge: '1',
        sound: 'default',
        body: `${notification.account.firstName || notification.account.displayName} sent you a friend request.`,
        title: 'Friend request.'
      }
      payload.data.type = notificationTypes.friendRequest
    }
    return payload;
  }
}

module.exports = Notification;
