import { Model } from '@mean-expert/model';
import * as async from 'async';
import * as moment from 'moment';
import { StatsInterface } from '../interfaces/stats.interface';
import { GraphStatsInterface } from '../interfaces/stats.interface';
import { DemographicsStatsInterface } from '../interfaces/stats.interface';
import { DemographicsGraphInterface } from '../interfaces/stats.interface';
import { VisitStats } from '../interfaces/stats.interface';

export interface TimeFrame {
  days: Array<number>,
  open: Array<{
    start: string,
    end: string
  }>
}

export interface Open {
  start: string,
  end: string
}
/**
 * @module Establishment
 * @description
 * Write a useful Establishment Model description.
 * Register hooks and remote methods within the
 * Model Decorator
 **/
@Model({
  hooks: {
    beforeSave: { name: 'before save', type: 'operation' }
  },
  remotes: {
    statistics: {
      returns : { root: true, type: 'Object' },
      accepts : { arg: 'id', type: 'string', required: true },
      http    : { path: '/:id/statistics', verb: 'get' },
      description: [
        'Returns a Stats object for establishment statistics.'
      ]
    },
    graphStats: {
      returns : { root: true, type: 'Object' },
      accepts : [
          { arg: 'id', type: 'string', required: true },
          { arg: 'startDate', type: 'string', required: true, description: 'YYYY-MM-DD' },
          { arg: 'endDate', type: 'string', required: true, description: 'YYYY-MM-DD' },
      ],
      http    : { path: '/:id/graphStats', verb: 'get' },
      description: [
        'Returns a Stats object for establishment statistics.'
      ]
    },
    demographicStats: {
      returns : { root: true, type: 'Object' },
      accepts :  { arg: 'id', type: 'string', required: true } ,
      http    : { path: '/:id/demographicStats', verb: 'get' },
      description: [
        'Returns a Stats object for establishment statistics.'
      ]
    },
    demographicsGraphs: {
      returns : { root: true, type: 'Object' },
      accepts : [
          { arg: 'id', type: 'string', required: true },
          { arg: 'startDate', type: 'string', required: true, description: 'YYYY-MM-DD' },
          { arg: 'endDate', type: 'string', required: true, description: 'YYYY-MM-DD' },
      ],
      http    : { path: '/:id/demographicsGraphs', verb: 'get' },
      description: [
        'Returns a Stats object for establishment statistics.'
      ]
    },
    visits: {
      returns : { root: true, type: 'Object' },
      accepts : [
          { arg: 'id', type: 'string', required: true },
          { arg: 'startDate', type: 'string', required: true, description: 'YYYY-MM-DD' },
          { arg: 'endDate', type: 'string', required: true, description: 'YYYY-MM-DD' },
      ],
      http    : { path: '/:id/visits', verb: 'get' },
      description: [
        'Returns a Stats object for establishment statistics.'
      ]
    },
    thereNow: {
      returns : { root: true, type: 'Object' },
      accepts : [
          { arg: 'establishmentId', type: 'string', required: true },
          { arg: 'accountId', type: 'string', required: true }
      ],
      http    : { path: '/:id/thereNow', verb: 'get' },
      description: [
        'Returns ids of mutual followers.'
      ]
    },
    visited: {
      returns : { root: true, type: 'Object' },
      accepts : [
          { arg: 'establishmentId', type: 'string', required: true },
          { arg: 'limit', type: 'number', required: false }
      ],
      http    : { path: '/:id/visited', verb: 'get' },
      description: [
        'Returns ids of all the friends who visited this establishment.'
      ]
    },
   loadBridge: {
      returns : { arg: 'result', type: 'object', root: true },
      http    : { path: '/load-bridge', verb: 'get' }
    },
   geoSearch: {
      accepts : [
        { arg: 'latitude', type: 'number', required: true },
        { arg: 'longitude', type: 'number', required: true },
        { arg: 'radius', type: 'number', required: false },
        { arg: 'limit', type: 'number', required: false }
      ],
      returns : { arg: 'result', type: 'object', root: true },
      http    : { path: '/geo-search', verb: 'get' }
    }
  }
})

class Establishment {
  // LoopBack model instance is injected in constructor
  constructor(public model: any) {}
  // Example Operation Hook
  beforeSave(ctx: any, next: Function): void {
    console.log('Establishment: Before Save');
    next();
  }
  // Example Remote Method
  loadBridge(next: Function): void {
    let listener: Function = (area: any) =>  {
      this.model.app.removeListener('BRIDGE:LOADED', listener);
      next(null, { loaded: true, area });
    };
    this.model.app.on('BRIDGE:LOADED', listener);
    this.model.app.emit('LOAD:BRIDGE');
  }

  // Custom NearBy Search Method

  geoSearch(latitude: number, longitude: number, radius: number = 10, limit: number = 10, next: Function): void {
     let EstablishmentCollection: any = this.model.getDataSource().connector.collection(this.model.modelName);
      EstablishmentCollection.find({
        geoLocation: {
          $geoWithin: {
            $centerSphere: [
              [ longitude, latitude],
              radius / 3963.2 // 10 miles
            ]
          }
        }
      }).limit(limit).toArray(next);
  }

  thereNow(estId:string, acctId:string, next: Function): void {
    next();
  }

  visited(estId:string, limit: number = 10,  next: Function): void {
    next();
  }

  statistics(id:string, next: Function): void {
    // let visit = this.model.app.models.Visit;
    let visit = this.model.app.loopback.getModel('Visit');
    let establishment: any;
    async.waterfall([
      (cb: Function) => {
        this.model.findById(id, cb);
      },
      (instance: any, cb: Function) => {
        establishment = instance;
        visit.find({
          where: {
            locationId: establishment.locationId
          },
          include: 'account'
        }, cb);
      }
    ], (error: Error, visits: Array<any>) => {
      if (error) {
        return next(error);
      }
      this.generateStats(visits, establishment, next)
    })
  }

  generateStats(visits: Array<any>, establishment: any, next: Function): void {
    let stats: StatsInterface = {}
    stats.displayAges = false;
    stats.ages = [
      { age: '13-17', ageCount: 0, min: 13, max: 17},
      { age: '18-24', ageCount: 0, min: 18, max: 24},
      { age: '25-34', ageCount: 0, min: 25, max: 34},
      { age: '35-44', ageCount: 0, min: 35, max: 44},
      { age: '45-54', ageCount: 0, min: 45, max: 54},
      { age: '55-64', ageCount: 0, min: 55, max: 64},
      { age: '65+', ageCount: 0, min: 64, max: 100}
    ];
    let heatHours: Array<any> = [];
    if (
      establishment &&
      establishment.popularAt && 
      establishment.popularAt.timeframes
    ) {
      establishment.popularAt.timeframes.forEach((timeFrame: TimeFrame) => {
          timeFrame.days.forEach((day) => {
            heatHours[day == 7 ? 0 : day] = this.formatDay(timeFrame.open);
          })
      })
      stats.heatHours = heatHours;
    }
    if (!visits.length) {
      return next(null, stats)
    }
    let totalRecords = 0;
    let validVisits = 0;
    let todayVisits = 0;
    let difference = 0;
    let currentDay = moment().isoWeekday();
    stats.gender = { female: 0, male: 0};
    stats.crowd = {
      current: 0
    }
    stats.visited = {
      today: 0,
      thisWeek:0,
      thisMonth:0,
      thisYear:0,
      total: 0
    }
    stats.timeSpent = {
      today: 0,
      thisWeek:0,
      thisMonth:0,
      thisYear:0
    }
    stats.time = {
      min: -1,
      max: 0,
      average: 0
    }
    async.each(visits, (visit, cb) => {
      visit = visit.toJSON();
      let date = moment(visit.account.birthday, 'MM/DD/YYYY');
      let userAge = moment().diff(date, 'years');
      let diffdays  = moment().diff(visit.createdAt, 'days');
      if (visit.validity === 100 && visit.entry && visit.exit) {
        difference = 0;
        let exit = moment(visit.exit)
        difference = exit.diff(moment(visit.entry));
        stats.time.max = difference > stats.time.max ? difference : stats.time.max;
        stats.time.min = (stats.time.min === -1 || stats.time.min > difference) ? difference : stats.time.min;
        stats.time.average += difference;
        validVisits += 1;
      }
      if (visit.account.gender === 'female') {
        stats.gender.female += 1;
      }
      if (visit.account.gender === 'male') {
        stats.gender.male += 1;
      }
      if (visit.entry && !visit.exit) {
        stats.crowd.current += 1;
      }
      if (diffdays === 0) {
        stats.visited.today += 1;
        stats.timeSpent.today += difference;
      }
      if (diffdays <= 7) {
        stats.visited.thisWeek += 1;
        stats.timeSpent.thisWeek += difference;
      }
      if (diffdays <= 30) {
        stats.visited.thisMonth += 1;
        stats.timeSpent.thisMonth += difference;
      }
      if (diffdays <= 365) {
        stats.visited.thisYear += 1;
        stats.timeSpent.thisYear += difference;
      }
      stats.visited.total +=1;
      stats.ages.forEach((age, index) => {
        if(userAge >= age.min && userAge <= age.max) {
          age.ageCount += 1;
          stats.displayAges = true;
          return 0;
        }
      });
      totalRecords += 1;
      return cb();
    }, (error: Error) => {
      if (error) {
        return next(error)
      }
      stats.time.average = stats.time.average / validVisits;
      stats.gender.female = Math.round((stats.gender.female * 100) / totalRecords);
      stats.gender.male = Math.round((stats.gender.male * 100) / totalRecords);
      if (stats.visited.today != 0) {
        stats.timeSpent.today = stats.timeSpent.today / stats.visited.today;
      }
      if (stats.visited.thisWeek != 0) {
        stats.timeSpent.thisWeek = stats.timeSpent.thisWeek / stats.visited.thisWeek;
      }
      if (stats.visited.thisMonth != 0) {
        stats.timeSpent.thisMonth = stats.timeSpent.thisMonth / stats.visited.thisMonth;
      }
      if (stats.visited.thisYear != 0) {
        stats.timeSpent.thisYear = stats.timeSpent.thisYear / stats.visited.thisYear;
      }
      return next(null, stats);
    });
  }

  formatDay(open: Array<{start: string, end: string}>): Array<any> {
    let popularHours = [
      { hour: "6a", popular: 0, min: 400, max: 700},
      { hour: "9a", popular: 0, min: 700, max: 999},
      { hour: "12p", popular: 0, min: 1000, max: 1300},
      { hour: "3p", popular: 0, min: 1300, max: 1600},
      { hour: "6p", popular: 0, min: 1600, max: 1900},
      { hour: "9p", popular: 0, min: 1900, max: 2200},
      { hour: "12a", popular: 0, min: 2200, max: 2400},
      { hour: "3a", popular: 0, min: 0, max: 400},
    ]
    open.forEach((hour: any) => {
      let end: number = Number(hour.end);
      let start: number = Number(hour.start);
      let hoursDiff: number;
      end = end === 0 ? 2400 : end
      if (end < start) {
        hoursDiff = end;
        end = 2400;
      }
      popularHours.forEach((popular) => {
        if (
          (popular.min >= start && popular.max <= end) ||
          (start >= popular.min && end <= popular.max) ||
          (0 >= popular.min && hoursDiff <= popular.max) ||
          (popular.min >= 0 && popular.max <= hoursDiff)
        ) {
          popular.popular += 1;
        }
      })
    })
    return popularHours;
  }

  graphStats(id:string, startDate:string, endDate:string, next: Function): void {
    // let visit = this.model.app.models.Visit;
    let visit = this.model.app.loopback.getModel('Visit');
    let startNow = new Date(startDate);
    let endNow = new Date(endDate);
    let establishment: any;
    async.waterfall([
      (cb: Function) => {
        this.model.findById(id, cb);
      },
      (instance: any, cb: Function) => {
        establishment = instance;
        visit.find({
          where: {
            locationId: establishment.locationId,
            createdAt : {gte: startNow, lte: endNow}
          },
          include: 'account'
        }, cb);
      }
    ], (error: Error, visits: Array<any>) => {
      if (error) {
        return next(error);
      }
      this.generateGraphStats(visits, establishment, next)
    })
  }

  generateGraphStats(visits: Array<any>, establishment: any, next: Function): void {
    let stats: GraphStatsInterface = {}
    stats.visitCount = {};
    stats.timeSpent = {};
    let difference = 0;
    async.each(visits, (visit, cb) => {
      visit = visit.toJSON();
      let vdate  = moment(visit.createdAt).format('YYYY-MM-DD')
      if (visit.validity === 100 && visit.entry && visit.exit) {
        difference = 0;
        let exit = moment(visit.exit)
        difference = exit.diff(moment(visit.entry));
        if (stats.visitCount[vdate]) {
          stats.visitCount[vdate] += 1;
        } else {
          stats.visitCount[vdate] = 1;
        }
        if (stats.timeSpent[vdate]) {
          stats.timeSpent[vdate] += difference;
        } else {
          stats.timeSpent[vdate] = difference;
        }
      }
      return cb();
    }, (error: Error) => {
      if (error) {
        return next(error)
      }
      for (var key in stats.timeSpent) {
        if (stats.visitCount[key]) {
          stats.timeSpent[key] = stats.timeSpent[key]/stats.visitCount[key];
        }
      }
      return next(null, stats);
    });
  }

  demographicStats(id:string, next: Function): void {
    let visit = this.model.app.loopback.getModel('Visit');
    let establishment: any;
    async.waterfall([
      (cb: Function) => {
        this.model.findById(id, cb);
      },
      (instance: any, cb: Function) => {
        establishment = instance;
        visit.find({
          where: {
            locationId: establishment.locationId
          },
          include: 'account'
        }, cb);
      }
    ], (error: Error, visits: Array<any>) => {
      if (error) {
        return next(error);
      }
      this.generateDemographicStats(visits, establishment, next)
    })
  }

  generateDemographicStats(visits: Array<any>, establishment: any, next: Function): void {
    let stats: DemographicsStatsInterface = {}
    stats.today = { ages : [
      { age: '13-17', ageCount: 0, min: 13, max: 17},
      { age: '18-24', ageCount: 0, min: 18, max: 24},
      { age: '25-34', ageCount: 0, min: 25, max: 34},
      { age: '35-44', ageCount: 0, min: 35, max: 44},
      { age: '45-54', ageCount: 0, min: 45, max: 54},
      { age: '55-64', ageCount: 0, min: 55, max: 64},
      { age: '65+', ageCount: 0, min: 64, max: 100}
    ],
    gender : {female:0, male :0}
    }
    stats.week = { ages : [
      { age: '13-17', ageCount: 0, min: 13, max: 17},
      { age: '18-24', ageCount: 0, min: 18, max: 24},
      { age: '25-34', ageCount: 0, min: 25, max: 34},
      { age: '35-44', ageCount: 0, min: 35, max: 44},
      { age: '45-54', ageCount: 0, min: 45, max: 54},
      { age: '55-64', ageCount: 0, min: 55, max: 64},
      { age: '65+', ageCount: 0, min: 64, max: 100}
    ],
    gender : {female:0, male :0}
    }
    stats.month = {ages : [
      { age: '13-17', ageCount: 0, min: 13, max: 17},
      { age: '18-24', ageCount: 0, min: 18, max: 24},
      { age: '25-34', ageCount: 0, min: 25, max: 34},
      { age: '35-44', ageCount: 0, min: 35, max: 44},
      { age: '45-54', ageCount: 0, min: 45, max: 54},
      { age: '55-64', ageCount: 0, min: 55, max: 64},
      { age: '65+', ageCount: 0, min: 64, max: 100}
    ],
    gender : {female:0, male :0}
    }
    stats.year = {ages: [
      { age: '13-17', ageCount: 0, min: 13, max: 17},
      { age: '18-24', ageCount: 0, min: 18, max: 24},
      { age: '25-34', ageCount: 0, min: 25, max: 34},
      { age: '35-44', ageCount: 0, min: 35, max: 44},
      { age: '45-54', ageCount: 0, min: 45, max: 54},
      { age: '55-64', ageCount: 0, min: 55, max: 64},
      { age: '65+', ageCount: 0, min: 64, max: 100}
    ],
    gender : {female:0, male :0}
    }
    let difference = 0;
    async.each(visits, (visit, cb) => {
      visit = visit.toJSON();
      let vdate  = moment(visit.createdAt).format('YYYY-MM-DD')
      if (visit.validity === 100 && visit.entry && visit.exit) {
        if(visit.account.birthday) {
           let date = moment(visit.account.birthday, 'MM/DD/YYYY');
          let userAge = moment().diff(date, 'years');
          let diffdays  = moment().diff(visit.createdAt, 'days');
          if (diffdays === 0) {
            stats.today.ages.forEach((age, index) => {
              if(userAge >= age.min && userAge <= age.max) {
                age.ageCount += 1;
                return 0;
              }
            });
            if (visit.account.gender === 'female') {
              stats.today.gender.female += 1;
            }
            if (visit.account.gender === 'male') {
              stats.today.gender.male += 1;
            }
          }
          if (diffdays <= 7) {
            stats.week.ages.forEach((age, index) => {
              if(userAge >= age.min && userAge <= age.max) {
                age.ageCount += 1;
                return 0;
              }
            });
            if (visit.account.gender === 'female') {
              stats.week.gender.female += 1;
            }
            if (visit.account.gender === 'male') {
              stats.week.gender.male += 1;
            }
          }
          if (diffdays <= 30) {
            stats.month.ages.forEach((age, index) => {
              if(userAge >= age.min && userAge <= age.max) {
                age.ageCount += 1;
                return 0;
              }
            });
            if (visit.account.gender === 'female') {
              stats.month.gender.female += 1;
            }
            if (visit.account.gender === 'male') {
              stats.month.gender.male += 1;
            }
          }
          if (diffdays <= 365) {
            stats.year.ages.forEach((age, index) => {
              if(userAge >= age.min && userAge <= age.max) {
                age.ageCount += 1;
                return 0;
              }
            });
            if (visit.account.gender === 'female') {
              stats.year.gender.female += 1;
            }
            if (visit.account.gender === 'male') {
              stats.year.gender.male += 1;
            }
          }
        }
      }
      return cb();
    }, (error: Error) => {
      if (error) {
        return next(error)
      }
      return next(null, stats);
    });
  }

  demographicsGraphs(id:string, startDate:string, endDate:string, next: Function): void {
    // let visit = this.model.app.models.Visit;
    let visit = this.model.app.loopback.getModel('Visit');
    let startNow = new Date(startDate);
    let endNow = new Date(endDate);
    let establishment: any;
    async.waterfall([
      (cb: Function) => {
        this.model.findById(id, cb);
      },
      (instance: any, cb: Function) => {
        establishment = instance;
        visit.find({
          where: {
            locationId: establishment.locationId,
            createdAt : {gte: startNow, lte: endNow}
          },
          include: 'account'
        }, cb);
      }
    ], (error: Error, visits: Array<any>) => {
      if (error) {
        return next(error);
      }
      this.generateDemographicGraphs(visits, establishment, next)
    })
  }

  generateDemographicGraphs(visits: Array<any>, establishment: any, next: Function): void {
    let stats: DemographicsGraphInterface = {}
    stats.total = { ages : [
      { age: '13-17', ageCount: 0, min: 13, max: 17},
      { age: '18-24', ageCount: 0, min: 18, max: 24},
      { age: '25-34', ageCount: 0, min: 25, max: 34},
      { age: '35-44', ageCount: 0, min: 35, max: 44},
      { age: '45-54', ageCount: 0, min: 45, max: 54},
      { age: '55-64', ageCount: 0, min: 55, max: 64},
      { age: '65+', ageCount: 0, min: 64, max: 100}
    ],
    gender : {female:0, male :0}
    }
    let difference = 0;
    async.each(visits, (visit, cb) => {
      visit = visit.toJSON();
      if (visit.validity === 100 && visit.entry && visit.exit) {
        if(visit.account.birthday) {
           let date = moment(visit.account.birthday, 'MM/DD/YYYY');
          let userAge = moment().diff(date, 'years');
          stats.total.ages.forEach((age, index) => {
            if(userAge >= age.min && userAge <= age.max) {
              age.ageCount += 1;
              return 0;
            }
          });
          if (visit.account.gender === 'female') {
            stats.total.gender.female += 1;
          }
          if (visit.account.gender === 'male') {
            stats.total.gender.male += 1;
          }
        }
      }
      return cb();
    }, (error: Error) => {
      if (error) {
        return next(error)
      }
      return next(null, stats);
    });
      
  }

  visits(id:string, startDate:string, endDate:string, next: Function): void {
    // let visit = this.model.app.models.Visit;
    let visit = this.model.app.loopback.getModel('Visit');
    let startNow = new Date(startDate);
    let endNow = new Date(endDate);
    let establishment: any;
    async.waterfall([
      (cb: Function) => {
        this.model.findById(id, cb);
      },
      (instance: any, cb: Function) => {
        establishment = instance;
        visit.find({
          where: {
            locationId: establishment.locationId,
            createdAt : {gte: startNow, lte: endNow}
          },
          include: 'account'
        }, cb);
      }
    ], (error: Error, visits: Array<any>) => {
      if (error) {
        return next(error);
      }
      this.getVisitCount(visits, establishment, next)
    })
  }

  getVisitCount(visits: Array<any>, establishment: any, next: Function): void {
    let stats: VisitStats = {}
    stats.count = 0;
    async.each(visits, (visit, cb) => {
      visit = visit.toJSON();
      if (visit.validity === 100 && visit.entry && visit.exit) {
        stats.count += 1;
      }
      return cb();
    }, (error: Error) => {
      if (error) {
        return next(error)
      }
      return next(null, stats);
    });
      
  }
}

module.exports = Establishment;
