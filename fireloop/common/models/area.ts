import { Model } from '@mean-expert/model';
import * as request from 'request';
import * as moment from 'moment';
import * as async from 'async';
/**
 * @module Area
 * @description
 * Write a useful Area Model description.
 * Register hooks and remote methods within the
 * Model Decorator
 **/
@Model({
  hooks: {
    beforeSave: { name: 'before save', type: 'operation' },
    afterFind: { name: 'find', type: 'afterRemote' },
    beforeFind: { name: 'find', type: 'beforeRemote' }
  },
  remotes: {
    calculateRadius: {
      accepts: [{arg: 'areaId', type: 'string'}],
      returns: { root: true, type: 'object' },
      http    : { path: '/calculateRadius', verb: 'post' }
    }
  }
})

class Area {
  // LoopBack model instance is injected in constructor
  constructor(public model: any) {}

  // Example Operation Hook
  beforeSave(ctx: any, next: Function): void {
    console.log('Area: Before Save');
    next();
  }
  // Example Remote Method
  calculateRadius(areaId: string, next: Function): void {
    async.waterfall([
      //Get Area instance
      (next: Function) => {
        this.model.findById(areaId, next);
      },
      //Get points
      (area: any, next: Function) => {
        console.log('Area!!!!!!, ', area.name);
        let pointA, pointB, bottomLeft: {lat: any, lng: any}, topRigth: {lat: any, lng: any};
        if(area.geogrid && Array.isArray(area.geogrid) && area.geogrid.length > 0) {
          let lastRowIndex = area.geogrid.length - 1;
          let lastOfRowIndex = area.geogrid[lastRowIndex].length - 1;
          if(area.geogrid[0].length > 0 && lastRowIndex > 0 && lastOfRowIndex > 0) {
            bottomLeft = area.geogrid[0][0];
            topRigth = area.geogrid[lastRowIndex][lastOfRowIndex];
            pointA = new this.model.app.loopback.GeoPoint({lat: bottomLeft.lat, lng: bottomLeft.lng});
            pointB = new this.model.app.loopback.GeoPoint({lat: bottomLeft.lat, lng: topRigth.lng});
          }
        }
        next(null, area, bottomLeft, topRigth, pointA, pointB);
      },
      //Update area radius property
      (area:any, southWest: {lat: any, lng: any}, northEast: {lat: any, lng: any}, pointA: any, pointB: any, next: Function) => {
        if(pointA && pointB) {
          let distance = Math.round(pointA.distanceTo(pointB, {type:"meters"}) / 2);
          area.updateAttributes({radius: distance, northEast: northEast, southWest: southWest}, next);
        } else {
          next(null, area);
        }
      }
    ], (err, res) => {
      if(err) return next(err);
      next(null, res);
    })
  }

  /**
   * @method beforeFind
   **/
  beforeFind(ctx: any, instance: any, next: Function) {
    let filter: any;
    if (typeof ctx.args.filter == 'object') {
      filter = ctx.args.filter;
    }else {
      filter = ctx.args.filter ? JSON.parse(ctx.args.filter) : {};
    }
    if (filter && filter.where && filter.where.name && !filter.where.name.like) {
      let socialServices = this.model.app.get('socialServices');
      let uri = `https://maps.googleapis.com/maps/api/geocode/json?address=${filter.where.name}&key=${socialServices.googleMaps.key}`;
      filter.where.name;
      request.get(uri, (err, response, body) => {
        if (err) {
          return next(err);
        }
        let data = JSON.parse(body);
        if (
          data && data.results && data.results.length && data.results[0].geometry
          && data.status === 'OK'
        ) {
          delete filter.where.name;
          filter.where.placeId = data.results[0].place_id;
          ctx.args.filter = JSON.stringify(filter);
          return next();
        }else {
          let error = new Error();
          error.message = 'The provided area can not be found';
          return next(error);
        }
      });
    }else {
      return next();
    }
  }

  /**
   * @method afterFind
   **/
  afterFind(ctx: any, instance: any, next: Function) {
    if (ctx.result.length) {
      return next();
    }
    let filter = typeof ctx.args.filter === 'string' ?
      JSON.parse(ctx.args.filter) : ctx.args.filter;
    let placeId = filter && filter.where && 
      filter.where.placeId ? filter.where.placeId : undefined;
      if (placeId) {
        this.getMapsData(placeId, instance, next);
      }else {
        return next();
      }
  }

  /**
   * @method getMapsData
   **/
  public getMapsData(placeId: string, instance: any, cb: Function): void {
    let socialServices = this.model.app.get('socialServices');
    let uri = `https://maps.googleapis.com/maps/api/geocode/json?place_id=${placeId}&key=${socialServices.googleMaps.key}`;
    request.get(uri, (err, response, body) => {
      if (err) {
        return cb(err);
      }
      let data = JSON.parse(body);
      if (data && data.results && data.results.length && data.results[0].geometry) {
          let options = {
            geometry: data.results[0].geometry,
            address: data.results[0].formatted_address,
            placeId: placeId
          }
          this.calculateGrid(options, instance, cb)
      }else {
        let error = new Error();
        error.message = 'The provided area can not be found';
        return cb(error);
      }
    });
  }

  /**
   * @method calculateGrid
   **/
  public calculateGrid(options: any, instance: any, cb: Function): void {
    let positions: Array<any> = [];
    let percentPoints = 100 / this.model.app.get('percentage');
    let southwest = options.geometry.bounds.southwest;
    let northeast = options.geometry.bounds.northeast;
    let differenceTopBottom = {
      lat: (northeast.lat - southwest.lat),
      lng: (northeast.lng - southwest.lng)
    }
    for (let i = 0; i <= percentPoints; i++) {
      positions[i] = [];
      for (let j = 0; j <= percentPoints; j++) {
        positions[i].push({
          lat: southwest.lat + (differenceTopBottom.lat / percentPoints * j),
          lng: southwest.lng + (differenceTopBottom.lng / percentPoints * i)
        })
      }
    }
    this.model.upsertWithWhere(
      { placeId: options.placeId },
      {
        placeId: options.placeId,
        name: options.address,
        pulledAt: moment().toISOString(),
        geogrid: positions,
        center: options.geometry.location,
        createdAt: moment().toISOString(),
        updatedAt: moment().toISOString()
      },
      (err: Error, area: any) => {
        if (err) {
          return cb(err);
        }
        this.calculateRadius(area.id, (err: Error, res: any) => {
          if(err) {
            cb(err);
          } else {
            console.log('Radius added! area: ', area.id);
            instance.push(res);
            cb();
          }
        });
        /** 
        Not loading the area parsing from here.
        let listener: Function = (areas: any) =>  {
          this.model.app.removeListener('BRIDGE:LOADED', listener);
          cb();
        };
        this.model.app.on('BRIDGE:LOADED', listener);
        this.model.app.emit('LOAD:BRIDGE');
        **/
      })
  }
}

module.exports = Area;
