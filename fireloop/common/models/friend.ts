import { Model } from '@mean-expert/model';
import * as async from 'async';
import { FirebasePayloadInterface } from '../interfaces/firebase-payload.interface';
import { FirebaseMessage } from '../services/firebase';

/**
 * @module Friend
 * @description
 * Write a useful Friend Model description.
 * Register hooks and remote methods within the
 * Model Decorator
 **/
@Model({
  hooks: {
    afterSave: { name: 'after save', type: 'operation' }
  },
  remotes: {
    myRemote: {
      returns : { arg: 'result', type: 'object' },
      http    : { path: '/my-remote', verb: 'get' }
    },
    mutualFollowers: {
      accepts: {arg: 'accountId', type: 'string'},
      returns: { root: true, type: 'array' },
      http: { path: '/mutualFollowers', verb: 'get'}
    }
  }
})

class Friend {
  // LoopBack model instance is injected in constructor
  constructor(public model: any) {}

  // Example Operation Hook
  afterSave(ctx: any, next: Function): void {
    console.log('Friend: After Save');
    if (ctx && !ctx.isNewInstance) {
      return next();
    }
    if (ctx.instance && process.env.NODE_ENV != 'testing') {
      this.createNotification(ctx.instance, next);
    }else {
      next();
    }
  }
  // Example Remote Method
  myRemote(next: Function): void {
    let get = this.model.app.get('port');
    this.model.find(next);
  }

  createNotification(instance: any, next: Function): void {
    let notificationTypes = this.model.app.get('notificationTypes');
    let notification = this.model.app.loopback.getModel('Notification');
    let data = {
      ownerId: instance.friendId,
      friendId: instance.id,
      accountId: instance.accountId,
      type: instance.approved ? notificationTypes.friendResponse : notificationTypes.friendRequest
    }
    notification.create(data, (err: Error, data: any) => {
      if (err) {
        console.log('Error!!! ', err);
        console.log('It wasnot possible to create friend notification');
      }
      return next();
    })
  }

  /**
   * @method mutualFollowers
   **/
  mutualFollowers(accountId: string, next: Function): void {
    async.waterfall([
      (cb: Function) => {
        this.model.app.models.Friend.find({
          where: {
            approved: true,
            mutual: true,
            accountId: accountId
          }
        },cb);
      },
      (mFriends: any[], cb: Function) => {
        let result: any[] = []
        if(Array.isArray(mFriends) && mFriends.length > 0) {
          async.eachSeries(
            mFriends,
            (mFriend: any, callback: Function) => {
              this.model.app.models.Account.findOne({
                where: {
                    id: mFriend.friendId
                }
              }, (err: Error, resaccount: any) => {
               if(err) return callback(err);
                  result.push(resaccount);
                  callback();
                });
            },
            (err: Error) => {
              if(err) return cb(err);
              cb(null,result);
            }
          );
        } else {
          cb(null, result);
        }
      }
    ],(err: Error, result: any) => {
      if(err) return next(err);
      next(null, result);
    });
  }
}

module.exports = Friend;
