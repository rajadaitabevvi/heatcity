import { Model } from '@mean-expert/model';
/**
 * @module ExternalFriend
 * @description
 * Write a useful ExternalFriend Model description.
 * Register hooks and remote methods within the
 * Model Decorator
 **/
@Model({
  hooks: {
    beforeSave: { name: 'before save', type: 'operation' }
  },
  remotes: {
    myRemote: {
      returns : { arg: 'result', type: 'object' },
      http    : { path: '/my-remote', verb: 'get' }
    }
  }
})

class ExternalFriend {
  // LoopBack model instance is injected in constructor
  constructor(public model: any) {}

  // Example Operation Hook
  beforeSave(ctx: any, next: Function): void {
    console.log('ExternalFriend: Before Save');
    next();
  }
  // Example Remote Method
  myRemote(next: Function): void {
    this.model.find(next);
  }
}

module.exports = ExternalFriend;
