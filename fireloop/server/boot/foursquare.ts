import { BootScript } from '@mean-expert/boot-script';
var fs = require('node-foursquare-venues');
import * as async from 'async';

function getFSCategories(): Array<string> {
  let categories = require('./foursquare/fsCategories');
  let keys = Object.keys(categories);
  let result = keys.map((key: string) => {
    return categories[key];
  });
  return result;
}
var categories = getFSCategories();

@BootScript()
class FoursquareService {

  private finish: Function;
  constructor(private app: any) {
    console.log('REGISTERING: ', `FETCH:ESTABLISHMENTS:${process.env.NODE_ENV}`);
    this.app.on(`FETCH:ESTABLISHMENTS:${process.env.NODE_ENV}`, (request: any) => {

      let ctx: any = { request, keyOffset: 0, venues: {}, establishments: [] };
      console.log('AREA RECEIVED, CTX: ', ctx);
      if (ctx.request && ctx.request.foursquare) {
        ctx.fs = fs(
          ctx.request.foursquare.keys[0].clientId,
          ctx.request.foursquare.keys[0].secretId,
        );
        let currentVertex: { x: number, y: number, z: number } = { x: 0, y: 0, z: 0 };
        async.eachSeries(ctx.request.area.geogrid, (geopoints: any, next: any) => {
          if (!request.area.errorOnVertex || currentVertex.y >= request.area.errorOnVertex.y) {
            async.eachSeries(geopoints, (geopoint: any, next: any) => {
              if (!request.area.errorOnVertex || currentVertex.x >= request.area.errorOnVertex.x) {
                async.eachSeries(categories, (categoryId: string, next: any) => {
                  if (!request.area.errorOnVertex || currentVertex.z >= request.area.errorOnVertex.z) {
                    this.fetchFSData(ctx, geopoint, categoryId, (err: Error) => {
                      if (err) {
                        return next(err);
                      }
                      currentVertex.z += 1;
                      next();
                    });
                  } else {
                    // This geopoint already imported in this round, don't do anything.
                    currentVertex.z += 1;
                    next();
                  }
                }, (err: Error) => {
                  if (!err) {
                    currentVertex.z = 0;
                    currentVertex.x += 1;
                  }
                  next(err);
                });
              } else {
                currentVertex.z = 0;
                currentVertex.x += 1;
                next();
              }
            }, (err: Error) => {
              if (!err) {
                currentVertex.x = 0;
                currentVertex.y += 1;
              }
              next(err);
            });
          } else {
            currentVertex.x = 0;
            currentVertex.y += 1;
            next();
          }
        }, (err: Error) => {
          request.area.errorOnVertex = currentVertex;
          let result: any = { area: request.area };
          if (err) { result.error = err; }
          console.log('ESTABLISHMENTS TO BE SENT');
          console.log(ctx.establishments.map((establishment: any) => establishment.name).join(', '));
          this.app.emit(`SEND:ESTABLISHMENTS:${process.env.NODE_ENV}`, ctx.establishments);
          console.log('SENDING AREA RESULT:', result);
          this.app.emit(`AREA:UPDATE:${process.env.NODE_ENV}`, result);
          if (typeof this.finish === 'function') { this.finish(); }
          ctx = null;
        });
      }
    });
  }

  onFinish(finish: Function): void {
    this.finish = finish;
  }

  fetchFSData(ctx: any, geopoint: any, categoryId: string, next: any) {
    async.waterfall([
      // get Foursquare Venues
      (next: any) => {
        let searchObj = {
          intent: 'browse',
          radius: 5000,
          ll: geopoint.lat + ',' + geopoint.lng,
          categoryId: categoryId
        };
        ctx.fs.venues.search(searchObj, (err: any, venues: any) => {
          if (err) {
            if (err == 403)
              this.changeFoursquareInstanceVenues(ctx, searchObj, next);
            else
              next(err);
          } else {
            next(null, venues);
          }
        });
      },
      //get Foursquare Venue Data
      (venues: any, next: any) => {
        async.eachSeries(venues.response.venues, (venue: any, callback: any) => {
          this.getFSVenueData(ctx, venue, callback);
        }, (err: Error) => {
          if (err) return next(err);
          next();
        });
      },
      //send Establishments
      (next: any) => {
        console.log(geopoint.lat + ',' + geopoint.lng);
        next();
      }
    ], next);
  }

  getFSVenueData(ctx: any, venue: any, cb: any) {
    if (ctx.venues[`${venue.name}:${venue.location.lat}:${venue.location.lng}`]) {
      return cb();
    } else {
      ctx.venues[`${venue.name}:${venue.location.lat}:${venue.location.lng}`] = true;
    }
    async.waterfall([
      //Get Venue photos
      (next: any) => {
        ctx.fs.venues.photos(venue.id,{ limit: ctx.request.foursquare.photosLimit },next);
      },
      (photos: any, next: Function) => {
        ctx.fs.venues.venue(venue.id, (err: Error, venueData: any) => {
          if (err) {
            return next(err);
          }
          next (null, photos, venueData);
        });
      },
      //Get Venue hours
      (venuePhotos: any, venueData: any, next: any) => {
        ctx.fs.venues.hours(venue.id, (err: any, data: any) => {
          if (err) return next(err);
          next(null, venuePhotos, venueData, data);
        });
      },
      //Build Establishment Object
      (venuePhotos: any, venueData: any, venueHours: any, next: Function) => next(null, this.buildEstablishmentObj(venue, venuePhotos, venueData, venueHours, ctx))
    ], (err: any, establishment: any) => {
      if (err) {
        ctx.venues[`${venue.name}:${venue.location.lat}:${venue.location.lng}`] = false;
        if (ctx.keyOffset + 1 < ctx.request.foursquare.keys.length && err == 403) {
          ctx.keyOffset += 1;
          ctx.fs = fs(
            ctx.request.foursquare.keys[ctx.keyOffset].clientId,
            ctx.request.foursquare.keys[ctx.keyOffset].secretId,
          );
          console.log('Error - CHANGING FOURSQUARE KEYS, TRYING AGAIN');
          this.getFSVenueData(ctx, venue, cb);
        } else if (err == 500) {
          console.log('Error - TRYING AGAIN');
          this.getFSVenueData(ctx, venue, cb);
        } else {
          cb(err);
        }
      } else {
        console.log('\x1b[33mADDING ESTABLISHMENT:\x1b[0m', establishment.name);
        ctx.establishments.push(establishment);
        cb();
      }
    });
  }

  buildEstablishmentObj(venue: any, venuePhotos: any, venueData: any, venueHours: any, ctx: any) {
    let establishment: any = {};
    //add name
    establishment.name = venue.name;
    //Initialize address
    establishment.address = '';
    //add Area id
    if(ctx && ctx.area && ctx.area.id)
      establishment.areaId = ctx.area.id;
    //add price
    if (venue.price != null)
      establishment.pricing = venue.price.tier ? venue.price.tier : 0;
    //add url
    if (venue.url != null)
      establishment.url = venue.url;
    //add rating
    if (venue.rating != null)
      establishment.rating = venue.rating;
    //add menu
    if (venue.menu != null)
      establishment.menu = venue.menu;
    //add address
    if (venue.location.address != null)
      establishment.address = venue.location.address
    //concat city to address
    if(venue.location.city != null)
      establishment.address += `${ establishment.address.length > 0 ? ', ' : '' }${venue.location.city}`
    //concate country to address
    if(venue.location.country != null)
      establishment.address += `${ establishment.address.length > 0 ? ', ' : '' }${venue.location.country}`
    //add foursquare visits
    establishment.visits = {
      foursquare: venue.stats ? venue.stats.checkinsCount : 0
    };
    //add operatesAt hours
    if (venueHours && venueHours.response && venueHours.response.hours)
      establishment.operatesAt = venueHours.response.hours;
    //add popularAt hours
    if (venueHours && venueHours.response && venueHours.response.popular)
      establishment.popularAt = venueHours.response.popular;
    //add location
    establishment.geoLocation = {
      type: 'Point',
      coordinates: [venue.location.lng, venue.location.lat]
    };
    //add categories
    establishment.categories = venue.categories.map((category: any) => {
      return category.name
    });
    //add rating
    if (
      venueData &&
      venueData.response &&
      venueData.response.venue &&
      venueData.response.venue.rating
    ) {
      establishment.rating = venueData.response.venue.rating;
    }
    //add photos
    if(
      venuePhotos &&
      venuePhotos.response &&
      venuePhotos.response.photos &&
      venuePhotos.response.photos.items
    ) {
      establishment.photos = venuePhotos.response.photos.items.map((photo: any) => {
        return { path: photo.prefix + '300x500' + photo.suffix };
      });
    }
    return establishment;
  }

  changeFoursquareInstanceVenues(ctx: any, searchObj: any, next: any) {
    if (ctx.keyOffset + 1 < ctx.request.foursquare.keys.length) {
      ctx.keyOffset += 1;
      ctx.fs = fs(
        ctx.request.foursquare.keys[ctx.keyOffset].clientId,
        ctx.request.foursquare.keys[ctx.keyOffset].secretId,
      );
      console.log('FOURSQUARE MS: changing foursquare credentials ', ctx.keyOffset);
      console.log(JSON.stringify(ctx.request.foursquare.keys[ctx.keyOffset]));
      console.log(JSON.stringify(searchObj));
      ctx.fs.venues.search(searchObj, (err: any, result: any) => {
        if (err) {
          if (err == 403)
            return this.changeFoursquareInstanceVenues(ctx, searchObj, next);
          else if (err === 400) {
            return next(err);
          } else {
            return next(err);
          }
        }
        else {
          return next(null, result);
        }
      });
    } else {
      return next("No more requests available");
    }
  }
}

module.exports = FoursquareService;
