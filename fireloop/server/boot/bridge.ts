import { BootScript } from '@mean-expert/boot-script';
import * as async from 'async';
import * as schedule from 'node-schedule';
import * as fs from 'fs';
import * as path from 'path';
import * as request from 'request';
import * as moment from 'moment';
import * as io from 'socket.io-client';
import { TopicInterface } from '../../common/interfaces/topic.interface';
/**
 * @class Bridge
 * @author Jonathan Casarrubias jcasarrubias@itexico.com
 * @author Raul Vargas rvargas@itexico.net
 */
@BootScript()
class Bridge {

    private job: any;
    private root: string = path.join(__dirname, "../tmp/");
    //private client: any = io('http://127.0.0.1:4200', { transports: ['websocket'] });
    private containerName: string = 'heat-city-storage';

    constructor(private app: any) { this.setup(); }
    /**
     * @method setup
     * @returns {void}
     * @description Setup Bridge Process
     **/
    setup(): void {
        // Setup bridge starter listener
        this.app.on('LOAD:BRIDGE', () => {
            console.log('LOADING BRIDGE');
            this.updateExpiredAreas();
        });
        // Setup listener to set an area as updated.
        this.app.on(
            `AREA:UPDATE:${process.env.NODE_ENV}`,
            (response: any) => this.onAreaUpdate(response)
        );
        // On Establishment Listener
        let OEB: Function = (establishments: any) => {
            console.log('====================');
            console.log(establishments.map((establishment: any) => establishment.name).join(', '));
            console.log('====================');
            this.onEstablishmentBatch(establishments)
        }
        // Setup listener to create or update received establishments.
        this.app.on(`SEND:ESTABLISHMENTS:${process.env.NODE_ENV}`, OEB);
        // Setup Scheduler
        /*let rule: schedule.RecurrenceRule = new schedule.RecurrenceRule();
        rule.minute = 15;
        this.job = schedule.scheduleJob(rule, () => this.updateExpiredAreas());*/
    }
    /**
     * @method updateExpiredAreas
     * @returns {void}
     * @description Setup Bridge Process
     **/
    updateExpiredAreas(): void {
        console.log('FINDING EXPIRED AREAS TO BE UPDATED');
        this.app.models.Area.findOne({
            where: {
                processing: false,
                pulledAt: {
                    lte: moment().subtract(this.app.get('expiration'), 'days').format()
                }
            }
        }, (err: any, area: any) => {
            if (err) {
                console.log('Error at line 67', err);
            } else {
                // Send result array to caller.
                this.app.emit('BRIDGE:LOADED', { area });
                // Send areas to microservice
                if (area) {
                    console.log('AREA TO BE UPDATED:', area.name);
                    area.processing = true;
                    area.save();
                    this.emitArea(area);
                } else {
                    console.log('NO EXPIRED AREA FOUND');
                }
            }
        });
    }
    /**
     * @method emitArea
     * @returns {void}
     * @description Send an area to microservices to fetch venues
     **/
    emitArea(area: any): void {
        let data = {
            area,
            foursquare: {
                keys: this.app.get('socialServices').foursquare,
                photosLimit: this.app.get('photosLimit')
            }
        };
        // In order to be tested, this method either sends back an area geogrid
        // or will actually continue the process by passing the grid to the
        // available microservices.
        if (process.env.NODE_ENV === 'testing') {
            this.app.emit('test:sendLocations', JSON.stringify(data));
        } else {
            console.log('EMITTING: ', `FETCH:ESTABLISHMENTS:${process.env.NODE_ENV}`);
            this.app.emit(`FETCH:ESTABLISHMENTS:${process.env.NODE_ENV}`, data);
        }
    }
    /**
     * @method onAreaUpdate
     * @returns {void}
     * @description When a microservice finish with an area it will fire
     * the following event, that will recursively start the process
     * when pending areas are still in the stack.
     **/
    onAreaUpdate(response: any) {
        console.log('GOT RESPONSE: ', response);      
        this.app.models.Area.findById(response.area.id, (err: Error, area: any) => {
            if (err) {
                return console.log('Error on line 108 ', err);
            } else if (area) {
                area.processing = false;
                if (response.error && response.area.errorOnVertex) {
                    area.errorOnVertex = response.area.errorOnVertex;
                    area.save();
                    console.log('ON AREA UPDATE RESPONSE ERROR', response.area.name, response.area.errorOnVertex);
                    console.log('ERROR: ', response.error);
                } else {
                    let date = moment().toISOString();
                    area.pulledAt = date;
                    area.updates += 1;
                    area.errorOnVertex = null;
                    area.save();
                    // If there are more areas in the stack, start the process for these.
                    this.updateExpiredAreas();
                }
            }
        });
    }
    /**
     * @method onEstablishmentBatch
     * @returns {void}
     * @description When a microservice finish with an area it will fire
     * the following event, that will recursively start the process
     * when pending areas are still in the stack.
     **/
    onEstablishmentBatch(establishments: any[]) {
        // Itearate and process each of the social source establishments
        async.eachSeries(establishments, (scEstablishment: any, next: any) => {
            // Create/Update the establishment and persist gallery
            async.waterfall([
                // Create or Update establishment
                (next: any) => this.app.models.Establishment.findOne({
                    where: {
                        and: [
                            { name: scEstablishment.name },
                            { 'geoLocation.coordinates.0': scEstablishment.geoLocation.coordinates[0] },
                            { 'geoLocation.coordinates.1': scEstablishment.geoLocation.coordinates[1] }
                        ]
                    },
                    include: 'location'
                }, (err: Error, hcEstablishment: any) => {
                    if (err) {
                        return next(err);
                    }
                    if (hcEstablishment) {
                        this.updateEstablishment(hcEstablishment, scEstablishment, next);
                    } else {
                        this.createEstablishment(scEstablishment, next);
                    }
                }),
                // Persist gallery from social source into heatcity database
                (hcEstablishment: any, next: any) =>
                    this.persistGallery(scEstablishment, hcEstablishment, next)
            ], next);
        }, (err: Error) => {
            // Remove establishments array from memory
            establishments = null;
            if (err) {
                console.log('ESTABLISHMENT BATCH ERROR: ', err);
            } else {
                console.log('ESTABLISHMENT BATCH SUCCESS');
            }
            if (process.env.NODE_ENV === 'testing') {
                this.app.emit('test:update', err);
            }
        })
    }
    /**
     * @method createEstablishment
     * @returns {void}
     * @description If a received establishment from social source is not found
     * within the heatcity database, then we will need to create it.
     **/
    createEstablishment(scEstablishment: any, next: any) {
        let hcEstablishment: any = null;
        async.waterfall([
            // The following flow will get async dependencies in parallel
            (next: any) => async.parallel({
                // Create Location
                location: (next: any) => this.app.models.Location.create({
                    geoLocation: scEstablishment.geoLocation
                }, next),
                // Find an Admin to be set as owner for this establishment
                admin: (next: any) => {
                    let admin: any = this.app.get('seed').admins[0];
                    // get an admin account
                    this.app.models.Account.findOne(
                        { where: { email: admin.email } },
                        next
                    );
                }
            }, next),
            // Create Establishment
            (dep: { location: any, admin: any }, next: any) => {
                let photos = scEstablishment.photos;
                delete scEstablishment.photos
                scEstablishment.accountId = dep.admin.id;
                scEstablishment.locationId = dep.location.id;
                this.app.models.Establishment.create(scEstablishment, (err: Error, establishment: any) => {
                    hcEstablishment = establishment;
                    scEstablishment.photos = photos;
                    next(err);
                });
            }
        ], (err: Error) => next(err, hcEstablishment));
    }
    /**
     * @method updateEstablishment
     * @returns {void}
     * @description When a microservice finish with an area it will fire
     * the following event, that will recursively start the process
     * when pending areas are still in the stack.
     **/
    updateEstablishment(hcEstablishment: any, scEstablishment: any, next: any) {
        // Update establishment visits according social source data
        Object.keys(hcEstablishment.visits).forEach((key) => {
            if (!scEstablishment.visits.hasOwnProperty(key)) {
                hcEstablishment.visits[key] = scEstablishment.visits[key];
            }
        });
        let photos = scEstablishment.photos;
        delete scEstablishment.photos;
        hcEstablishment.updateAttributes(scEstablishment, (err: Error, hcEstablishment: any) => {
            scEstablishment.photos = photos;
            next(err, hcEstablishment);
        });
    }
    /**
     * @method persistGallery
     * @returns {void}
     * @description This method will find or create a container in google cloud storage.
     **/
    persistGallery(scEstablishment: any, hcEstablishment: any, next: any): void {
        if (process.env.NODE_ENV === 'testing') { return next(); }
        // Persisted Images Reference
        let gallery: any[] = [];
        // Find or Download Image
        async.eachSeries(scEstablishment.photos, (photo: any, next: any) => {
            let filename: string = photo.path.split('/').pop();
            console.log('Getting File: ', filename);
            this.app.models.Storage.getFile(this.containerName, filename, (err: Error, file: any) => {
                
                let establishmentPhoto: any = {
                    accountId: hcEstablishment.accountId,
                    establishmentId: hcEstablishment.id,
                    file: {
                        filename: filename,
                        path: `storages/heat-city-storage/download/${filename}`
                    }
                };
                if (err || !file) {
                    this.cloneFile(this.containerName, filename, photo.path, (err: Error) => {
                        if (err) {
                            // Don't want to break the flow because there is a broken image url for this venue.
                            // Though we won't save an EstablishmentPhoto instance neither.
                            next();
                        } else {
                            this.app.models.EstablishmentPhoto.create(establishmentPhoto, next);
                        }
                    })
                } else {
                    console.log('Finding or Creating Establishment Photo');
                    this.app.models.EstablishmentPhoto
                                   .findOrCreate(establishmentPhoto, establishmentPhoto, next);
                    
                }
            })
        }, next);
    }


    cloneFile(containerName: string, filename: string, filepath: string, next: any): void {
        request.head(filepath, (err: Error, res: any, body: any) => {
            if (err) {
                console.log('ERROR WHILE FETCHING FILE TO BE CLONED: ', err);
                return next();
            }
            let fullPath: any = path.join(this.root, filename);
            request(filepath).pipe(fs.createWriteStream(fullPath)).on('close', () => {
                if (err) {
                    return next(err);
                }
                this.uploadFile(containerName, filename, fullPath, next);
            });
        });
    }

    uploadFile(containerName: string, filename: string, fullPath: string, next: any): void {
        let uploadPath: string = `http://127.0.0.1:${this.app.get('port')}${this.app.get('restApiRoot')}/storages/heat-city-storage/upload`;
        console.log('TRYING TO UPLOAD:', uploadPath);
        request.post({
            url: uploadPath,
            formData: {
                attachments: [
                    fs.createReadStream(fullPath)
                ]
            }
        }, (err: any, httpResponse: any, body: any) => {
            if (err) {
                console.error('upload failed:', err);
                return next(err);
            }
            fs.unlink(fullPath, (err: Error) => next(err));
        })
    }
}

module.exports = Bridge;
