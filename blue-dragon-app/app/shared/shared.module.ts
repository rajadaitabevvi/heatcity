import { NgModule } from '@angular/core';
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { FriendsName } from './friends-name.pipe';
import { GradientDirective } from './gradient.directive';
import { DisconnectionComponent } from '../disconnection/disconnection.component';
import { DistanceFromMePipe } from "./pipes/distance.pipe";
import { FormatDistancePipe } from "./format.pipe";
import { PostCardComponent } from "./post-card/post-card.component";
import {
    LoadItemsPipe, FormatHourPipe,
    CategoryPipe, GenderTextPipe, CrowdPipe,
    TimeSpentPipe, AccountUrlPipe,
    EstablishmentUrlPipe, PriceRangePipe,
    CurrentVisitlPipe, DateFormat, TimeDiference,
    FirstNamePipe
} from './pipes';

//import { FormatDistancePipe} from './format.pipe';

@NgModule({
    imports: [
        NativeScriptModule
    ],
    
    exports: [
        GradientDirective,
        TimeSpentPipe,
        GenderTextPipe,
        CrowdPipe,
        AccountUrlPipe,
        EstablishmentUrlPipe,
        LoadItemsPipe,
        CategoryPipe,
        FormatHourPipe,
        PriceRangePipe,
        FriendsName,
        CurrentVisitlPipe,
        DateFormat,
        TimeDiference,
        FirstNamePipe,
        FormatDistancePipe,
        DistanceFromMePipe,
        PostCardComponent,
        DisconnectionComponent
    ],
    declarations: [
        //    FormatDistancePipe,
        GradientDirective,
        TimeSpentPipe,
        CrowdPipe,
        GenderTextPipe,
        AccountUrlPipe,
        EstablishmentUrlPipe,
        LoadItemsPipe,
        CategoryPipe,
        FormatHourPipe,
        PriceRangePipe,
        FriendsName,
        CurrentVisitlPipe,
        DateFormat,
        TimeDiference,
        FirstNamePipe,
        FormatDistancePipe,
        DistanceFromMePipe,
        PostCardComponent,
        DisconnectionComponent,
    ],
    providers: [],
})
export class SharedModule { }
