export interface MenuInfoInterface {
    newNotifications?: number;
    enableNotifications?: boolean;
    enableHistory?: boolean;
    unconfirmedHistory?: number;
    totalCount?: number;
}
