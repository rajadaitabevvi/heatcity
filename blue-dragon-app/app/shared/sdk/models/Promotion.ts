/* tslint:disable */
import {
    Establishment
} from '../index';

declare var Object: any;
export interface PromotionInterface {
    "attachments"?: Array<any>;
    "name": string;
    "description": string;
    "startsAt"?: Date;
    "endsAt"?: Date;
    "amount": number;
    "areaName": string;
    "promoRadius": number;
    "ageRange": string;
    "deletedAt"?: Date;
    "targeted"?: boolean;
    "establishmentId": any;
    "clicks"?: number;
    "promotionVists"?: number;
    "active"?: boolean;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    establishment?: Establishment;
}

export class Promotion implements PromotionInterface {
    "attachments": Array<any> = <any>[];
    "name": string = '';
    "description": string = '';
    "startsAt": Date = new Date();
    "endsAt": Date = new Date();
    "amount": number = 0;
    "areaName": string = '';
    "promoRadius": number = 0;
    "ageRange": string = '';
    "deletedAt": Date = new Date();
    "targeted": boolean = true;
    "establishmentId": any = <any>null;
    "clicks": number = 0;
    "promotionVists": number = 0;
    "active": boolean = false;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    establishment: Establishment = null;
    constructor(data?: PromotionInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Promotion`.
     */
    public static getModelName() {
        return "Promotion";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Promotion for dynamic purposes.
    **/
    public static factory(data: PromotionInterface): Promotion {
        return new Promotion(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Promotion',
            plural: 'promotions',
            path: 'promotions',
            idName: 'id',
            properties: {
                "attachments": {
                    name: 'attachments',
                    type: 'Array&lt;any&gt;'
                },
                "name": {
                    name: 'name',
                    type: 'string'
                },
                "description": {
                    name: 'description',
                    type: 'string'
                },
                "startsAt": {
                    name: 'startsAt',
                    type: 'Date'
                },
                "endsAt": {
                    name: 'endsAt',
                    type: 'Date'
                },
                "amount": {
                    name: 'amount',
                    type: 'number'
                },
                "areaName": {
                    name: 'areaName',
                    type: 'string'
                },
                "promoRadius": {
                    name: 'promoRadius',
                    type: 'number'
                },
                "ageRange": {
                    name: 'ageRange',
                    type: 'string'
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "targeted": {
                    name: 'targeted',
                    type: 'boolean',
                    default: true
                },
                "establishmentId": {
                    name: 'establishmentId',
                    type: 'any'
                },
                "clicks": {
                    name: 'clicks',
                    type: 'number',
                    default: 0
                },
                "promotionVists": {
                    name: 'promotionVists',
                    type: 'number',
                    default: 0
                },
                "active": {
                    name: 'active',
                    type: 'boolean',
                    default: false
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
            },
            relations: {
                establishment: {
                    name: 'establishment',
                    type: 'Establishment',
                    model: 'Establishment',
                    relationType: 'belongsTo',
                    keyFrom: 'establishmentId',
                    keyTo: 'id'
                },
            }
        }
    }
}
