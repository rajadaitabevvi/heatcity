/* tslint:disable */
import {
    Account
} from '../index';

declare var Object: any;
export interface DeviceInterface {
    "uuid": string;
    "deletedAt"?: Date;
    "id"?: any;
    "accountId"?: any;
    "createdAt"?: Date;
    "updatedAt"?: Date;
    "bizAccountId"?: any;
    account?: Account;
}

export class Device implements DeviceInterface {
    "uuid": string = '';
    "deletedAt": Date = new Date();
    "id": any = <any>null;
    "accountId": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "bizAccountId": any = <any>null;
    account: Account = null;
    constructor(data?: DeviceInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Device`.
     */
    public static getModelName() {
        return "Device";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Device for dynamic purposes.
    **/
    public static factory(data: DeviceInterface): Device {
        return new Device(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Device',
            plural: 'devices',
            path: 'devices',
            idName: 'id',
            properties: {
                "uuid": {
                    name: 'uuid',
                    type: 'string'
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "bizAccountId": {
                    name: 'bizAccountId',
                    type: 'any'
                },
            },
            relations: {
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
            }
        }
    }
}
