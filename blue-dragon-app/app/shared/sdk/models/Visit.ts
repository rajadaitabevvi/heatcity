/* tslint:disable */
import {
    Account,
    LocationActivity,
    Location,
    Like,
    Comment
} from '../index';

declare var Object: any;
export interface VisitInterface {
    "geoLocation"?: any;
    "weather"?: any;
    "validity"?: number;
    "entry"?: Date;
    "exit"?: Date;
    "notified"?: boolean;
    "deleted"?: boolean;
    "deletedAt"?: Date;
    "accountId": any;
    "state"?: string;
    "locationId": any;
    "ownerNotified"?: boolean;
    "visible"?: number;
    "points"?: number;
    "altitude"?: number;
    "speed"?: number;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    "bizAccountId"?: any;
    account?: Account;
    locationActivities?: LocationActivity[];
    location?: Location;
    likes?: Like[];
    comments?: Comment[];
}

export class Visit implements VisitInterface {
    "geoLocation": any = <any>null;
    "weather": any = <any>null;
    "validity": number = 0;
    "entry": Date = new Date();
    "exit": Date = null;
    "notified": boolean = false;
    "deleted": boolean = false;
    "deletedAt": Date = new Date();
    "accountId": any = <any>null;
    "state": string = '';
    "locationId": any = <any>null;
    "ownerNotified": boolean = false;
    "visible": number = 0;
    "points": number = 5;
    "altitude": number = 0;
    "speed": number = 0;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "bizAccountId": any = <any>null;
    account: Account = null;
    locationActivities: LocationActivity[] = null;
    location: Location = null;
    likes: Like[] = null;
    comments: Comment[] = null;
    constructor(data?: VisitInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Visit`.
     */
    public static getModelName() {
        return "Visit";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Visit for dynamic purposes.
    **/
    public static factory(data: VisitInterface): Visit {
        return new Visit(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Visit',
            plural: 'visits',
            path: 'visits',
            idName: 'id',
            properties: {
                "geoLocation": {
                    name: 'geoLocation',
                    type: 'any'
                },
                "weather": {
                    name: 'weather',
                    type: 'any',
                    default: <any>null
                },
                "validity": {
                    name: 'validity',
                    type: 'number'
                },
                "entry": {
                    name: 'entry',
                    type: 'Date'
                },
                "exit": {
                    name: 'exit',
                    type: 'Date'
                },
                "notified": {
                    name: 'notified',
                    type: 'boolean',
                    default: false
                },
                "deleted": {
                    name: 'deleted',
                    type: 'boolean',
                    default: false
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "state": {
                    name: 'state',
                    type: 'string'
                },
                "locationId": {
                    name: 'locationId',
                    type: 'any'
                },
                "ownerNotified": {
                    name: 'ownerNotified',
                    type: 'boolean',
                    default: false
                },
                "visible": {
                    name: 'visible',
                    type: 'number',
                    default: 0
                },
                "points": {
                    name: 'points',
                    type: 'number',
                    default: 5
                },
                "altitude": {
                    name: 'altitude',
                    type: 'number'
                },
                "speed": {
                    name: 'speed',
                    type: 'number'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "bizAccountId": {
                    name: 'bizAccountId',
                    type: 'any'
                },
            },
            relations: {
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
                locationActivities: {
                    name: 'locationActivities',
                    type: 'LocationActivity[]',
                    model: 'LocationActivity',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'visitId'
                },
                location: {
                    name: 'location',
                    type: 'Location',
                    model: 'Location',
                    relationType: 'belongsTo',
                    keyFrom: 'locationId',
                    keyTo: 'id'
                },
                likes: {
                    name: 'likes',
                    type: 'Like[]',
                    model: 'Like',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'visitId'
                },
                comments: {
                    name: 'comments',
                    type: 'Comment[]',
                    model: 'Comment',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'visitId'
                },
            }
        }
    }
}
