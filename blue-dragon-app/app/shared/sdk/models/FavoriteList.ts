/* tslint:disable */
import {
    Share,
    Account,
    Favorite,
    FavoriteListFollower
} from '../index';

declare var Object: any;
export interface FavoriteListInterface {
    "name": string;
    "public": boolean;
    "followersCount": number;
    "deletedAt"?: Date;
    "active": boolean;
    "accountId": any;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    "bizAccountId"?: any;
    shares?: Share[];
    account?: Account;
    favorites?: Favorite[];
    followers?: FavoriteListFollower[];
}

export class FavoriteList implements FavoriteListInterface {
    "name": string = '';
    "public": boolean = true;
    "followersCount": number = 0;
    "deletedAt": Date = new Date();
    "active": boolean = false;
    "accountId": any = <any>null;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "bizAccountId": any = <any>null;
    shares: Share[] = null;
    account: Account = null;
    favorites: Favorite[] = null;
    followers: FavoriteListFollower[] = null;
    constructor(data?: FavoriteListInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `FavoriteList`.
     */
    public static getModelName() {
        return "FavoriteList";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of FavoriteList for dynamic purposes.
    **/
    public static factory(data: FavoriteListInterface): FavoriteList {
        return new FavoriteList(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'FavoriteList',
            plural: 'favoriteLists',
            path: 'favoriteLists',
            idName: 'id',
            properties: {
                "name": {
                    name: 'name',
                    type: 'string'
                },
                "public": {
                    name: 'public',
                    type: 'boolean',
                    default: true
                },
                "followersCount": {
                    name: 'followersCount',
                    type: 'number'
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "active": {
                    name: 'active',
                    type: 'boolean',
                    default: false
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "bizAccountId": {
                    name: 'bizAccountId',
                    type: 'any'
                },
            },
            relations: {
                shares: {
                    name: 'shares',
                    type: 'Share[]',
                    model: 'Share',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'favoriteListId'
                },
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
                favorites: {
                    name: 'favorites',
                    type: 'Favorite[]',
                    model: 'Favorite',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'favoriteListId'
                },
                followers: {
                    name: 'followers',
                    type: 'FavoriteListFollower[]',
                    model: 'FavoriteListFollower',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'favoriteListId'
                },
            }
        }
    }
}
