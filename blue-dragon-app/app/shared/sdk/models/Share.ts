/* tslint:disable */
import {
    Establishment,
    Account,
    FavoriteList
} from '../index';

declare var Object: any;
export interface ShareInterface {
    "deletedAt"?: Date;
    "accountId": any;
    "friendId": any;
    "establishmentId": any;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    "favoriteListId"?: any;
    "bizAccountId"?: any;
    establishment?: Establishment;
    account?: Account;
    friend?: Account;
    favoriteList?: FavoriteList;
}

export class Share implements ShareInterface {
    "deletedAt": Date = new Date();
    "accountId": any = <any>null;
    "friendId": any = <any>null;
    "establishmentId": any = <any>null;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "favoriteListId": any = <any>null;
    "bizAccountId": any = <any>null;
    establishment: Establishment = null;
    account: Account = null;
    friend: Account = null;
    favoriteList: FavoriteList = null;
    constructor(data?: ShareInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Share`.
     */
    public static getModelName() {
        return "Share";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Share for dynamic purposes.
    **/
    public static factory(data: ShareInterface): Share {
        return new Share(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Share',
            plural: 'shares',
            path: 'shares',
            idName: 'id',
            properties: {
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "friendId": {
                    name: 'friendId',
                    type: 'any'
                },
                "establishmentId": {
                    name: 'establishmentId',
                    type: 'any'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "favoriteListId": {
                    name: 'favoriteListId',
                    type: 'any'
                },
                "bizAccountId": {
                    name: 'bizAccountId',
                    type: 'any'
                },
            },
            relations: {
                establishment: {
                    name: 'establishment',
                    type: 'Establishment',
                    model: 'Establishment',
                    relationType: 'belongsTo',
                    keyFrom: 'establishmentId',
                    keyTo: 'id'
                },
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
                friend: {
                    name: 'friend',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'friendId',
                    keyTo: 'id'
                },
                favoriteList: {
                    name: 'favoriteList',
                    type: 'FavoriteList',
                    model: 'FavoriteList',
                    relationType: 'belongsTo',
                    keyFrom: 'favoriteListId',
                    keyTo: 'id'
                },
            }
        }
    }
}
