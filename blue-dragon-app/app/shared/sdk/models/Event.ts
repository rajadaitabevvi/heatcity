/* tslint:disable */
import {
    Establishment,
    Account
} from '../index';

declare var Object: any;
export interface EventInterface {
    "name": string;
    "description": string;
    "deletedAt"?: Date;
    "accountId": any;
    "establishmentId": any;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    "bizAccountId"?: any;
    establishment?: Establishment;
    account?: Account;
}

export class Event implements EventInterface {
    "name": string = '';
    "description": string = '';
    "deletedAt": Date = new Date();
    "accountId": any = <any>null;
    "establishmentId": any = <any>null;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "bizAccountId": any = <any>null;
    establishment: Establishment = null;
    account: Account = null;
    constructor(data?: EventInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Event`.
     */
    public static getModelName() {
        return "Event";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Event for dynamic purposes.
    **/
    public static factory(data: EventInterface): Event {
        return new Event(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Event',
            plural: 'events',
            path: 'events',
            idName: 'id',
            properties: {
                "name": {
                    name: 'name',
                    type: 'string'
                },
                "description": {
                    name: 'description',
                    type: 'string'
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "establishmentId": {
                    name: 'establishmentId',
                    type: 'any'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "bizAccountId": {
                    name: 'bizAccountId',
                    type: 'any'
                },
            },
            relations: {
                establishment: {
                    name: 'establishment',
                    type: 'Establishment',
                    model: 'Establishment',
                    relationType: 'belongsTo',
                    keyFrom: 'establishmentId',
                    keyTo: 'id'
                },
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
            }
        }
    }
}
