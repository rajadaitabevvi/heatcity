/* tslint:disable */
import {
    Establishment,
    GeoPoint
} from '../index';

declare var Object: any;
export interface AreaInterface {
    "name"?: string;
    "placeId"?: string;
    "pulledAt"?: Date;
    "deletedAt"?: Date;
    "geogrid"?: Array<any>;
    "center"?: GeoPoint;
    "southWest"?: GeoPoint;
    "northEast"?: GeoPoint;
    "errorOnVertex"?: any;
    "processing"?: boolean;
    "radius"?: number;
    "updates"?: number;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    establishments?: Establishment[];
}

export class Area implements AreaInterface {
    "name": string = '';
    "placeId": string = '';
    "pulledAt": Date = new Date();
    "deletedAt": Date = new Date();
    "geogrid": Array<any> = <any>[];
    "center": GeoPoint = <any>null;
    "southWest": GeoPoint = <any>null;
    "northEast": GeoPoint = <any>null;
    "errorOnVertex": any = <any>null;
    "processing": boolean = false;
    "radius": number = 0;
    "updates": number = 0;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    establishments: Establishment[] = null;
    constructor(data?: AreaInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Area`.
     */
    public static getModelName() {
        return "Area";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Area for dynamic purposes.
    **/
    public static factory(data: AreaInterface): Area {
        return new Area(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Area',
            plural: 'Areas',
            path: 'Areas',
            idName: 'id',
            properties: {
                "name": {
                    name: 'name',
                    type: 'string'
                },
                "placeId": {
                    name: 'placeId',
                    type: 'string'
                },
                "pulledAt": {
                    name: 'pulledAt',
                    type: 'Date'
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "geogrid": {
                    name: 'geogrid',
                    type: 'Array&lt;any&gt;'
                },
                "center": {
                    name: 'center',
                    type: 'GeoPoint'
                },
                "southWest": {
                    name: 'southWest',
                    type: 'GeoPoint'
                },
                "northEast": {
                    name: 'northEast',
                    type: 'GeoPoint'
                },
                "errorOnVertex": {
                    name: 'errorOnVertex',
                    type: 'any'
                },
                "processing": {
                    name: 'processing',
                    type: 'boolean',
                    default: false
                },
                "radius": {
                    name: 'radius',
                    type: 'number'
                },
                "updates": {
                    name: 'updates',
                    type: 'number',
                    default: 0
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
            },
            relations: {
                establishments: {
                    name: 'establishments',
                    type: 'Establishment[]',
                    model: 'Establishment',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'areaId'
                },
            }
        }
    }
}
