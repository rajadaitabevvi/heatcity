/* tslint:disable */
import {
    Visit,
    Account
} from '../index';

declare var Object: any;
export interface LikeInterface {
    "accountId"?: any;
    "visitId": any;
    "like"?: boolean;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    visit?: Visit;
    account?: Account;
}

export class Like implements LikeInterface {
    "accountId": any = <any>null;
    "visitId": any = <any>null;
    "like": boolean = false;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    visit: Visit = null;
    account: Account = null;
    constructor(data?: LikeInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Like`.
     */
    public static getModelName() {
        return "Like";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Like for dynamic purposes.
    **/
    public static factory(data: LikeInterface): Like {
        return new Like(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Like',
            plural: 'likes',
            path: 'likes',
            idName: 'id',
            properties: {
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "visitId": {
                    name: 'visitId',
                    type: 'any'
                },
                "like": {
                    name: 'like',
                    type: 'boolean',
                    default: false
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
            },
            relations: {
                visit: {
                    name: 'visit',
                    type: 'Visit',
                    model: 'Visit',
                    relationType: 'belongsTo',
                    keyFrom: 'visitId',
                    keyTo: 'id'
                },
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
            }
        }
    }
}
