/* tslint:disable */

declare var Object: any;
export interface InviteeInterface {
    "name": string;
    "email": string;
    "hcUser"?: boolean;
    "attended": boolean;
    "accountId"?: any;
    "promotionId": any;
    "establishmentId": any;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
}

export class Invitee implements InviteeInterface {
    "name": string = '';
    "email": string = '';
    "hcUser": boolean = true;
    "attended": boolean = false;
    "accountId": any = <any>null;
    "promotionId": any = <any>null;
    "establishmentId": any = <any>null;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    constructor(data?: InviteeInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Invitee`.
     */
    public static getModelName() {
        return "Invitee";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Invitee for dynamic purposes.
    **/
    public static factory(data: InviteeInterface): Invitee {
        return new Invitee(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Invitee',
            plural: 'invitees',
            path: 'invitees',
            idName: 'id',
            properties: {
                "name": {
                    name: 'name',
                    type: 'string'
                },
                "email": {
                    name: 'email',
                    type: 'string'
                },
                "hcUser": {
                    name: 'hcUser',
                    type: 'boolean',
                    default: true
                },
                "attended": {
                    name: 'attended',
                    type: 'boolean',
                    default: false
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "promotionId": {
                    name: 'promotionId',
                    type: 'any'
                },
                "establishmentId": {
                    name: 'establishmentId',
                    type: 'any'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
            },
            relations: {
            }
        }
    }
}
