/* tslint:disable */
import {
    Account,
    Friend,
    Share,
    Establishment,
    Visit,
    Like
} from '../index';

declare var Object: any;
export interface NotificationInterface {
    "text"?: string;
    "viewed"?: boolean;
    "type": string;
    "id"?: any;
    "ownerId"?: any;
    "friendId"?: any;
    "shareId"?: any;
    "establishmentId"?: any;
    "visitId"?: any;
    "accountId"?: any;
    "createdAt"?: Date;
    "updatedAt"?: Date;
    "likeId"?: any;
    owner?: Account;
    friend?: Friend;
    share?: Share;
    establishment?: Establishment;
    visit?: Visit;
    account?: Account;
    like?: Like;
}

export class Notification implements NotificationInterface {
    "text": string = '';
    "viewed": boolean = false;
    "type": string = '';
    "id": any = <any>null;
    "ownerId": any = <any>null;
    "friendId": any = <any>null;
    "shareId": any = <any>null;
    "establishmentId": any = <any>null;
    "visitId": any = <any>null;
    "accountId": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "likeId": any = <any>null;
    owner: Account = null;
    friend: Friend = null;
    share: Share = null;
    establishment: Establishment = null;
    visit: Visit = null;
    account: Account = null;
    like: Like = null;
    constructor(data?: NotificationInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Notification`.
     */
    public static getModelName() {
        return "Notification";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Notification for dynamic purposes.
    **/
    public static factory(data: NotificationInterface): Notification {
        return new Notification(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Notification',
            plural: 'notifications',
            path: 'notifications',
            idName: 'id',
            properties: {
                "text": {
                    name: 'text',
                    type: 'string'
                },
                "viewed": {
                    name: 'viewed',
                    type: 'boolean',
                    default: false
                },
                "type": {
                    name: 'type',
                    type: 'string'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "ownerId": {
                    name: 'ownerId',
                    type: 'any'
                },
                "friendId": {
                    name: 'friendId',
                    type: 'any'
                },
                "shareId": {
                    name: 'shareId',
                    type: 'any'
                },
                "establishmentId": {
                    name: 'establishmentId',
                    type: 'any'
                },
                "visitId": {
                    name: 'visitId',
                    type: 'any'
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "likeId": {
                    name: 'likeId',
                    type: 'any'
                },
            },
            relations: {
                owner: {
                    name: 'owner',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'ownerId',
                    keyTo: 'id'
                },
                friend: {
                    name: 'friend',
                    type: 'Friend',
                    model: 'Friend',
                    relationType: 'belongsTo',
                    keyFrom: 'friendId',
                    keyTo: 'id'
                },
                share: {
                    name: 'share',
                    type: 'Share',
                    model: 'Share',
                    relationType: 'belongsTo',
                    keyFrom: 'shareId',
                    keyTo: 'id'
                },
                establishment: {
                    name: 'establishment',
                    type: 'Establishment',
                    model: 'Establishment',
                    relationType: 'belongsTo',
                    keyFrom: 'establishmentId',
                    keyTo: 'id'
                },
                visit: {
                    name: 'visit',
                    type: 'Visit',
                    model: 'Visit',
                    relationType: 'belongsTo',
                    keyFrom: 'visitId',
                    keyTo: 'id'
                },
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
                like: {
                    name: 'like',
                    type: 'Like',
                    model: 'Like',
                    relationType: 'belongsTo',
                    keyFrom: 'likeId',
                    keyTo: 'id'
                },
            }
        }
    }
}
