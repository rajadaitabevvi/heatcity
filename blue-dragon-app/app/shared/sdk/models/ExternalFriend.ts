/* tslint:disable */
import {
    Account
} from '../index';

declare var Object: any;
export interface ExternalFriendInterface {
    "provider"?: string;
    "deletedAt"?: Date;
    "accountId": any;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    "bizAccountId"?: any;
    account?: Account;
}

export class ExternalFriend implements ExternalFriendInterface {
    "provider": string = '';
    "deletedAt": Date = new Date();
    "accountId": any = <any>null;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "bizAccountId": any = <any>null;
    account: Account = null;
    constructor(data?: ExternalFriendInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `ExternalFriend`.
     */
    public static getModelName() {
        return "ExternalFriend";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of ExternalFriend for dynamic purposes.
    **/
    public static factory(data: ExternalFriendInterface): ExternalFriend {
        return new ExternalFriend(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'ExternalFriend',
            plural: 'externalFriends',
            path: 'externalFriends',
            idName: 'id',
            properties: {
                "provider": {
                    name: 'provider',
                    type: 'string'
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "bizAccountId": {
                    name: 'bizAccountId',
                    type: 'any'
                },
            },
            relations: {
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
            }
        }
    }
}
