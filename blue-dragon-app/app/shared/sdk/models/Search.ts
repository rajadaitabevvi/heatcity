/* tslint:disable */
import {
    Establishment,
    BizAccount
} from '../index';

declare var Object: any;
export interface SearchInterface {
    "deleted"?: boolean;
    "deletedAt"?: Date;
    "establishmentId": any;
    "bizaccountId": any;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    establishment?: Establishment;
    bizaccount?: BizAccount;
}

export class Search implements SearchInterface {
    "deleted": boolean = false;
    "deletedAt": Date = new Date();
    "establishmentId": any = <any>null;
    "bizaccountId": any = <any>null;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    establishment: Establishment = null;
    bizaccount: BizAccount = null;
    constructor(data?: SearchInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Search`.
     */
    public static getModelName() {
        return "Search";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Search for dynamic purposes.
    **/
    public static factory(data: SearchInterface): Search {
        return new Search(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Search',
            plural: 'searches',
            path: 'searches',
            idName: 'id',
            properties: {
                "deleted": {
                    name: 'deleted',
                    type: 'boolean',
                    default: false
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "establishmentId": {
                    name: 'establishmentId',
                    type: 'any'
                },
                "bizaccountId": {
                    name: 'bizaccountId',
                    type: 'any'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
            },
            relations: {
                establishment: {
                    name: 'establishment',
                    type: 'Establishment',
                    model: 'Establishment',
                    relationType: 'belongsTo',
                    keyFrom: 'establishmentId',
                    keyTo: 'id'
                },
                bizaccount: {
                    name: 'bizaccount',
                    type: 'BizAccount',
                    model: 'BizAccount',
                    relationType: 'belongsTo',
                    keyFrom: 'bizaccountId',
                    keyTo: 'id'
                },
            }
        }
    }
}
