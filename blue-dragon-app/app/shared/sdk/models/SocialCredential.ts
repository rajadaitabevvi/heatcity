/* tslint:disable */
import {
    Account
} from '../index';

declare var Object: any;
export interface SocialCredentialInterface {
    "facebookId": string;
    "id"?: any;
    "accountId"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    "bizAccountId"?: any;
    account?: Account;
}

export class SocialCredential implements SocialCredentialInterface {
    "facebookId": string = '';
    "id": any = <any>null;
    "accountId": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "bizAccountId": any = <any>null;
    account: Account = null;
    constructor(data?: SocialCredentialInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `SocialCredential`.
     */
    public static getModelName() {
        return "SocialCredential";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of SocialCredential for dynamic purposes.
    **/
    public static factory(data: SocialCredentialInterface): SocialCredential {
        return new SocialCredential(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'SocialCredential',
            plural: 'socialCredentials',
            path: 'socialCredentials',
            idName: 'id',
            properties: {
                "facebookId": {
                    name: 'facebookId',
                    type: 'string'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "bizAccountId": {
                    name: 'bizAccountId',
                    type: 'any'
                },
            },
            relations: {
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
            }
        }
    }
}
