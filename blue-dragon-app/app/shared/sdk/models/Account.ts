/* tslint:disable */
import {
    Friend,
    ExternalFriend,
    Establishment,
    EstablishmentPhoto,
    Event,
    Visit,
    MusicList,
    Share,
    FavoriteList,
    Favorite,
    FavoriteListFollower,
    ChatChannel,
    Message,
    SocialCredential,
    Device,
    Notification,
    GeoPoint
} from '../index';

declare var Object: any;
export interface AccountInterface {
    "firstName"?: string;
    "lastName"?: string;
    "middleName"?: string;
    "displayName"?: string;
    "photo"?: any;
    "fbHometown"?: any;
    "gender"?: string;
    "ageRange"?: number;
    "hometown"?: string;
    "homecountry"?: string;
    "code"?: string;
    "invitedBy"?: string;
    "exclutions"?: Array<any>;
    "fbFriendList"?: Array<any>;
    "fbFavoriteTeams"?: Array<any>;
    "location"?: GeoPoint;
    "settings"?: any;
    "deletedAt"?: Date;
    "approved"?: boolean;
    "bio"?: string;
    "realm"?: string;
    "username"?: string;
    "challenges"?: any;
    "email": string;
    "emailVerified"?: boolean;
    "status"?: string;
    "created"?: Date;
    "lastUpdated"?: Date;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    "password"?: string;
    accessTokens?: any[];
    friends?: Friend[];
    externalFriends?: ExternalFriend[];
    establishments?: Establishment[];
    establishmentPhotos?: EstablishmentPhoto[];
    events?: Event[];
    visits?: Visit[];
    musicLists?: MusicList[];
    shared?: Share[];
    shares?: Share[];
    favoriteLists?: FavoriteList[];
    favorites?: Favorite[];
    followedLists?: FavoriteListFollower[];
    chatChannels?: ChatChannel[];
    messages?: Message[];
    socialCredentials?: SocialCredential[];
    devices?: Device[];
    notifications?: Notification[];
}

export class Account implements AccountInterface {
    "firstName": string = '';
    "lastName": string = '';
    "middleName": string = '';
    "displayName": string = '';
    "photo": any = <any>null;
    "fbHometown": any = <any>null;
    "gender": string = '';
    "ageRange": number = 0;
    "hometown": string = '';
    "homecountry": string = '';
    "code": string = '';
    "invitedBy": string = '';
    "exclutions": Array<any> = <any>[];
    "fbFriendList": Array<any> = <any>[];
    "fbFavoriteTeams": Array<any> = <any>[];
    "location": GeoPoint = <any>null;
    "settings": any = <any>null;
    "deletedAt": Date = new Date();
    "approved": boolean = false;
    "bio": string = '';
    "realm": string = '';
    "username": string = '';
    "challenges": any = <any>null;
    "email": string = '';
    "emailVerified": boolean = false;
    "status": string = '';
    "created": Date = new Date();
    "lastUpdated": Date = new Date();
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "password": string = '';
    accessTokens: any[] = null;
    friends: Friend[] = null;
    externalFriends: ExternalFriend[] = null;
    establishments: Establishment[] = null;
    establishmentPhotos: EstablishmentPhoto[] = null;
    events: Event[] = null;
    visits: Visit[] = null;
    musicLists: MusicList[] = null;
    shared: Share[] = null;
    shares: Share[] = null;
    favoriteLists: FavoriteList[] = null;
    favorites: Favorite[] = null;
    followedLists: FavoriteListFollower[] = null;
    chatChannels: ChatChannel[] = null;
    messages: Message[] = null;
    socialCredentials: SocialCredential[] = null;
    devices: Device[] = null;
    notifications: Notification[] = null;
    constructor(data?: AccountInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Account`.
     */
    public static getModelName() {
        return "Account";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Account for dynamic purposes.
    **/
    public static factory(data: AccountInterface): Account {
        return new Account(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Account',
            plural: 'accounts',
            path: 'accounts',
            idName: 'id',
            properties: {
                "firstName": {
                    name: 'firstName',
                    type: 'string'
                },
                "lastName": {
                    name: 'lastName',
                    type: 'string'
                },
                "middleName": {
                    name: 'middleName',
                    type: 'string'
                },
                "displayName": {
                    name: 'displayName',
                    type: 'string'
                },
                "photo": {
                    name: 'photo',
                    type: 'any'
                },
                "fbHometown": {
                    name: 'fbHometown',
                    type: 'any'
                },
                "gender": {
                    name: 'gender',
                    type: 'string'
                },
                "ageRange": {
                    name: 'ageRange',
                    type: 'number'
                },
                "hometown": {
                    name: 'hometown',
                    type: 'string'
                },
                "homecountry": {
                    name: 'homecountry',
                    type: 'string'
                },
                "code": {
                    name: 'code',
                    type: 'string'
                },
                "invitedBy": {
                    name: 'invitedBy',
                    type: 'string'
                },
                "exclutions": {
                    name: 'exclutions',
                    type: 'Array&lt;any&gt;'
                },
                "fbFriendList": {
                    name: 'fbFriendList',
                    type: 'Array&lt;any&gt;'
                },
                "fbFavoriteTeams": {
                    name: 'fbFavoriteTeams',
                    type: 'Array&lt;any&gt;'
                },
                "location": {
                    name: 'location',
                    type: 'GeoPoint'
                },
                "settings": {
                    name: 'settings',
                    type: 'any'
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "approved": {
                    name: 'approved',
                    type: 'boolean',
                    default: false
                },
                "bio": {
                    name: 'bio',
                    type: 'string'
                },
                "realm": {
                    name: 'realm',
                    type: 'string'
                },
                "username": {
                    name: 'username',
                    type: 'string'
                },
                "credentials": {
                    name: 'credentials',
                    type: 'any'
                },
                "challenges": {
                    name: 'challenges',
                    type: 'any'
                },
                "email": {
                    name: 'email',
                    type: 'string'
                },
                "emailVerified": {
                    name: 'emailVerified',
                    type: 'boolean'
                },
                "status": {
                    name: 'status',
                    type: 'string'
                },
                "created": {
                    name: 'created',
                    type: 'Date'
                },
                "lastUpdated": {
                    name: 'lastUpdated',
                    type: 'Date'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "password": {
                    name: 'password',
                    type: 'string'
                },
            },
            relations: {
                accessTokens: {
                    name: 'accessTokens',
                    type: 'any[]',
                    model: '',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'userId'
                },
                friends: {
                    name: 'friends',
                    type: 'Friend[]',
                    model: 'Friend',
                    relationType: 'hasMany',
                    modelThrough: 'Friend',
                    keyThrough: 'friendId',
                    keyFrom: 'id',
                    keyTo: 'accountId'
                },
                externalFriends: {
                    name: 'externalFriends',
                    type: 'ExternalFriend[]',
                    model: 'ExternalFriend',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'accountId'
                },
                establishments: {
                    name: 'establishments',
                    type: 'Establishment[]',
                    model: 'Establishment',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'accountId'
                },
                establishmentPhotos: {
                    name: 'establishmentPhotos',
                    type: 'EstablishmentPhoto[]',
                    model: 'EstablishmentPhoto',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'accountId'
                },
                events: {
                    name: 'events',
                    type: 'Event[]',
                    model: 'Event',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'accountId'
                },
                visits: {
                    name: 'visits',
                    type: 'Visit[]',
                    model: 'Visit',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'accountId'
                },
                musicLists: {
                    name: 'musicLists',
                    type: 'MusicList[]',
                    model: 'MusicList',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'accountId'
                },
                shared: {
                    name: 'shared',
                    type: 'Share[]',
                    model: 'Share',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'accountId'
                },
                shares: {
                    name: 'shares',
                    type: 'Share[]',
                    model: 'Share',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'friendId'
                },
                favoriteLists: {
                    name: 'favoriteLists',
                    type: 'FavoriteList[]',
                    model: 'FavoriteList',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'accountId'
                },
                favorites: {
                    name: 'favorites',
                    type: 'Favorite[]',
                    model: 'Favorite',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'accountId'
                },
                followedLists: {
                    name: 'followedLists',
                    type: 'FavoriteListFollower[]',
                    model: 'FavoriteListFollower',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'accountId'
                },
                chatChannels: {
                    name: 'chatChannels',
                    type: 'ChatChannel[]',
                    model: 'ChatChannel',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'accountId'
                },
                messages: {
                    name: 'messages',
                    type: 'Message[]',
                    model: 'Message',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'accountId'
                },
                socialCredentials: {
                    name: 'socialCredentials',
                    type: 'SocialCredential[]',
                    model: 'SocialCredential',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'accountId'
                },
                devices: {
                    name: 'devices',
                    type: 'Device[]',
                    model: 'Device',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'accountId'
                },
                notifications: {
                    name: 'notifications',
                    type: 'Notification[]',
                    model: 'Notification',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'ownerId'
                },
            }
        }
    }
}
