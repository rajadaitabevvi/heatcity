/* tslint:disable */
import {
    Account,
    Promotion,
    EstablishmentPhoto,
    Event,
    Location,
    MusicList,
    Share,
    Favorite,
    Area,
    ParentOrg
} from '../index';

declare var Object: any;
export interface EstablishmentInterface {
    "name": string;
    "description"?: string;
    "email"?: string;
    "url"?: string;
    "pricing"?: number;
    "rating"?: number;
    "categories"?: Array<any>;
    "operatesAt"?: any;
    "popularAt"?: any;
    "deletedAt"?: Date;
    "accountId": any;
    "locationId": any;
    "visits"?: any;
    "geoLocation"?: any;
    "menu"?: any;
    "address"?: string;
    "hcCategory"?: string;
    "hcCategories"?: Array<any>;
    "hcRating"?: number;
    "deleted"?: boolean;
    "parentOrgId"?: any;
    "capacity"?: number;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    "areaId"?: any;
    "bizAccountId"?: any;
    account?: Account;
    promotions?: Promotion[];
    photos?: EstablishmentPhoto[];
    events?: Event[];
    location?: Location;
    musicLists?: MusicList[];
    shares?: Share[];
    favorites?: Favorite[];
    area?: Area;
    parentOrg?: ParentOrg;
}

export class Establishment implements EstablishmentInterface {
    "name": string = '';
    "description": string = '';
    "email": string = '';
    "url": string = '';
    "pricing": number = 0;
    "rating": number = 0;
    "categories": Array<any> = <any>[];
    "operatesAt": any = <any>null;
    "popularAt": any = <any>null;
    "deletedAt": Date = new Date();
    "accountId": any = <any>null;
    "locationId": any = <any>null;
    "visits": any = <any>null;
    "geoLocation": any = <any>null;
    "menu": any = <any>null;
    "address": string = '';
    "hcCategory": string = '';
    "hcCategories": Array<any> = <any>[];
    "hcRating": number = 0;
    "deleted": boolean = false;
    "parentOrgId": any = <any>null;
    "capacity": number = 0;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "areaId": any = <any>null;
    "bizAccountId": any = <any>null;
    account: Account = null;
    promotions: Promotion[] = null;
    photos: EstablishmentPhoto[] = null;
    events: Event[] = null;
    location: Location = null;
    musicLists: MusicList[] = null;
    shares: Share[] = null;
    favorites: Favorite[] = null;
    area: Area = null;
    parentOrg: ParentOrg = null;
    constructor(data?: EstablishmentInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Establishment`.
     */
    public static getModelName() {
        return "Establishment";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Establishment for dynamic purposes.
    **/
    public static factory(data: EstablishmentInterface): Establishment {
        return new Establishment(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Establishment',
            plural: 'establishments',
            path: 'establishments',
            idName: 'id',
            properties: {
                "name": {
                    name: 'name',
                    type: 'string'
                },
                "description": {
                    name: 'description',
                    type: 'string'
                },
                "email": {
                    name: 'email',
                    type: 'string'
                },
                "url": {
                    name: 'url',
                    type: 'string'
                },
                "pricing": {
                    name: 'pricing',
                    type: 'number'
                },
                "rating": {
                    name: 'rating',
                    type: 'number'
                },
                "categories": {
                    name: 'categories',
                    type: 'Array&lt;any&gt;'
                },
                "operatesAt": {
                    name: 'operatesAt',
                    type: 'any'
                },
                "popularAt": {
                    name: 'popularAt',
                    type: 'any'
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "locationId": {
                    name: 'locationId',
                    type: 'any'
                },
                "visits": {
                    name: 'visits',
                    type: 'any',
                    default: <any>null
                },
                "geoLocation": {
                    name: 'geoLocation',
                    type: 'any'
                },
                "menu": {
                    name: 'menu',
                    type: 'any',
                    default: <any>null
                },
                "address": {
                    name: 'address',
                    type: 'string'
                },
                "hcCategory": {
                    name: 'hcCategory',
                    type: 'string'
                },
                "hcCategories": {
                    name: 'hcCategories',
                    type: 'Array&lt;any&gt;'
                },
                "hcRating": {
                    name: 'hcRating',
                    type: 'number',
                    default: 0
                },
                "deleted": {
                    name: 'deleted',
                    type: 'boolean',
                    default: false
                },
                "parentOrgId": {
                    name: 'parentOrgId',
                    type: 'any'
                },
                "capacity": {
                    name: 'capacity',
                    type: 'number'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "areaId": {
                    name: 'areaId',
                    type: 'any'
                },
                "bizAccountId": {
                    name: 'bizAccountId',
                    type: 'any'
                },
            },
            relations: {
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
                promotions: {
                    name: 'promotions',
                    type: 'Promotion[]',
                    model: 'Promotion',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'establishmentId'
                },
                photos: {
                    name: 'photos',
                    type: 'EstablishmentPhoto[]',
                    model: 'EstablishmentPhoto',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'establishmentId'
                },
                events: {
                    name: 'events',
                    type: 'Event[]',
                    model: 'Event',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'establishmentId'
                },
                location: {
                    name: 'location',
                    type: 'Location',
                    model: 'Location',
                    relationType: 'belongsTo',
                    keyFrom: 'locationId',
                    keyTo: 'id'
                },
                musicLists: {
                    name: 'musicLists',
                    type: 'MusicList[]',
                    model: 'MusicList',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'establishmentId'
                },
                shares: {
                    name: 'shares',
                    type: 'Share[]',
                    model: 'Share',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'establishmentId'
                },
                favorites: {
                    name: 'favorites',
                    type: 'Favorite[]',
                    model: 'Favorite',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'establishmentId'
                },
                area: {
                    name: 'area',
                    type: 'Area',
                    model: 'Area',
                    relationType: 'belongsTo',
                    keyFrom: 'areaId',
                    keyTo: 'id'
                },
                parentOrg: {
                    name: 'parentOrg',
                    type: 'ParentOrg',
                    model: 'ParentOrg',
                    relationType: 'belongsTo',
                    keyFrom: 'parentOrgId',
                    keyTo: 'id'
                },
            }
        }
    }
}
