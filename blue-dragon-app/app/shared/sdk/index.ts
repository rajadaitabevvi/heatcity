/* tslint:disable */
/**
* @module SDKModule
* @author Jonathan Casarrubias <t:@johncasarrubias> <gh:jonathan-casarrubias>
* @license MIT 2016 Jonathan Casarrubias
* @version 2.1.0
* @description
* The SDKModule is a generated Software Development Kit automatically built by
* the LoopBack SDK Builder open source module.
*
* The SDKModule provides Angular 2 >= RC.5 support, which means that NgModules
* can import this Software Development Kit as follows:
*
*
* APP Route Module Context
* ============================================================================
* import { NgModule }       from '@angular/core';
* import { BrowserModule }  from '@angular/platform-browser';
* // App Root 
* import { AppComponent }   from './app.component';
* // Feature Modules
* import { SDK[Browser|Node|Native]Module } from './shared/sdk/sdk.module';
* // Import Routing
* import { routing }        from './app.routing';
* @NgModule({
*  imports: [
*    BrowserModule,
*    routing,
*    SDK[Browser|Node|Native]Module.forRoot()
*  ],
*  declarations: [ AppComponent ],
*  bootstrap:    [ AppComponent ]
* })
* export class AppModule { }
*
**/
import { JSONSearchParams } from './services/core/search.params';
import { ErrorHandler } from './services/core/error.service';
import { LoopBackAuth } from './services/core/auth.service';
import { LoggerService } from './services/custom/logger.service';
import { SDKModels } from './services/custom/SDKModels';
import { InternalStorage, SDKStorage } from './storage/storage.swaps';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { StorageNative } from './storage/storage.native';
import { SocketNative } from './sockets/socket.native';
import { SocketDriver } from './sockets/socket.driver';
import { SocketConnection } from './sockets/socket.connections';
import { RealTime } from './services/core/real.time';
import { UserApi } from './services/custom/User';
import { AccountApi } from './services/custom/Account';
import { FriendApi } from './services/custom/Friend';
import { ExternalFriendApi } from './services/custom/ExternalFriend';
import { EstablishmentApi } from './services/custom/Establishment';
import { PromotionApi } from './services/custom/Promotion';
import { EstablishmentPhotoApi } from './services/custom/EstablishmentPhoto';
import { EventApi } from './services/custom/Event';
import { VisitApi } from './services/custom/Visit';
import { ActivityCategoryApi } from './services/custom/ActivityCategory';
import { ActivityApi } from './services/custom/Activity';
import { LocationActivityApi } from './services/custom/LocationActivity';
import { LocationFixApi } from './services/custom/LocationFix';
import { LocationApi } from './services/custom/Location';
import { MusicListApi } from './services/custom/MusicList';
import { ShareApi } from './services/custom/Share';
import { FavoriteListApi } from './services/custom/FavoriteList';
import { FavoriteApi } from './services/custom/Favorite';
import { FavoriteListFollowerApi } from './services/custom/FavoriteListFollower';
import { ChatChannelApi } from './services/custom/ChatChannel';
import { MessageApi } from './services/custom/Message';
import { AreaApi } from './services/custom/Area';
import { StorageApi } from './services/custom/Storage';
import { SocialCredentialApi } from './services/custom/SocialCredential';
import { TwilioApi } from './services/custom/Twilio';
import { DeviceApi } from './services/custom/Device';
import { NotificationApi } from './services/custom/Notification';
import { LikeApi } from './services/custom/Like';
import { CommentApi } from './services/custom/Comment';
import { BizAccountApi } from './services/custom/BizAccount';
import { InviteeApi } from './services/custom/Invitee';
import { ParentOrgApi } from './services/custom/ParentOrg';
import { SearchApi } from './services/custom/Search';
/**
* @module SDKNativeModule
* @description
* This module should be imported when building a NativeScript Applications.
**/
@NgModule({
  imports:      [ CommonModule, HttpModule ],
  declarations: [ ],
  exports:      [ ],
  providers:    [
    ErrorHandler,
    SocketConnection
  ]
})
export class SDKNativeModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule  : SDKNativeModule,
      providers : [
        LoopBackAuth,
        LoggerService,
        JSONSearchParams,
        SDKModels,
        RealTime,
        UserApi,
        AccountApi,
        FriendApi,
        ExternalFriendApi,
        EstablishmentApi,
        PromotionApi,
        EstablishmentPhotoApi,
        EventApi,
        VisitApi,
        ActivityCategoryApi,
        ActivityApi,
        LocationActivityApi,
        LocationFixApi,
        LocationApi,
        MusicListApi,
        ShareApi,
        FavoriteListApi,
        FavoriteApi,
        FavoriteListFollowerApi,
        ChatChannelApi,
        MessageApi,
        AreaApi,
        StorageApi,
        SocialCredentialApi,
        TwilioApi,
        DeviceApi,
        NotificationApi,
        LikeApi,
        CommentApi,
        BizAccountApi,
        InviteeApi,
        ParentOrgApi,
        SearchApi,
        { provide: InternalStorage, useClass: StorageNative },
        { provide: SDKStorage, useClass: StorageNative },
        { provide: SocketDriver, useClass: SocketNative }
      ]
    };
  }
}
/**
* Have Fun!!!
* - Jon
**/
export * from './models/index';
export * from './services/index';
export * from './lb.config';

