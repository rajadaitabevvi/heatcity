import { Injectable, NgZone, Inject } from '@angular/core';
import { RouterExtensions } from "nativescript-angular/router";
import { Router } from "@angular/router";

export interface NavigateSettings {
    clearHistory?: boolean;
}

@Injectable()
export class NavigateService {

    constructor(
        @Inject(RouterExtensions) private router,
        @Inject(NgZone) private zone
    ) { }

    to(uri: string, settings?: NavigateSettings): void {
        this.zone.run(() => {
            if (settings && settings.clearHistory) {
                this.router.navigate([uri], {clearHistory: true});
            }else {
                this.router.navigate([uri]);
            }
        })
    }
}