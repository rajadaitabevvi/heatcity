import { Injectable, Inject } from '@angular/core';
import { ImageSource, fromFile, fromResource, fromNativeSource } from 'image-source';
import { FileSystemEntity, knownFolders, path } from 'file-system';
import { CategoryResourceService } from './category-resource.service';
import { Image } from 'ui/image';
@Injectable()
export class CategoriesMap{
    constructor(
        @Inject(CategoryResourceService) private categoryResourceService
    ) {}

    public getImageSource(categories: string[], highlight: boolean = false): Image {
        let markerSource: string = '';
        let prefix: string = highlight ? 'bmarker' : 'amarker';
        categories.forEach((category: string) => {
            markerSource = this.categoryResourceService.getResourceName(prefix, category);
            //markerSource = this.categoryResourceService.getResourceHcName(prefix, category);
        })
        //1-- console.log('MARKER SOURCES: ', markerSource);
        let icon: Image = new Image();
        icon.imageSource =  fromResource(markerSource);
 //      icon.borderRadius = 5;
 //       icon.width = 60;
 //       icon.height = 60;
 //       icon.className = 'img-circle';
        icon.stretch = 'fill';
        return icon;
    }
    
    public getHcImageSource(categories: string[], highlight: boolean = false): Image {
        let markerSource: string = '';
        let prefix: string = highlight ? 'bmarker' : 'amarker';
        markerSource = this.categoryResourceService.getResourceHcName(prefix, categories);
        
        console.log('MARKER SOURCES: ', markerSource);
        let icon: Image = new Image();
        icon.imageSource =  fromResource(markerSource);
 //      icon.borderRadius = 5;
 //       icon.width = 60;
 //       icon.height = 60;
 //       icon.className = 'img-circle';
        icon.stretch = 'fill';
        return icon;
    }
}
