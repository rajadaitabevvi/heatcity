import { Pipe, PipeTransform } from '@angular/core';
import { Account } from './sdk/models';

@Pipe({name: 'formatDistance'})
export class FormatDistancePipe implements PipeTransform {
    transform(distance: number, account: Account): string {
        if (this.isMile(account)) {
            let ft = this.metersToFt(distance);
            if (ft < 5280) {
                return `${ft.toFixed(1)} ft away`;
            } else { 
                let mi: number = ft / 5280;
                return `${mi.toFixed(1)} mi away`;
            }

        }else {
            if (distance < 1000) {
                return `${distance.toFixed(1)} m away`;
            } else { 
                let km: number = distance / 1000;
                return `${km.toFixed(1)} km away`;
            }
        }
    }

    metersToFt(meters: number): number {
        return meters * 3.28084;
    }

    isMile(account: Account): boolean {
        return (
            account &&
            account.settings &&
            account.settings.unit &&    
            account.settings.unit === 'miles'
        )
    }
}