import * as app from "application";
import {Observable} from 'rxjs/Observable'

export class KeyboardObserver {

    public heightChange$(): Observable<number> {

        return Observable.create((observer) => {

            const iosObserver = app.ios.addNotificationObserver(UIKeyboardWillChangeFrameNotification, (notification) => {
                const height = notification.userInfo.valueForKey(UIKeyboardFrameEndUserInfoKey).CGRectValue.size.height;
                observer.next(height);
            });

            return () => {
                app.ios.removeNotificationObserver(iosObserver, UIKeyboardWillChangeFrameNotification);
            }
        });
    }
}
