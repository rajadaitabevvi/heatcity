import { Pipe, PipeTransform } from '@angular/core';
import { BASE_URL, API_VERSION } from '../base.api';
import { DEFAULTS } from '../config';
import { Establishment } from '../sdk/models';

@Pipe({ name: 'establishmentUrl' })
export class EstablishmentUrlPipe implements PipeTransform {
    index: number;
    transform(establishment: Establishment, type: string, index: number): string {
        this.index = index || 0;
        let url: string;
        switch (type) {
            case 'background-img':
                url = `url('${this.getUrlFromEstablishment(establishment)}')`;
                break;
            default:
                url = this.getUrlFromEstablishment(establishment);
                break;
        }
        return url;
    }

    getUrlFromEstablishment(establishment: Establishment): string {
        let path = establishment && establishment.photos
            && establishment.photos[this.index] ?
            `${BASE_URL}/${API_VERSION}/${establishment.photos[this.index].file.path}` :
            this.getImageFromLocal(establishment);
        return path;
    }

    getImageFromLocal(establishment: Establishment): string {
        if (establishment.hcCategory === "Drinks" || establishment.hcCategories[0] === "Drinks") {
            let path = "res://default_drink"
            return path;
        } else if (establishment.hcCategory === "Dance" || establishment.hcCategories[0] === "Dance") {
            let path = "res://default_dance"
            return path;
        } else if (establishment.hcCategory === "Dinner" || establishment.hcCategories[0] === "Dinner") {
            let path = "res://default_dinner"
            return path;
        }else{
            return '';
        }

    }
}