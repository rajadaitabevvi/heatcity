import { Pipe, PipeTransform } from '@angular/core';
import { Account } from '../sdk/models';
@Pipe({name: 'firstName'})
export class FirstNamePipe implements PipeTransform {
    transform(account: Account, type: string): string {
        let result = '';
        if (type === 'firstNameSmall') {
            result = account.firstName.split(' ')[0];
            console.log('smaller name: ', result);
        }else if(account.firstName) {
            result = account.firstName;
        } else if(account.displayName) {
            result = account.displayName.split(' ')[0];
        } else if(account.username) {
            result = account.username.split(' ')[0];
        } 
        return result;
    }
}