import { Pipe, PipeTransform } from '@angular/core';
import { StatsInterface } from '../interfaces';

@Pipe({name: 'crowd'})
export class CrowdPipe implements PipeTransform {
    /*
    *   This pipe allow parse the information of crowd to display
    *   brief description based on the value.
    */
    transform(stats: StatsInterface, type: string): string {
        let labelText: string;
        switch (type) {
            case 'percent':
                labelText = this.formatCrowdinessPercent(stats)
                break;
            case 'scale':
                labelText = this.formatCrowdinessText(stats)
                break;
        }
        return labelText;
    }

    formatCrowdinessText(stats: StatsInterface): string {
        let value = '';
        if (stats && stats.crowd && stats.crowd.current) {
            if (stats.crowd.current <= 30) {
                value = 'Cold';
            }else if (stats.crowd.current > 30 && stats.crowd.current <= 50) {
                value = 'Warm';
            }else if (stats.crowd.current > 50 && stats.crowd.current <= 100) {
                value = 'Warm';
            }else if (stats.crowd.current > 100) {
                value = 'On Fire';
            }
        }
        return value;
    }

    formatCrowdinessPercent(stats: StatsInterface): string {
        let value = '';
        if (stats && stats.crowd && stats.crowd.current) {
            if (stats.crowd.current <= 30) {
                value = '25%';
            }else if (stats.crowd.current > 30 && stats.crowd.current <= 50) {
                value = '50%';
            }else if (stats.crowd.current > 50 && stats.crowd.current <= 100) {
                value = '75%';
            }else if (stats.crowd.current > 100) {
                value = '100%';
            }
        }
        return value;
    }
}
