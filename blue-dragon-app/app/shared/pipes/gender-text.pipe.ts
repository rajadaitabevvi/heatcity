import { Pipe, PipeTransform } from '@angular/core';
import { StatsInterface } from '../interfaces';

@Pipe({name: 'genderText'})
export class GenderTextPipe implements PipeTransform {
    transform(stats: StatsInterface): string {
        let labelText: string;
        labelText = this.formatGender(stats)
        return labelText;
    }

    formatGender(stats: StatsInterface): string {
        let value = '';
        if (stats && stats.gender) {
            if (stats.gender.female === 50) {
                value = 'Both girls and guys like this place';
            }else if (stats.gender.female < 50) {
                value = 'Guys like the place';
            }else {
                value = 'Girls like the place';
            }
        }
        return value;
    }
}
