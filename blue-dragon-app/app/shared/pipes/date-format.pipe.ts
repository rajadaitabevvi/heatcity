import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({name: 'dateFormat'})
export class DateFormat implements PipeTransform {
    transform(date: string, format: string): string {
        let result: string = '';
        switch (format) {
            case 'fromNow':
                result = moment(date).fromNow();
                break;
        
            default:
                result  = moment(date).format(format);
                break;
        }
        return result;
    }
}

@Pipe({name: 'timeDiference'})
export class TimeDiference implements PipeTransform {
    transform(date: string, secondDate: string): string {
        let result  = moment(secondDate).diff(date, 'minutes');
        let dif = '';
        if(result > 59) {
            if(result % 60 > 0) {
                dif = Math.floor(result/60) + ':' + (result%60) + ' hours'
            } else {
                let hour = Math.floor(result/60);
                let textHours = hour == 1 ? ' hour' : ' hours'
                dif = hour + textHours;
            }
        } else {
            if(result >= 1) {
                let textMinutes = result == 1 ? ' minute' : ' minutes';
                dif = result + textMinutes;
            } else {
                dif = moment(secondDate).diff(date, 'seconds') + ' seconds';
            }
        }
        return dif;
    }
}