import { Pipe, PipeTransform } from '@angular/core';
import { StorageNative } from '../sdk/storage/storage.native';
import * as geolocation from 'nativescript-geolocation'

@Pipe({ name: 'distanceFromMe' })
export class DistanceFromMePipe implements PipeTransform {

    constructor(private _storageNative: StorageNative) {

    }

    transform(coordinates: any) {
        let Position = this._storageNative.get('currentPosition');

        let currentPosition = new geolocation.Location();
        currentPosition.latitude = Position.latitude || 0;
        currentPosition.longitude = Position.longitude || 0;


        let destinationPosition = new geolocation.Location()
        destinationPosition.longitude = coordinates[0] || 0;
        destinationPosition.latitude = coordinates[1] || 0;

        return geolocation.distance(currentPosition, destinationPosition);
    }
}