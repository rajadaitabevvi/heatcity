import { Pipe, PipeTransform, Inject } from '@angular/core';
import { BASE_URL, API_VERSION } from '../base.api';
import { DEFAULTS } from '../config';
import { Establishment } from '../sdk/models';
import { CategoryResourceService } from '../category-resource.service';

@Pipe({name: 'category'})
export class CategoryPipe implements PipeTransform {
    constructor(
        @Inject(CategoryResourceService) private categoryResourceService
    ) {}

    transform(establishment: Establishment, type: string): string {
        let name: string;
        let category: string = this.getCategory(establishment);
        switch (type) {
            case 'url':
                name = `res://${this.categoryResourceService.getResourceName('bigicon', category)}`;
                break;
            default:
                name = category;
                break;
        }
        return name;
    }

    getCategory(establishment: Establishment): string {
        return establishment.categories &&
            establishment.categories.length ?
            establishment.categories[0] :
            '';
    }
}