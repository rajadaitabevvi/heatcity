import { Pipe, PipeTransform } from '@angular/core';
import { Establishment } from '../sdk/models';
import { FormattedString } from "text/formatted-string";
import { Span } from "text/span";
import { Color } from "color";
import * as moment from 'moment';
import { Moment } from 'moment';
const STATUS: string = 'Closed';

@Pipe({name: 'formatHour'})
export class FormatHourPipe implements PipeTransform {
    transform(establishment: Establishment): any {
        let dayOfWeek = moment().isoWeekday();
        let time: string;
        let formattedString: FormattedString;
        if (
            establishment &&
            establishment.operatesAt &&
            establishment.operatesAt.timeframes
        ) {
            establishment.operatesAt.timeframes.forEach((timeFrame) => {
                let found = timeFrame.days.filter(day => day == dayOfWeek)
                if (found.length) {
                    time = this.formatHour(timeFrame.open);
                    formattedString = time === STATUS ?
                        this.buildFormattedString(null, STATUS) :
                        this.buildFormattedString(time);
                }
            })
            if (!time) {
                formattedString = this.buildFormattedString(null, STATUS);
            }
        }else {
            formattedString = this.buildFormattedString(null);
        }
        return formattedString;
    }

    buildFormattedString(time: string, status: string = null): FormattedString {
        let formattedString = new FormattedString();
        let text = new Span();
        if (status === STATUS) {
            text.text = status;
        }else if (time) {
            text.text = 'Open until ';
        }else {
            text.text = 'Hours not available';
        }
        text.color = new Color('#230045');
        formattedString.spans.push(text);
        if (time && time !== STATUS) {
            let hour = new Span();
            hour.text = time;
            hour.color = new Color('#230045');
            hour.fontWeight = 'bold';
            formattedString.spans.push(hour);
        }
        return formattedString;
    }

    formatHour(times: Array<any>): string {
        if (!(times && Array.isArray(times) && times.length > 0)) {
            return '';
        }
        let now = moment().toISOString();
        let closedAt: string;
        times.forEach((hour) => {
            let isNextDay = Number(hour.end) < Number(hour.start) ? true : false;
            let closeHour = this.parseStringToDate(hour.end.replace('+', ''), isNextDay);
            let openHour = this.parseStringToDate(hour.start.replace('+', ''));
            if (
                closeHour.diff(now, 'minutes', true) >= 0
                && openHour.diff(now, 'minutes', true) <= 0
            ) {
                closedAt = closeHour.format('h:mm a');
            }
        })
        if (!closedAt) {
            closedAt = STATUS;
        }
        return closedAt;
    }

    parseStringToDate(time: string, isNextDay: boolean = false): Moment {
        let date = moment();
        let timeArray = time.split('');
        let hour = timeArray.splice(0, 2).join('');
        let minute = timeArray.splice(3, 2).join('');
        date.second(0);
        date.minute(Number(minute));
        date.hour(Number(hour));
        if (isNextDay) {
            date.add(1, 'days')
        }
        return date;
    }
}
