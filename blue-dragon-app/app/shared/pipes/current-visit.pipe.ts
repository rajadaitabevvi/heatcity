import { Pipe, PipeTransform } from '@angular/core';
import { Account } from '../sdk/models';

@Pipe({name: 'currentVisit'})
export class CurrentVisitlPipe implements PipeTransform {
    transform(account: Account, loggedUser: Account): string {
        let address = '';
        let userEnabledShareLocation = account && account.settings && account.settings.shareLocation;
        let loggedUserEnabledShareLocation = loggedUser && loggedUser.settings 
            && loggedUser.settings.shareLocation;
        if(
            userEnabledShareLocation &&
            account.visits &&
            account.visits.length > 0
        ) {
            let visit = account.visits[0]
            if(!visit.hasOwnProperty('exit')){
                console.log(JSON.stringify(account.visits[0].location))
                address = account.visits[0].location.establishment.name;
            }
        }
        // console.log('address', account.hometown, account.homecountry, account.id)
        // console.log('comparison', account.hometown && account.hometown != undefined ? 'exist': 'nel')
        if(
            address == '' ||
            (!userEnabledShareLocation || !loggedUserEnabledShareLocation)
        ){
            if (account.fbHometown && account.fbHometown.name) {
                address = account.fbHometown.name;
            }else if (
                account.hometown &&
                account.hometown != undefined &&
                account.homecountry &&
                account.homecountry != undefined
            ) {
                address = account.hometown + ', ' + account.homecountry;
            }else {
                address = '';
            }
        }
        return address;
    }
}