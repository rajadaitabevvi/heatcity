import { Pipe, PipeTransform } from '@angular/core';
import { Establishment } from '../sdk/models';

@Pipe({name: 'priceRange'})
export class PriceRangePipe implements PipeTransform {
    transform(establishment: Establishment): string {
        let labelText: string;
        labelText = this.formatPriceRange(establishment)
        return labelText;
    }

    formatPriceRange(establishment: Establishment): string {
        let value: string = '';
        if (establishment && establishment.pricing) {
            switch (establishment.pricing) {
                case 1:
                    value = '$';
                    break;
                case 2:
                    value = '$$';
                    break;
                case 3:
                    value = '$$$';
                    break;
                case 4:
                    value = '$$$$';
                    break;
                case 5:
                    value = '$$$$$';
                    break;
                default:
                    value = '';
                    break;
            }
        }
        return value;
    }
}
