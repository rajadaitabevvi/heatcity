// this import should be first in order to load some required settings (like globals and reflect-metadata)
import * as application from "application";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { platformNativeScriptDynamic } from "nativescript-angular/platform";
import { AppModule } from "./app.module";
import { delegate } from "./login/facebook-login/app-delegate";
import { registerElement } from "nativescript-angular/element-registry";
import firebase = require("nativescript-plugin-firebase");
import * as googleAnalytics from "nativescript-google-analytics";
import { BackgroundFetch } from "nativescript-background-fetch";
declare const UIApplication: any;

if (application.ios) {
    application.ios.delegate = delegate();

}


registerElement("Carousel", () => require("nativescript-carousel").Carousel);
registerElement("CarouselItem", () => require("nativescript-carousel").CarouselItem);

if (application.android) {
    application.on(application.launchEvent, function (args) {
        googleAnalytics.initalize(<any>{
            trackingId: "UA-93149103-1",
            dispatchInterval: 120,
            logging: {
                native: true,
                console: false
            },
            enableDemographics: true
        });
    });
}

/* if (application.ios) {
    class MyDelegate extends UIResponder implements UIApplicationDelegate {
        public static ObjCProtocols = [UIApplicationDelegate];

        public applicationPerformFetchWithCompletionHandler(application: UIApplication, completionHandler: any) {
            console.log('- AppDelegate Rx Fetch event');
            BackgroundFetch.performFetchWithCompletionHandler(application, completionHandler);
        }
    }
    application.ios.delegate = MyDelegate;
} */

platformNativeScriptDynamic().bootstrapModule(AppModule);
