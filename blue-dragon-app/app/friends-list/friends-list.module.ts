import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NgModule } from "@angular/core";
import { FriendsListComponent } from "./friends-list.component";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  imports: [
    NativeScriptModule,
    SharedModule
  ],
  exports: [FriendsListComponent],
  declarations: [FriendsListComponent],
  providers: []
})
export class FriendsListModule { }
