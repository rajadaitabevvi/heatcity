import { Component, OnInit, OnDestroy, Input, Output, OnChanges, SimpleChange, EventEmitter} from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AccountApi, ShareApi } from '../shared/sdk/services';
import { LoopBackConfig } from '../shared/sdk';
import { BASE_URL, API_VERSION } from '../shared/base.api';
import { Account, Share, Establishment } from '../shared/sdk/models';
import { AppNotificationsService } from '../app-notifications/app-notifications.service';
import { LoadingIndicator } from "nativescript-loading-indicator";
import { SwissArmyKnife } from 'nativescript-swiss-army-knife';
import { Page } from 'ui/page';
import { ScrollView } from 'ui/scroll-view';
import * as SocialShare from "nativescript-social-share";
import * as googleAnalytics from "nativescript-google-analytics";
import { TextField } from "ui/text-field";
import { LogoutService } from '../shared/logout.service';

declare var Object: any;
declare var android;
interface ShareListInterface { 
    [key: string]: Account
}

@Component({
    moduleId: module.id,
    selector: "ShareComponent",
    templateUrl: "share.component.html",
    styleUrls: ['share-common.css']
})
export class ShareComponent implements OnInit, OnDestroy, OnChanges {
    /** @type {Subscription[]} **/
    private subscriptions: Subscription[] = new Array<Subscription>();

    /** @type {Subscription[]} **/
    private friends: Account[];

    /** @type {Establishment} **/
    private selectedEstablishment: Establishment;

    /** @type {FriendRequestInterface} **/
    private shareList: ShareListInterface = {};

    /** @type {string[]} **/
    private shareListKeys: string[] = [];

    private searchInput: string = '';

    private counter: { typed: boolean, number: number} = {
        typed: false,
        number: 0
    };

    private counterInterval: any;

    private limit: number = 10;

    /** @type {LoadingIndicator} **/
    private loader: {
        instance: LoadingIndicator,
        options: any,
        loading: boolean
    } = {
        loading: false,
        instance: new LoadingIndicator(),
        options: {
            message: 'Loading...',
            progress: 0,
            android: {
                indeterminate: true,
                cancelable: false,
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            },
            ios: {
                square: false,
                margin: 10,
                dimBackground: true,
                color: "#fff",
                mode: 'MBProgressHUDModeText'// see iOS specific options below
            }
        }
    };

    @Input('show')show: boolean;

    @Input('establishment')establishment: Establishment;

    @Input('notificationsName')notificationsName: string;

    @Output('close') onClose: EventEmitter<boolean> = new EventEmitter<boolean>();
    
    /**
     * @method constructor
     * @param {Page} page Page instance
     * @param {AccountApi} accountApi Fireloop Account Service
     * @param {ShareApi} shareApi Fireloop Share Service
     * @param {FriendApi} friendApi Fireloop Friend Service
     **/
    constructor(
        private page: Page,
        private accountApi: AccountApi,
        private shareApi: ShareApi,
        private logoutService: LogoutService,
        private appNotifications: AppNotificationsService
    ) {
        LoopBackConfig.setBaseURL(BASE_URL);
        LoopBackConfig.setApiVersion(API_VERSION);
        googleAnalytics.logView('share');
    }

    /**
     * @method ngOnInit
     **/
    ngOnInit() {
        // remove to avoid loading on map.
        // this.getFriends();
    }

    /**
     * @method ngOnChanges
     **/
    ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
        if(changes['show']) {
            this.show = changes['show'].currentValue;
            if(!this.show) {
                this.clearList();
            } else {
                setTimeout(() => {
                    let shareInput: TextField = this.page.getViewById<TextField>('share-input');
                    if (shareInput && !this.page.ios) {
                        shareInput.android.setEllipsize(android.text.TextUtils.TruncateAt.END) ;
                    }
                    
                }, 0);
                this.getFriends();
            }
        }
        if(
            changes['establishment'] &&
            changes['establishment'].currentValue !== null &&
            changes['establishment'].currentValue !== undefined
        ) {
            this.selectedEstablishment = <Establishment> Object.assign({}, this.establishment);
        }
    }

    /**
     * @method getFriends
     **/
    getFriends(): void {
        let id = this.accountApi.getCurrentId();
        this.loader.instance.show(this.loader.options);
        this.subscriptions.push(
            this.accountApi.friendsSearch(id, this.searchInput, this.limit).subscribe(
                (friends: Account[]) => {
                    this.friends = friends;
                    this.loader.instance.hide();
                    let friendList = this.page.getViewById<ScrollView>('friends-list');
                    SwissArmyKnife.removeHorizontalScrollBars(friendList);
                },
                (err: Error) => {
                    this.loader.instance.hide();
                    this.errorHandler(err);
                }
            )
        );
    }

    /**
     * @method loadMore
     **/
    loadMore(): void {
        this.limit += 10;
        this.getFriends();
    }

    /**
     * @method addToShareList
     **/
    addToShareList(friend: Account): void {
        this.dismissKeyboard();
        let shareList = this.page.getViewById<ScrollView>('share-list');
        SwissArmyKnife.removeHorizontalScrollBars(shareList);
        if(this.shareList[friend.id]) {
            alert('User is already added to list.');
        } else {
            this.shareList[friend.id] = friend;
            this.shareListKeys = Object.keys(this.shareList);
        }
    
    }

    /**
     * @method share
     **/
    share():void {
        let id = this.accountApi.getCurrentId();
        let shares = this.shareListKeys.map((key: string) => {
            return {
                accountId: id,
                friendId: this.shareList[key].id,
                establishmentId: this.selectedEstablishment.id
            };
        });
        this.loader.instance.show(this.loader.options);
        this.subscriptions.push(
            this.shareApi.create(shares).subscribe(
                (shares: Share[]) => {
                    this.loader.instance.hide();
                    let message = '';
                    if (this.shareListKeys.length > 1) {
                        message = `${this.selectedEstablishment.name} successfully shared with ${this.shareListKeys.length} friends`;
                        this.appNotifications.displayNotification(this.notificationsName, {text: message, status: true});
                    } else {
                        message = `${this.selectedEstablishment.name} successfully shared with ${this.shareListKeys.length} friend`;
                        this.appNotifications.displayNotification(this.notificationsName, {text: message, status: true});
                    }
                    this.close();
                },
                (err: Error) => {
                    this.loader.instance.hide();
                    this.errorHandler(err);
                }
            )
        );
    }

    /**
     * @method shareToAll
     **/
    shareToAll(): void {
        let id = this.accountApi.getCurrentId();
        this.loader.instance.show(this.loader.options);
        this.subscriptions.push(
            this.accountApi.shareToAllFriends(id, this.selectedEstablishment.id).subscribe(
                (shares: Share[]) => {
                    this.loader.instance.hide();
                    this.appNotifications.displayNotification(this.notificationsName,
                        {text: `${this.selectedEstablishment.name} successfully shared with all friends`, status: true}
                    );
                    this.close()
                },
                (err: Error) => {
                    this.loader.instance.hide();
                    this.errorHandler(err)
                }
            )
        );
    }

    shareToExternal(): void {
        this.loader.instance.show(this.loader.options);
        this.subscriptions.push(
            this.accountApi.getCurrent().subscribe(
                (account: Account) => {
                    this.loader.instance.hide();
                    SocialShare.shareText(
                        `${account.displayName} shared ${this.selectedEstablishment.name} ${this.selectedEstablishment.address} and using HeatCity.
                         Download HeatCity for iOS: https://goo.gl/Oo4EPS, Android: https://goo.gl/vrM9oX 
                         to check out the place.`,
                         'share with...'
                    );
                },
                (err: Error) => {
                    this.loader.instance.hide();
                    this.errorHandler(err);
                }
            )            
        );
    }

    /**
     * @method startCounter
     **/
    startCounter() {
        if (!this.counterInterval) {
            this.counterInterval = setInterval(() => {
                if (this.counter.typed && this.counter.number < 4) {
                    this.counter.number += 1;
                } else {
                    this.counter.typed = false;
                    console.log('Get friends with! ', this.searchInput);
                    this.getFriends();
                    clearInterval(this.counterInterval);
                    this.counterInterval = null;
                }
            }, 333);
        }
    }

    /**
     * @method onModelChanged
     **/
    onModelChanged() {
        console.log('Here in the share!');
        this.counter.number = 0;
        this.counter.typed  = true;
        this.startCounter();
    }

    /**
     * @method close
     **/
    close(): void {
        this.dismissKeyboard();
        this.onClose.emit(true);
    }

    clearList(): void {
        this.limit = 10;
        this.shareList = {};
        this.shareListKeys = [];
    }

    dismissKeyboard(): void {
        let shareInput: any = this.page.getViewById('share-input');
        if (shareInput) {
            shareInput.dismissSoftInput();
        }
    }

    /**
     * @method ngOnDestroy
     **/
    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
    }
    
    errorHandler(error: any): void {
        console.log('Share error handler: ', JSON.stringify(error))
        if (error && error.statusCode === 401) {
            this.logoutService.logout();
        }
    }
}
