import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NgModule } from "@angular/core";
import { ShareComponent } from "./share.component";
import { SharedModule } from "../shared/shared.module";


@NgModule({
  imports: [
    NativeScriptModule,
    NativeScriptFormsModule,
    SharedModule,
  ],
  exports: [
    ShareComponent,
  ],
  declarations: [
    ShareComponent,
  ],
  providers: []
})
export class ShareModule { }
