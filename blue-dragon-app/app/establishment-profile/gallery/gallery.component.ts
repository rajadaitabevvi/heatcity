import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { Page } from 'ui/page';
const pageCommon = require('ui/page/page-common').Page;
import { Color } from 'color';
import { ScrollView } from 'ui/scroll-view';
import * as utils from 'utils/utils';
import { Establishment } from '../../shared/sdk/models';

declare var UIModalPresentationOverFullScreen: any;
declare var UIModalTransitionStyleCrossDissolve: any;
declare var UIColor: any;
declare var UIModalPresentationCurrentContext: any;

@Component({
    moduleId: module.id,
    selector: 'gallery',
    styleUrls: ['gallery.common.css'],
    templateUrl: 'gallery.component.html'
})
export class GalleryComponent implements OnInit {
    selectedIndex: number = 0;
    establishment: Establishment = new Establishment();

    constructor(
        private params: ModalDialogParams,
        private page: Page
        
    ) {
        this.selectedIndex = params.context.index;
        this.establishment = params.context.establishment;
        this.changeTransparencyIOS()
    }

    ngOnInit() { }

    onScrollLoaded(args: any) {
        console.log('entro');
        let scrollOffset = 4;
        (<ScrollView>args.object).scrollToHorizontalOffset(scrollOffset, false);
    }

    onSwipe(args: any) {
        let totalPhotos = this.establishment.photos.length;
        if (args.direction == 1) {
            this.selectedIndex = this.selectedIndex == 0 ? totalPhotos - 1 : this.selectedIndex - 1;
        }else if (args.direction == 2) {
            this.selectedIndex = this.selectedIndex == (totalPhotos -1) ? 0 : this.selectedIndex + 1;
        }
    }

    onSelectImage(index: number) {
        this.selectedIndex = index;
    }

    close() {
        this.params.closeCallback();
    }

    changeTransparencyIOS() {
        let color = new Color(220, 0, 0, 0);
        if (this.page.ios) {
            this.page.backgroundColor = color;
            (<any>this.page)._showNativeModalView = function(parent, context, closeCallback, fullscreen) {
                pageCommon.prototype._showNativeModalView.call(this, parent, context, closeCallback, fullscreen);
                let that = this;

                this._modalParent = parent;
                if (!parent.ios.view.window) {
                    throw new Error('Parent page is not part of the window hierarchy. Close the current modal page before showing another one!');
                }

                if (fullscreen) {
                this._ios.modalPresentationStyle = 0;
                } else {
                this._ios.modalPresentationStyle = 2;
                this._UIModalPresentationFormSheet = true;
                }

                pageCommon.prototype._raiseShowingModallyEvent.call(this);

                this._ios.providesPresentationContextTransitionStyle = true;
                this._ios.definesPresentationContext = true;
                this._ios.modalPresentationStyle = UIModalPresentationOverFullScreen;
                this._ios.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                this._ios.view.backgroundColor = UIColor.clearColor;

                parent.ios.presentViewControllerAnimatedCompletion(this._ios, utils.ios.MajorVersion >= 9, function completion() {
                that._ios.modalPresentationStyle = UIModalPresentationCurrentContext;
                that._raiseShownModallyEvent(parent, context, closeCallback);
                });
            };
        }else {
            this.page.backgroundColor = color;
        }
    }
}