import { NgModule, NgModuleFactoryLoader } from '@angular/core';
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { ModalDialogService } from "nativescript-angular/modal-dialog";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptUIListViewModule } from "nativescript-pro-ui/listview/angular";

import { EstablishmentProfileComponent } from './establishment-profile.component';
import { establishmentProfileRouting } from './establishment-profile.routing';
import { GalleryComponent } from './gallery/gallery.component';
import { SharedModule } from '../shared/shared.module';
import { CHART_DIRECTIVES } from 'nativescript-pro-ui/chart/angular';
import { StorageNative } from '../shared/sdk/storage/storage.native';
import { AppNotificationsModule } from '../app-notifications/app-notifications.module';
import { ShareModule } from '../share/share.module';

@NgModule({
    imports: [
        NativeScriptUIListViewModule,
        establishmentProfileRouting,
        SharedModule,
        NativeScriptModule,
        NativeScriptFormsModule,
        AppNotificationsModule,
        ShareModule
    ],
    exports: [GalleryComponent],
    declarations: [
        EstablishmentProfileComponent,
        GalleryComponent,
        CHART_DIRECTIVES,
    ],
    providers: [
        ModalDialogService,
        StorageNative
    ],
    entryComponents: [GalleryComponent]
})
export class EstablishmentProfileModule { }
