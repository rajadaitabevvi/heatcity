import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { isIOS } from "platform";
import { Account } from "../../shared/sdk/models";
import { alert } from "../../shared/dialog-util";
import * as email from "nativescript-email";
import { AppNotificationsService } from '../../app-notifications/app-notifications.service';

@Component({
    selector: "app-request-invitation",
    templateUrl: "login/request-invitation/request-invitation.component.html",
    styleUrls: ["login/request-invitation/request-invitation-common.css", "login/request-invitation/request-invitation.component.css"]
})
export class RequestInvitationComponent {
    account: Account = new Account();

    constructor(
        private _router: Router,
        private appNotificationsService: AppNotificationsService
    ) {}

    requestInvitation() {
        this.sendEmail();
    }

    sendEmail() {
        if(this.account.firstName && this.account.email) {
            email.available().then(avail => {
                if(avail) {
                    email.compose({
                        subject: "heat Invitation Request",
                        body: "Please send us your email address.  We will process your request very soon.  Thanks for your interest in HEATCITY.  Regards, HEATCITY Team",
                        to: ['info@heatcity.io'],
                        appPickerTitle: 'Send email with...'
                    }).then(() => {
                        this._router.navigate(['login/']);
                    });
                } else {
                    alert("e-mail client it is not available");
                    this._router.navigate(['login/']);
                }
            });
        }
    }
}