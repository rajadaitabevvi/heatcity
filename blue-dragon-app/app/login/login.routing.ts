import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login.component';
import { FacebookLoginComponent } from './facebook-login/facebook-login.component';
import { RequestInvitationComponent } from './request-invitation/request-invitation.component';
import { InvitationCodeComponent } from './invitation-code/invitation-code.component';

export const loginRoutes: Routes = [
  { 
    path: 'login',
    component: LoginComponent,
    children: [
      // { path: 'facebook-login', component: FacebookLoginComponent},
      // { path: 'request-invitation', component: RequestInvitationComponent },
      { path: '', component: FacebookLoginComponent }
    ]
  }
];