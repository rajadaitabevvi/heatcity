import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from 'rxjs/Subscription';
import { Router } from "@angular/router";
import * as appSettings from "application-settings";
import { alert } from "../../shared/dialog-util";
import { AccountApi } from '../../shared/sdk/services';
import { LoopBackConfig } from '../../shared/sdk';
import { BASE_URL, API_VERSION } from '../../shared/base.api';
import { Page }  from "ui/page";
import { AppNotificationsService } from '../../app-notifications/app-notifications.service';
import { LoadingIndicator } from "nativescript-loading-indicator";

@Component({
    selector: "app-invitation-code",
    templateUrl: "login/invitation-code/invitation-code.component.html",
    styleUrls: ["login/invitation-code/invitation-code-common.css", "login/invitation-code/invitation-code.component.css"]
})
export class InvitationCodeComponent implements OnInit, OnDestroy{
    /** @type {string} **/
    private invitCode: string;

    /** @type {Subscription[]} **/
    private subscriptions: Subscription[] = new Array<Subscription>();

    /** @type {LoadingIndicator} **/
    private loader: {
        instance: LoadingIndicator,
        options: any,
        loading: boolean
    } = {
        loading: false,
        instance: new LoadingIndicator(),
        options: {
            message: 'Loading...',
            progress: 0,
            android: {
                indeterminate: true,
                cancelable: false,
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            },
            ios: {
                square: false,
                margin: 10,
                dimBackground: true,
                color: "#fff",
                mode: 'MBProgressHUDModeText'// see iOS specific options below
            }
        }
    };

    /**
     * @method constructor
     * @param {Router} _router Angular 2 Router Service
     * @param {AcountApi} accountApi Fireloop Account Service
     **/
    constructor(
        private _router: Router,
        private accountApi: AccountApi,
        private page: Page,
        private appNotifications: AppNotificationsService
    ) {
        LoopBackConfig.setBaseURL(BASE_URL);
        LoopBackConfig.setApiVersion(API_VERSION);
    }

    /**
     * @method ngOnInit
     **/
    ngOnInit() {
        this.page.actionBarHidden = true;
        let code = appSettings.getString('invitationCode', null);
        if(code) {
            this._router.navigate(['login/facebook-login']);
        }
    }

    /**
     * @method login
     **/
    login() {
        this.loader.instance.show(this.loader.options);
        this.subscriptions.push(
            this.accountApi.validCode(this.invitCode).subscribe(
                (res: boolean) => {
                    this.loader.instance.hide();
                    if(res) {
                        this.appNotifications.displayNotification('login-notifications', {text:'Invite code accepted', status: true});
                        appSettings.setString('invitationCode', this.invitCode);
                        this._router.navigate(['login/facebook-login']);
                    } else {
                        this.appNotifications.displayNotification('login-notifications', {text:'Invalid code!. Please provide a valid code', status: false});
                    }
                },
                (err: Error) => {
                    this.loader.instance.hide();
                    console.log(err.message);
                }
            )
        );
    }

    /**
     * @method goToRI
     **/
    goToRI() {
        this._router.navigate(['login/request-invitation']);
    }

    /**
     * @method ngOnDestroy
     **/
    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
    }
}
