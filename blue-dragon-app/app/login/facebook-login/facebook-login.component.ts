import { Component, NgZone } from "@angular/core";
import { Router } from "@angular/router";
const FacebookLoginHandler = require("nativescript-facebook-login");
import { isIOS, device } from "platform";
import * as appSettings from "application-settings";
import { alert } from "../../shared/dialog-util";
import { SocialCredentialApi } from "../../shared/sdk/services";
import { SocialCredential, Account } from "../../shared/sdk/models";
import { AccountApi } from "../../shared/sdk/services";
import { LoopBackConfig } from '../../shared/sdk';
import { Subscription } from 'rxjs/Subscription';
import { StorageNative } from '../../shared/sdk/storage/storage.native';
import { BASE_URL, API_VERSION } from '../../shared/base.api';
import { Page } from "ui/page";
import { NavigateService } from '../../shared/navigate.service';
import { LoadingIndicator } from "nativescript-loading-indicator";
import { Http } from '@angular/http';
import { Accuracy } from "ui/enums";
import geolocation = require('nativescript-geolocation');
import { RouterExtensions } from "nativescript-angular/router";
import { AuthGuard } from "../../auth-guard.service"

declare var FBSDKGraphRequest: any;
declare var com: any;
declare var CFRunLoopGetMain: any;
declare var CFRunLoopPerformBlock: any;
declare var kCFRunLoopDefaultMode: any;
declare var CFRunLoopWakeUp: any;
declare var FBSDKAccessToken: any;
declare var NSJSONSerialization: any;
declare var NSString: any;

interface FacebookUser {
    id: string;
    name: string;
    first_name?: string;
    last_name?: string;
    hometown?: string;
    age_range?: string;
    link?: string;
    gender?: string;
    locale?: string;
    picture?: any;
    cover?: any;
    timezone?: any;
    birthday?: string;
    updated_time?: any;
    verified?: boolean;
};

let invokeOnRunLoop = (function () {
    if (isIOS) {
        var runloop = CFRunLoopGetMain();
        return function (func) {
            CFRunLoopPerformBlock(runloop, kCFRunLoopDefaultMode, func);
            CFRunLoopWakeUp(runloop);
        }
    }
}());

@Component({
    selector: "app-facebook-login",
    templateUrl: "login/facebook-login/facebook-login.component.html",
    styleUrls: ["login/facebook-login/facebook-login-common.css", "login/facebook-login/facebook-login.component.css"]
})
export class FacebookLoginComponent {
    facebookToken: any;
    account: any;
    subscriptions: Array<Subscription>;
    result: any;
    userEmail: any;
    socialCredentialError: boolean = false;
    processing: boolean = false;
    /** @type {LoadingIndicator} **/
    private loader: {
        instance: LoadingIndicator,
        options: any,
        loading: boolean
    } = {
            loading: false,
            instance: new LoadingIndicator(),
            options: {
                message: 'Loading...',
                progress: 0,
                android: {
                    indeterminate: true,
                    cancelable: false,
                    max: 100,
                    progressNumberFormat: "%1d/%2d",
                    progressPercentFormat: 0.53,
                    progressStyle: 1,
                    secondaryProgress: 1
                },
                ios: {
                    square: false,
                    margin: 10,
                    dimBackground: true,
                    color: "#fff",
                    mode: 'MBProgressHUDModeText'// see iOS specific options below
                }
            }
        };


    constructor(
        private _zone: NgZone,
        private _router: Router,
        private _socialCredential: SocialCredentialApi,
        private _accountApi: AccountApi,
        private page: Page,
        private http: Http,
        private navigate: NavigateService,
        private _storage: StorageNative,
        private _routerExtenstion: RouterExtensions,
        private authGuard: AuthGuard
    ) {
        page.actionBarHidden = true;
        LoopBackConfig.setBaseURL(BASE_URL);
        LoopBackConfig.setApiVersion(API_VERSION);
        this.subscriptions = new Array();
        this.facebookToken = JSON.parse(appSettings.getString('facebookToken', '{}'));
        FacebookLoginHandler.init();
        FacebookLoginHandler.registerCallback(this.successCallback.bind(this), this.cancelCallback.bind(this), this.failCallback.bind(this));
        if (this.facebookToken.userId) {
            this.getSocialCredential(true);
        }
    }

    /**
     * @method ngOnInit
     **/
    ngOnInit() {
        this.page.actionBarHidden = true;
        this.page.backgroundColor = "black";
        this.page.backgroundSpanUnderStatusBar = true;
    }

    /**
     * @method facebookLogin
     **/
    facebookLogin() {
        this.processing = true;
        FacebookLoginHandler.logInWithReadPermissions(["public_profile", "email", "user_birthday", "user_hometown"]);
    }

    /**
     * @method successCallback
     **/
    successCallback(result) {
        this._zone.run(() => {
            let token;
            let userId;
            this.result = result;
            if (!isIOS) {
                token = result.getAccessToken().getToken();
                userId = result.getAccessToken().getUserId();
                com.facebook.AccessToken.setCurrentAccessToken(result.getAccessToken());
            }
            else {
                token = result.token.tokenString;
                userId = FBSDKAccessToken.currentAccessToken().userID;
            }
            this.facebookToken = {
                token: token,
                userId: userId
            };
            this.getSocialCredential();
        });
    }

    /**
     * @method cancelCallback
     **/
    cancelCallback() {
        this._zone.run(() => {
            this.processing = false;
            alert("Login was cancelled");
        });
    }

    /**
     * @method failCallback
     **/
    failCallback(error) {
        this._zone.run(() => {
            this.processing = false;
            let errorMessage = "Error with Facebook";
            if (error) {
                if (isIOS) {
                    if (error.localizedDescription) {
                        errorMessage += ": " + error.localizedDescription;
                    }
                    else if (error.code) {
                        errorMessage += ": Code " + error.code;
                    }
                    else {
                        errorMessage += ": " + error;
                    }
                }
                else if (!isIOS) {
                    if (error.getErrorMessage) {
                        errorMessage += ": " + error.getErrorMessage();
                    }
                    else if (error.getErrorCode) {
                        errorMessage += ": Code " + error.getErrorCode();
                    }
                    else {
                        errorMessage += ": " + error;
                    }
                }
            }
            alert(errorMessage);
        });
    }

    /**
     * @method getSocialCredential
     **/
    getSocialCredential(login?: boolean) {
        // this.loader.instance.show(this.loader.options); Causing crashes on fresh installations
        this.subscriptions.push(
            this._socialCredential.find({
                where: {
                    facebookId: this.facebookToken.userId
                },
                include: 'account'
            }).subscribe((res: Array<SocialCredential>) => {
                if (res.length > 0) {
                    if (res[0].account) {
                        this.getFacebookEmail(res[0].account.id);
                        /*
                        let account = res[0].account;
                        if(
                            account.location &&
                            account.location.lat &&
                            account.location.lng &&
                            account.homecountry &&
                            account.hometown
                        ) {
                            // this.login(res[0].account);
                            this.getFacebookEmail(res[0].account.id);
                        } else {
                            this.getFacebookEmail(res[0].account.id);
                        }
                        */
                    } else {
                        this.getFacebookEmail();
                    }
                } else {
                    this.getFacebookEmail();
                }
            }, (err: any) => {
                if (err == 'Server error') {
                    alert('Unable to connect with the server, please try again latter')
                } else {
                    alert("Error in request!" + JSON.stringify(err));
                }
                this.processing = false;
                //this.loader.instance.hide();
            })
        );
    }

    /**
     * @method getFacebookEmail
     **/
    getFacebookEmail(accountId?: string) {
        if (isIOS) {
            invokeOnRunLoop(() => {
                let graph = FBSDKGraphRequest.alloc();
                graph.initWithGraphPathParameters("me", { "fields": "id,name,first_name,last_name,email,picture.width(400).height(400),age_range,gender,birthday,hometown" })
                    .startWithCompletionHandler((connection, result, error) => {
                        if (error) {
                            console.log("Error! can not get user info!");
                        } else {
                            let resObj: any = this.parseObject(result);
                            let email = resObj.email;
                            if (email == null || email == '' || email.length == 0) {
                                email = resObj.id + "@facebook.com";
                            }
                            let fbUser = {
                                id: resObj.id,
                                name: resObj.name,
                                first_name: resObj.first_name,
                                last_name: resObj.last_name,
                                age_range: resObj.age_range,
                                gender: resObj.gender,
                                hometown: resObj.hometown,
                                picture: resObj.picture,
                                birthday: resObj.birthday
                            }
                            if (fbUser.age_range) {
                                fbUser.age_range = fbUser.age_range.min;
                            }
                            fbUser.picture = fbUser.picture ? fbUser.picture.data.url : '';
                            // this.buildUserObject(email, result.valueForKey("id"), fbUser, accountId);
                            this.createAccount(email, result.valueForKey("id"), fbUser, accountId)
                        }
                    })
            });
        } else {
            let graphCallback = new com.facebook.GraphRequest.GraphJSONObjectCallback({
                onCompleted: (me, response) => {
                    if (response.getError() != null) {
                        console.log(response.getError());
                    } else {
                        let email = me.optString("email");
                        if (email == null || email == '' || email.length == 0) {
                            email = me.optString("id") + "@facebook.com";
                        }
                        let fbUser = {
                            id: me.optString("id"),
                            name: me.optString("name"),
                            first_name: me.optString("first_name"),
                            last_name: me.optString("last_name"),
                            age_range: me.optString("age_range"),
                            hometown: me.optString("hometown"),
                            gender: me.optString("gender"),
                            picture: me.optString("picture"),
                            birthday: me.optString("birthday")
                        }
                        if (fbUser.age_range) {
                            fbUser.age_range = this.isJSON(fbUser.age_range) ? JSON.parse(fbUser.age_range).min : fbUser.age_range.min;
                        }
                        fbUser.picture = this.isJSON(fbUser.picture) ? JSON.parse(fbUser.picture).data.url : '';
                        fbUser.hometown = this.isJSON(fbUser.hometown) ? JSON.parse(fbUser.hometown) : {};
                        this.createAccount(email, me.optString("id"), fbUser, accountId)
                        //this.buildUserObject(email, me.optString("id"), fbUser, accountId);
                    }
                }
            });

            let graphRequest = new com.facebook.GraphRequest.newMeRequest(this.result.getAccessToken(), graphCallback);
            var bundle = graphRequest.getParameters();
            bundle.clear();
            bundle.putString("fields", "id,name,first_name,last_name,email,picture.width(400).height(400),gender,age_range,birthday,hometown");
            graphRequest.setParameters(bundle);
            graphRequest.executeAsync();
        }
    }

    parseObject(data) {
        if (isIOS) {
            var jsonData = NSJSONSerialization.dataWithJSONObjectOptionsError(data, 0, null);
            var JSONObject = JSON.parse(NSString.alloc().initWithDataEncoding(jsonData, 4).toString());
            return JSONObject;
        } else {
            return data
        }
    }

    buildUserObject(userEmail: string, fbId: string, fbUser: FacebookUser, accountId: string): void {
        let data: any = {};
        if (!geolocation.isEnabled()) {
            console.log('not enabled, requesting');
            geolocation.enableLocationRequest(true).then(() => {
                console.log('request location success')
                this.getUserLocation(data, (err: any, account: Account) => {
                    if (err) {
                        console.log('Error on Build user object', err);
                    }
                    if (accountId) {
                        account.id = accountId;
                    }
                    this.createAccount(userEmail, fbId, fbUser, account.id);
                })
            }).catch((err) => {
                console.log('5--request location err', err)
                this.createAccount(userEmail, fbId, fbUser);
            });
        } else {
            this.getUserLocation(data, (err: any, account: Account) => {
                if (err) {
                    console.log('Error on Build user object', err);
                }
                if (accountId) {
                    account.id = accountId;
                }
                this.createAccount(userEmail, fbId, fbUser, account.id);
            })
        }
    }

    getUserLocation(account: Account, cb: Function): void {
        geolocation.getCurrentLocation({
            desiredAccuracy: Accuracy.any
        }).then((location: geolocation.Location) => {
            account.location = {
                lat: location.latitude,
                lng: location.longitude
            };
            this.getTownAndCountryName(account, cb);
        }).catch((err) => {
            console.log('5--Error on get user location', err)
            console.log('5--Error on get user location', JSON.stringify(err));
            cb(err);
        });
    }

    getTownAndCountryName(account: Account, cb: Function): void {
        this.subscriptions.push(
            this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${account.location.lat},${account.location.lng}&key=AIzaSyAaGsoyNTNWIgJJwCUn9sagg3bvyvQbRfc`)
                .subscribe(
                (res: any) => {
                    let result = res.json();
                    if (result.results && Array.isArray(result.results) && result.results.length > 0) {
                        result.results[0].address_components.forEach((addressComponent: any) => {
                            addressComponent.types.forEach((type: string) => {
                                if (type == "administrative_area_level_1") {
                                    account.hometown = addressComponent.long_name;
                                }
                                if (type == "country") {
                                    account.homecountry = addressComponent.long_name;
                                }
                            });
                        });
                        return cb(null, account);
                    }
                },
                (err: Error) => {
                    console.log('error getTownAndCountryName: ', err.name);
                    return cb(err.name, account);
                }
                )
        );
    }

    private isJSON(data: string) {
        try {
            JSON.parse(data);
        } catch (e) {
            return false;
        }
        return true;
    }

    /**
     * @method login
     **/
    login(account: Account) {
        this.subscriptions.push(
            this._accountApi.login({
                email: account.email,
                password: 'xdh800'
            }).subscribe(res => {
                console.log('login!!!', JSON.stringify(account));
                this.authGuard.loginAccount(account.id);
                appSettings.setString('facebookToken', JSON.stringify(this.facebookToken));
                if (this.socialCredentialError) {
                    account.id = res.userId;
                    this.createSocialCredential(account, this.facebookToken.userId)
                }
                this.registerDeviceByAccount(account);
            }, err => {
                this.loader.instance.hide();
                alert(err.message);
            })
        );
    }

    nextScreen(account: Account): void {
        let settings = {
            clearHistoy: true
        }
        this._routerExtenstion.navigate(['/map'], {
            clearHistory: true,
            animated: false,
        })
        //this.navigate.to('/map', settings);
    }

    //-------------------------------------------------------------
    // Modified registerDeviceByAccount on June 14th 2017 to
    // delete device ids for the current user before inserting a new one
    // this guarantees there is only one device registered for notifications.
    // --Raja D.
    //-------------------------------------------------------------
    registerDeviceByAccount(account: Account): void {
        let deviceToken: string = this._storage.get('deviceToken') || '';

        if (!deviceToken) {
            console.log("--- no deviceToken ----- ");
            this.loader.instance.hide();
            this.nextScreen(account);
            return;
        }
        console.log("---- deviceToken::", deviceToken);
        // Remove token to avoid register multiple times the same device.
        //this._storage.set('deviceToken', '');
        this.subscriptions.push(
            this._accountApi.deleteDevices(account.id).subscribe((response: any) => {
                console.log('devices deleted successfully')
            },
                (error: Error) => {
                    console.log('Error: ', error.stack)
                    this.nextScreen(account);
                    this.loader.instance.hide();
                }
            ));

        this.subscriptions.push(
            this._accountApi.createDevices(account.id, {
                uuid: deviceToken
            }).subscribe(
                (response: any) => {
                    console.log('device registered successfully')
                    this.nextScreen(account);
                    this.loader.instance.hide();
                },
                (error: Error) => {
                    console.log('Error: ', error.stack)
                    this.nextScreen(account);
                    this.loader.instance.hide();
                }
                )
        )
    }

    /**
     * @method createAccount
     **/
    createAccount(userEmail: string, fbId: string, fbUser: FacebookUser, accountId?: string) {
        // let invitedBy = appSettings.getString('invitationCode', null);
        let data: any = {
            email: userEmail,
            password: 'xdh800',
            displayName: fbUser.name,
            firstName: fbUser.first_name,
            lastName: fbUser.last_name,
            gender: fbUser.gender,
            ageRange: fbUser.age_range,
            fbHometown: fbUser.hometown,
            birthday: fbUser.birthday,
            photo: { path: fbUser.picture }
        };
        if (accountId) {
            data.id = accountId;
        }
        this.subscriptions.push(
            this._accountApi.upsert(data).subscribe((account: Account) => {
                if (accountId) {
                    this.login(account)
                } else {
                    this.createSocialCredential(account, fbId);
                }
            }, (err: any) => {
                console.log("Error in the server! " + err.message);
                this.loader.instance.hide();
                if (
                    err.name == 'ValidationError' &&
                    err.statusCode == 422 &&
                    err.details &&
                    err.details.messages &&
                    err.details.messages.email &&
                    err.details.messages.email[0] == 'Email already exists'
                ) {
                    this.socialCredentialError = true
                    this.login(data);
                }
            })
        );
    }

    /**
     * @method createSocialCredential
     **/
    createSocialCredential(account: Account, fbId: string) {
        this._zone.run(() => {
            this.subscriptions.push(
                this._accountApi.createSocialCredentials(account.id, { facebookId: fbId })
                    .subscribe(res => {
                        if (!this.socialCredentialError) {
                            this.login(account)
                        }
                    }, err => {
                        this.loader.instance.hide();
                        this.processing = false;
                        alert(err.message)
                    })
            );
        })
    }

    /**
     * @method ngOnDestroy
     **/
    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
    }
}
