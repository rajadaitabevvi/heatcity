import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { NgModule } from "@angular/core";

import { loginRoutes } from "./login.routing";
import { LoginComponent } from "./login.component";
import { FacebookLoginComponent } from "./facebook-login/facebook-login.component";
import { RequestInvitationComponent } from "./request-invitation/request-invitation.component";
import { InvitationCodeComponent } from "./invitation-code/invitation-code.component";
import { SharedModule } from "../shared/shared.module";
import { AppNotificationsModule } from '../app-notifications/app-notifications.module';

@NgModule({
  imports: [
    NativeScriptModule,
    NativeScriptFormsModule,
    NativeScriptRouterModule,
    NativeScriptRouterModule.forChild(loginRoutes),
    SharedModule,
    AppNotificationsModule
  ],
  exports: [ NativeScriptRouterModule ],
  declarations: [
    LoginComponent,
    FacebookLoginComponent,
    RequestInvitationComponent,
    InvitationCodeComponent
  ]
})
export class LoginModule { }
