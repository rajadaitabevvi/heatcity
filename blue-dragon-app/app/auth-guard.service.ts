import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AccountApi } from './shared/sdk/services';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate {

  private currentAccountSubject: Subject<{ accountId: string }> = new Subject<{ accountId: string }>();

  constructor(
    private router: Router,
    private accountApi: AccountApi
  ) { }

  canActivate() {
    if (this.accountApi.isAuthenticated()) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }

  loginAccount(accountId:string): void {
    this.currentAccountSubject.next({ accountId});
  }

  onLoginAccount(): Observable<any> {
    return this.currentAccountSubject.asObservable();
  }
}