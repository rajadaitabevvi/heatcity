import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { AccountApi } from '../../shared/sdk/services/custom/Account';
import { Account } from '../../shared/sdk/index';
import { topmost } from "ui/frame";
import { Page } from "ui/page";
import { TextField } from "ui/text-field";
import { TextView } from "ui/text-view";

import { LoadingIndicator } from "nativescript-loading-indicator";

declare const UIControlContentVerticalAlignmentTop: any;

@Component({
    selector: 'edit-user',
    moduleId: module.id,
    templateUrl: 'edit-user.component.html',
    styleUrls: ['edit-user.component.css']
})

export class editUserComponent implements OnInit, AfterViewInit{
    public currentAccount;
    public userLocation = "";
    public isUser = false;

    private loader: {
        instance: LoadingIndicator,
        options: any,
        loading: boolean
    } = {
            loading: false,
            instance: new LoadingIndicator(),
            options: {
                message: 'Loading...',
                progress: 0,
                android: {
                    indeterminate: true,
                    cancelable: false,
                    max: 100,
                    progressNumberFormat: "%1d/%2d",
                    progressPercentFormat: 0.53,
                    progressStyle: 1,
                    secondaryProgress: 1
                },
                ios: {
                    square: false,
                    margin: 10,
                    dimBackground: true,
                    color: "#fff",
                    mode: 'MBProgressHUDModeText'// see iOS specific options below
                }
            }
        };

    constructor(private router: Router, private route: ActivatedRoute,
        private _accountApi: AccountApi, private routerExtensions: RouterExtensions,
        private _page: Page) {
        this.route.params
            .subscribe((params) => {
                if (params['account']) {
                    this.currentAccount = JSON.parse(params['account']);
                    console.log(JSON.stringify(this.currentAccount));
                    this.currentAccount.name = this.currentAccount.firstName + " " + this.currentAccount.lastName;
                    this.currentAccount.birthday = new Date(this.currentAccount.birthday);
                    this.isUser = true;
                    if (this.currentAccount.hometown) this.userLocation = this.currentAccount.hometown
                    else if (this.currentAccount.fbHometown && this.currentAccount.fbHometown.name)
                        this.userLocation = this.currentAccount.fbHometown.name
                    else this.userLocation = ''
                }
            });
    }

    ngOnInit() {
        this.loader.instance.show(this.loader.options);
        if (topmost().ios) {
            // get the view controller navigation item
            const controller = topmost().ios.controller;
            const navigationItem = controller.visibleViewController.navigationItem;
            // hide back button
            navigationItem.setHidesBackButtonAnimated(true, false);
        }
        this.loader.instance.hide();
        
    }

    ngAfterViewInit() {
        if (topmost().ios) {
            let txtField: TextView = <TextView>this._page.getViewById("biotxt");
            txtField.ios.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        }
    }

    public updateProfile() {
        const fullName = this.currentAccount.name.split(" ");
        console.log(fullName);
        let that = this;
        this.loader.instance.show(this.loader.options);
        let updateAccount = {
            "username":this.currentAccount.username,
            "displayName": this.currentAccount.displayName,
            "bio": this.currentAccount.bio,
            "hometown": this.userLocation,
            //"fbHometown.name": this.currentAccount.fbHometown.name,
        }
        this._accountApi.updateAttributes(this.currentAccount.id, updateAccount).subscribe((result) => {
            console.log(JSON.stringify(result));
            that.loader.instance.hide();
            // that.routerExtensions.back();
        }, (error) => {
            alert(error.message);
            console.error("Error While Updating Profile -> " + error.message);
            that.loader.instance.hide();
        });
        this.routerExtensions.navigate(['/user-profile/' + this.currentAccount.id]);


    }

    public goToBack() {
        this.routerExtensions.back();
    }

    
}
