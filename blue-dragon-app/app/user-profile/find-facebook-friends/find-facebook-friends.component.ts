import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular';

@Component({
    selector: 'app-find-facebook-friends',
    moduleId: module.id,
    templateUrl: 'find-facebook-friends.component.html',
    styleUrls: ['find-facebook-friends.component.css']
})

export class findFacebookFriendsComponent implements OnInit {
    users = ['Grace Webb', 'Grace Webb', 'Grace Webb', 'Grace Webb', 'Grace Webb', 'Grace Webb', 'Grace Webb', 'Grace Webb', 'Grace Webb']
    constructor(
        private routerExtensions: RouterExtensions, ) { }

    goToBack() {
        this.routerExtensions.back();
    }
    invite(name) {
        console.log('Request is send to ' + name);
    }
    ngOnInit() { }
}