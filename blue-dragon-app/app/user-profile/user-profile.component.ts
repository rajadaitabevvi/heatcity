import { Component, OnInit, AfterViewInit, OnDestroy, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { isIOS } from "platform";
import { Page } from "ui/page";
import { topmost } from "ui/frame";
import * as frameModule from "ui/frame";
import * as ImgModule from "ui/image";
import { Account, Friend } from '../shared/sdk/models';
import { AccountApi, FriendApi } from '../shared/sdk/services';
import { StorageNative } from '../shared/sdk/storage/storage.native';
import { LoopBackConfig } from '../shared/sdk';
import { BASE_URL, API_VERSION } from '../shared/base.api';
import { RouterExtensions } from "nativescript-angular/router";
import { AppNotificationsService } from '../app-notifications/app-notifications.service';
import * as application from "application";
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import { LoadingIndicator } from "nativescript-loading-indicator";
import * as googleAnalytics from "nativescript-google-analytics";
import { alert } from "../shared/dialog-util";
import { LogoutService } from '../shared/logout.service';
import dialogs = require("ui/dialogs");
import { LoopBackFilter } from '../shared/sdk/models/BaseModels';
import { BottomBar, BottomBarItem, TITLE_STATE, SelectedIndexChangedEventData } from 'nativescript-bottombar';
import { UserSettingsComponent } from "./user-settings/user-settings.component";
import { ListView } from "ui/list-view";
import { CheckInsService } from "../check-ins/check-ins.service"
import { VisitApi } from '../shared/sdk/services/custom/Visit';

declare var UITableViewCellSelectionStyle: any;
@Component({
    moduleId: module.id,
    selector: "app-user-profile",
    templateUrl: "user-profile.component.html",
    styleUrls: ['user-profile-common.css', 'user-profile.component.css']
})
export class UserProfileComponent implements OnInit, OnDestroy, AfterViewInit {

    private subscriptions: Subscription[] = new Array<Subscription>();

    public loggedInId: string;
    public accountId: string;
    public currentAccount;
    public bio;
    public showUserProfile = false;
    public visits;
    public Isdata = false;
    public points = 0;
    public isLogInAccount = true;
    public following = [];
    public followers = [];
    public isFriend = false;
    public friendId: string;
    private loader: {
        instance: LoadingIndicator,
        options: any,
        loading: boolean
    } = {
            loading: false,
            instance: new LoadingIndicator(),
            options: {
                message: 'Loading...',
                progress: 0,
                android: {
                    indeterminate: true,
                    cancelable: false,
                    max: 100,
                    progressNumberFormat: "%1d/%2d",
                    progressPercentFormat: 0.53,
                    progressStyle: 1,
                    secondaryProgress: 1
                },
                ios: {
                    square: false,
                    margin: 10,
                    dimBackground: true,
                    color: "#fff",
                    mode: 'MBProgressHUDModeText'// see iOS specific options below
                }
            }
        };
    private _myPostsList: ListView;

    constructor(
        private accountApi: AccountApi,
        private friendApi: FriendApi,
        private page: Page,
        private router: Router,
        private storage: StorageNative,
        private route: ActivatedRoute,
        private _ngZone: NgZone,
        private routerExtensions: RouterExtensions,
        private logoutService: LogoutService,
        private appNotifications: AppNotificationsService,
        private checkInsService: CheckInsService,
        private _visitApi: VisitApi,
    ) {
        // LoopBackConfig.setBaseURL(BASE_URL);
        // LoopBackConfig.setApiVersion(API_VERSION);
        // this.page.actionBarHidden = false;

        this.route.params.subscribe((params) => {
            this.accountId = params['id'];

        });
        this.isLogInAccount = this.isUserProfile(this.accountId);
        //  this.getAccountDetail();
        // this.getVisit();
        // this.findFollwers();

    }



    public onLoaded() {
        this.getAccountDetail();
        this.getVisit();
        this.findFollwers();
    }

    public ngOnInit() {
        this.loader.instance.show(this.loader.options);
        if (topmost().ios) {
            // get the view controller navigation item
            const controller = frameModule.topmost().ios.controller;
            const navigationItem = controller.visibleViewController.navigationItem;
            // hide back button
            navigationItem.setHidesBackButtonAnimated(true, false);

        }
        console.log("ngONINt");
        this.checkInsService.checkInPostrecevie().subscribe(() => {
            this.loader.instance.show(this.loader.options);
            this.getVisit();
        })
        // this.getAccountDetail();
        //  this.getVisit();
        // this.findFollwers();
        this.page.on("loaded", this.onLoaded, this);
    }

    public ngAfterViewInit() {
        this._myPostsList = <ListView>this.page.getViewById("lvMyPosts");
    }
    public ngOnDestroy() {

    }
    public onClickUser(args) {
        // this.routerExtensions.navigate(['/user-profile/' + this.userid], {
        //     clearHistory: true,
        //     animated: false
        // });
    }
    public onClickPosts(args) {
        this.routerExtensions.navigate(['/posts'], {
            clearHistory: true,
            animated: false
        });
    }
    public onClickDiscover(args) {
        this.routerExtensions.navigate(['/map'], {
            clearHistory: true,
            animated: false
        });
    }
    public onClickNotification(args) {
        this.routerExtensions.navigate(['/notification'], {
            clearHistory: true,
            animated: false
        });
    }

    public listViewCallbacks(data) {
        try {
            if (data.action === "delete") {
                this.loader.instance.show(this.loader.options);
                this.subscriptions.push(
                    this._visitApi.deleteById(data.data).subscribe((result) => {
                        this.getVisit();
                    }, (error) => {
                        this.errorHandler(error);
                        console.error("Error While Deleting Friends -> " + error.message);
                        this.loader.instance.hide();
                    })
                );
            } else if (data.action === "edit") {

            } else
                if (data.action === 'like_unlike') {
                    if (this._myPostsList) this._myPostsList.refresh();
                }
        } catch (error) {
            console.log("Refresh Error" + error);
        }
    }

    public listViewItemLoading(args): void {
        if (isIOS) {

            args.ios.selectionStyle = UITableViewCellSelectionStyle.UITableViewCellSelectionStyleNone;
        }
    }

    public getAccountDetail() {
        const filter: LoopBackFilter = {
            include: [{ "relation": "friends", "scope": { "fields": ["Id"] } }]
        }
        try {
            let that = this;
            this.accountApi
                .findById(this.accountId, filter)
                .subscribe((accoutDetail: any) => {
                    that._ngZone.run(() => {
                        this.currentAccount = accoutDetail;
                        this.bio = this.currentAccount.bio.slice(0, 130);
                        this.showUserProfile = true;
                    });

                }, (error) => {
                    this.errorHandler(error);
                    console.error("Error While Getting Account Details -> " + error.message);
                    this.loader.instance.hide();
                });
        } catch (error) {
            console.error("ErR -> " + error.message);
        }


    }

    public getVisit() {
        this._ngZone.run(() => {
            let filter: LoopBackFilter = {
                include: ["account", { "location": "establishment" },
                    { "relation": "comments", "scope": { "fields": ["accountId", "comment"] } },
                    { "relation": "likes", "scope": { "fields": ["accountId", "id"] } }],
                where: { "validity": 100 },
                order: "createdAt DESC"
            }
            this.accountApi
                .getVisits(this.accountId, filter)
                .subscribe((visits) => {
                    this.visits = visits;
                    this.Isdata = true;
                    this.points = this.countPoint();
                    this.loader.instance.hide();

                }, (error) => {
                    this.errorHandler(error);
                    console.error("Error While Getting Visits -> " + error.message);
                    this.loader.instance.hide();
                });
        });
    }
    public countPoint() {
        let count = 0;
        this.visits.forEach(visit => {
            if (visit.validity === 100) {
                count += visit.points;
            }
        });
        return count;
    }



    public isUserProfile(id: string): boolean {
        this.loggedInId = this.accountApi.getCurrentId();
        if (id !== this.loggedInId) {
            let filter: LoopBackFilter = {
                where: { "accountId": this.loggedInId, "friendId": id }
            }
            this.friendApi.find(filter).subscribe((result: any) => {
                if (result.length) {
                    this.friendId = result[0].id;
                    this.isFriend = true;
                }
            }, (error) => {
                this.errorHandler(error);
                console.error("Error While User Profile -> " + error.message);
                this.loader.instance.hide();
            })
        }
        return (id === this.loggedInId);
    }

    public startFollow() {
        let filter: LoopBackFilter = {
            where: { accountId: this.currentAccount.id, friendId: this.loggedInId },
        }
        this.isFriend = true;
        this.friendApi.find(filter).subscribe((following: any) => {
            let friendObj = new Friend();
            friendObj.accountId = this.loggedInId;
            friendObj.friendId = this.currentAccount.id;
            if (following.length) {
                friendObj.mutual = true;
                this.friendApi.updateAttributes(following[0].id, { mutual: true }).subscribe();
            } else {
                friendObj.mutual = false;
            }
            this.friendApi.create(friendObj).subscribe((result) => {
                this.friendId = result.id;
            }, (error) => {
                this.errorHandler(error);
                console.error("Error While Creating Friend -> " + error.message);
                this.loader.instance.hide();
            });
        }, (error) => {
            this.errorHandler(error);
            console.error("Error While Start Following -> " + error.message);
            this.loader.instance.hide();
        });
    }

    public unFollow() {
        dialogs.confirm({
            cancelButtonText: 'cancel',
            okButtonText: 'unfollow',
            title: 'Are you sure you want to unfollow ' + this.currentAccount.displayName + '?',
        }).then((result) => {
            if (result) {
                this.isFriend = false;
                this.friendApi.deleteById(this.friendId).subscribe();
                let filter: LoopBackFilter = {
                    where: { accountId: this.currentAccount.id, friendId: this.loggedInId },
                }
                this.friendApi.findOne(filter).subscribe((following: any) => {
                    console.log(following);
                    if (following.mutual === true) {
                        this.friendApi.updateAttributes(following.id, { mutual: false }).subscribe((result) => {
                        }, (error) => {
                            console.error("Error While Updating Attributes -> " + error.message);
                            this.loader.instance.hide();
                        });
                    }
                }, (error) => {
                    console.error("Error While UnFollowing -> " + error.message);
                    this.loader.instance.hide();
                });
            }
        })
    }

    public editUser() {
        const account = JSON.stringify(this.currentAccount);

        this.routerExtensions.navigate(['/edit-profile', { account: account }]);

        // this.routerExtensions.navigate(['/edit-profile', this.currentAccount]);
    }

    public followingUser() {
        const followingData = JSON.stringify({ type: 'Following', accountId: this.accountId })
        this.routerExtensions.navigate(['/following', { data: followingData }]);
    }

    public followerUser() {
        const followerData = JSON.stringify({ type: 'Follower', accountId: this.accountId })
        this.routerExtensions.navigate(['/following', { data: followerData }]);
    }

    public goToProfile(id: string): void {
        this.routerExtensions.navigate(['/user-profile', this.accountId]);
    }

    public goToBack(): void {
        this.routerExtensions.back();
    }

    public findFollwers() {
        let filter: LoopBackFilter = {
            where: { "friendId": this.accountId }
        }
        this.friendApi.find(filter).subscribe((followers) => {
            this.followers = followers;
        }, (error) => {
            this.errorHandler(error);
            console.error("Error While Finding Followers -> " + error.message);
            this.loader.instance.hide();
        });
    }

    public goToFollwer() {
        this.routerExtensions.navigate(['/user-profile', this.accountId])
    }

    public openUserSetting() {

        const account = JSON.stringify(this.currentAccount);
        this.routerExtensions.navigate(['/user-setting', { account: account }]);
    }
    // getAccount(id: string, showAddFriends?: boolean): void {
    //     this.subscriptions.push(
    //         this.accountApi.findOne({
    //             where: {
    //                 id: id
    //             }
    //         }).subscribe(
    //             (res: Account) => {
    //                 this.account = res;
    //                 let name = `user-profile-${this.account.email}`;
    //                 console.log('1--Account', res.displayName)
    //                 googleAnalytics.logView(name);
    //                 if (showAddFriends)
    //                     this.showAddFriends = showAddFriends;
    //                 this.isCurrent = this.isUserProfile(res.id);

    //             })
    //     )
    // }

    /*  getAccountFriends(id: string): void {
         this.subscriptions.push(
             this.friendApi.find({
                 where: {
                     accountId: id,
                     approved: true
                 },
                 include: 'friend'
             }).subscribe(
                 (friends: Friend[]) => {
                     this.friends = friends.map((friend: Friend) => {
                         if(friend.friend) {
                             return friend.friend;
                         }
                     });
                 },
                 error => this.errorHandler(error)
                )
            )
        } */

    /* public displayUserLocation(): any {
        let show;
        if (this.isCurrent) {
            show = this.account.settings &&
            this.account.settings.shareLocation &&
            this.account.hometown &&
            this.account.homecountry
        } else {
            if (this.currentAccount) {
                show = this.account.settings &&
                this.account.settings.shareLocation &&
                this.account.hometown &&
                this.account.homecountry &&
                this.currentAccount &&
                this.currentAccount.settings &&
                this.currentAccount.settings.shareLocation;
        } else {
            show = this.account.settings &&
                this.account.settings.shareLocation &&
                this.account.hometown &&
                this.account.homecountry;
        }
    }
    return show;
} */

    /* public countAccountVisits(accountId: string) {
       this.subscriptions.push(
           this.accountApi.countVisits(accountId).subscribe(
               res => {
                   if (res.count > 0) {
                       this.disableHistory = false;
                   } else {
                       this.disableHistory = true;
                   }
               },
               error => this.errorHandler(error)
           )
       );
   }
*/
    /**
     * @method handleFriendButton
     * @return {void}
     **/
    /* handleFriendButton(): void {
        if (this.isFriend == 2) {
            let text = `Are you sure you want to delete ${this.account.firstName || this.account.displayName}?`
            console.log('2--text', text);
            dialogs.confirm(text)
                .then((result) => {
                    console.log("Dialog result: " + result);
                    if (result) {
                        this.removeOfMyFriends();
                    }
                });
            // this.removeOfMyFriends();
        } else if (this.isFriend == 0) {
            this.addAsFriend();
        } else {
            alert('Friend Request has already been send');
        }
    }
 */
    /**
     * @method addAsFriend
     * @return {void}
     **/
    /*  addAsFriend() {
         let currentId = this.accountApi.getCurrentId();
         this.subscriptions.push(
             this.accountApi.linkFriends(currentId, this.account.id).subscribe(
                 res => {
                     this.isFriend = 1;
                     alert('A Friend Request has been sent to ' + this.account.firstName);
                 },
                 error => this.errorHandler(error)
             )
         );
     } */

    /**
     * @method removeOfMyFriends
     * @return {void}
     **/
    /* removeOfMyFriends(): void {
        this.loader.instance.show(this.loader.options);
        let currentId = this.accountApi.getCurrentId();
        this.subscriptions.push(
            this.accountApi.unlinkFriends(currentId, this.account.id).subscribe(
                res => this.removeMeFromFriendList(),
                err => this.removeMeFromFriendList()
            )
        );

    } */

    /* removeMeFromFriendList(): void {
        let currentId = this.accountApi.getCurrentId();
        this.subscriptions.push(
            this.accountApi.unlinkFriends(this.account.id, currentId).subscribe(
                res => { this.getAccount(this.account.id); this.loader.instance.hide(); },
                err => { this.getAccount(this.account.id); this.loader.instance.hide(); }
            )
        )
    } */



    /**
     * @method isUserFriend
     * @return {void}
     **/
    /* isUserFriend(): void {
        let userId = this.accountApi.getCurrentId();
        this.friendApi.find({
            where: {
                accountId: userId,
                friendId: this.account.id
            }
        }).subscribe((friends: Friend[]) => {
            if (Array.isArray(friends)) {
                if (friends.length > 0) {
                    if (friends[0].approved) {
                        this.isFriend = 2;
                    } else {
                        this.isFriend = 1;
                    }
                } else {
                    this.isFriend = 0;
                }
            }
        },
            error => this.errorHandler(error))
    }
 */


    /**
     * @method openSettings
     * @return {void}
     **/
    /* openSettings(): void {
        this.showSettings = true;
    } */

    /**
     * @method onCloseSettings
     * @return {void}
     **/
    /* onCloseSettings(): void {
        this.showSettings = false;
    } */

    /**
     * @method onCloseAddFriends
     * @return {void}
     **/
    /* onCloseAddFriends(): void {
        this.showAddFriends = false;
    } */

    /**
     * @method onFriendsClose
     * @return {void}
     **/
    /* onFriendsClose(): void {
        this.showFriends = false;
    } */

    /* showFriendsList(): void {
        if (this.isCurrent) {
            this.showFriends = true;
        }
    } */

    /**
     * @method showHistory
     * @return {void}
     **/
    /* showHistory(): void {
        if (!this.disableHistory) {
            this.routerExtensions.navigate(['/map', { accountHistoryId: this.account.id }]);
        }
    } */


    errorHandler(error: any): void {
        console.log('User profile Error', JSON.stringify(error));
        if (error && error.statusCode === 401) {
            this.logoutService.logout();
        }
    }
}
