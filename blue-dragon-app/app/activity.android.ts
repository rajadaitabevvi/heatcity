import * as application from "application"
import * as frame from "ui/frame";
var firebaseCommon = require("nativescript-plugin-firebase/firebase-common");
var branch = require("nativescript-branch-io-sdk");

@JavaProxy('com.tns.NativeScriptActivity')
class Activity extends android.app.Activity {

    private _callbacks: frame.AndroidActivityCallbacks

    protected onCreate(savedInstanceState: android.os.Bundle): void {
        if (!this._callbacks) {
            (<any>frame).setActivityCallbacks(this)
        }
        this._callbacks.onCreate(this, savedInstanceState, super.onCreate)
    }

    protected onNewIntent(intent: android.content.Intent): void {
        console.log('\x1b[33monNewIntent!!!!\x1b[0m');
        super.onNewIntent(intent)
        this.setIntent(intent);

        var extras = intent.getExtras();
        if (extras !== null) {
            var result = {
                foreground: false
            };

            var iterator = extras.keySet().iterator();
            while (iterator.hasNext()) {
                var key = iterator.next();
                if (key !== "from" && key !== "collapse_key") {
                result[key] = extras.get(key);
                }
            }
            if (firebaseCommon.firebase._receivedNotificationCallback === null) {
                firebaseCommon.firebase._launchNotification = result;
            } else {
                setTimeout(function() {
                    firebaseCommon.firebase._receivedNotificationCallback(result);
                });
            }
        }
    }

    protected onSaveInstanceState(outState: android.os.Bundle): void {
        this._callbacks.onSaveInstanceState(this, outState, super.onSaveInstanceState);
    }

    protected onStart(): void {
				branch.initializeAndroid();
        this._callbacks.onStart(this, super.onStart);
    }

    protected onStop(): void {
        this._callbacks.onStop(this, super.onStop);
    }

    protected onDestroy(): void {
        this._callbacks.onDestroy(this, super.onDestroy);
    }

    public onBackPressed(): void {
        this._callbacks.onBackPressed(this, super.onBackPressed);
    }

    public onRequestPermissionsResult(requestCode: number, permissions: Array<String>, grantResults: Array<number>): void {
        this._callbacks.onRequestPermissionsResult(this, requestCode, permissions, grantResults, undefined /*TODO: Enable if needed*/);
    }

    protected onActivityResult(requestCode: number, resultCode: number, data: android.content.Intent): void {
        this._callbacks.onActivityResult(this, requestCode, resultCode, data, super.onActivityResult);
    }
}
