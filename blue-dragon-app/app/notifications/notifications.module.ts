import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NgModule } from "@angular/core";
import {notificationRouting} from "./notifications.routing";
import { NotificationsComponent } from "./notifications.component";
import {AppNotificationsModule} from "../app-notifications/app-notifications.module";
// import { NotificationsService } from "./notifications.service";
import { SharedModule } from "../shared/shared.module";
import { StorageNative } from '../shared/sdk/storage/storage.native';

@NgModule({
  imports: [
    NativeScriptModule,
    AppNotificationsModule,
    SharedModule,
    notificationRouting,
  ],
  exports: [NotificationsComponent],
  declarations: [NotificationsComponent],
  providers: [StorageNative]
})
export class NotificationsModule { }
