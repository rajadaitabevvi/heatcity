import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class NotificationsService {
    private notificationsSubject: Subject<any> = new Subject<any>();
    private newNotificationSubject: Subject<any> = new Subject<any>();
    private pushNotificationSubject: Subject<any> = new Subject<any>();

    displayNotifications(show?: boolean): void {
        this.notificationsSubject.next(show);
    }

    onDisplayNotifications(): Observable<any> {
        return this.notificationsSubject.asObservable();
    }

    addNewNotification(notification: any): void {
        this.newNotificationSubject.next(notification);
    }

    onNewNotification(): Observable<any> {
        return this.newNotificationSubject.asObservable();
    }

    pushNotification(notification: any): void {
        this.pushNotificationSubject.next(notification);
    }

    onNewPushNotification(): Observable<any> {
        return this.pushNotificationSubject.asObservable();
    }

}