import { Component, OnInit } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { Page } from "ui/page";

@Component({
    moduleId: module.id,
    selector: "model-dialog",
    styleUrls: ["dialog.component.css"],
    templateUrl: "dialog.component.html",
})

export class DialogComponent implements OnInit {

    public currentdate;

    constructor(private params: ModalDialogParams, private page: Page) {
        this.currentdate = params.context;
        this.page.on("unloaded", () => {
            this.params.closeCallback();
        });
    }

    public ngOnInit() {

    }

    public close(result: string) {
        this.params.closeCallback(result);
    }
}