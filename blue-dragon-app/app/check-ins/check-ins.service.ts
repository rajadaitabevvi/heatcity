import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CheckInsService  {

  private checkInPostSubject: Subject<{  }> = new Subject<{  }>();
  

  constructor() { }


  checkInPostcall(): void {
    this.checkInPostSubject.next();
  }

  checkInPostrecevie(): Observable<any> {
    return this.checkInPostSubject.asObservable();
  }
}