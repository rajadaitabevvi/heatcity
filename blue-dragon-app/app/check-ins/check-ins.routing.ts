import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth-guard.service';
import { CheckInsComponent } from './check-ins.component';
// import { PostCardComponent } from "./post-card/post-card.component";
import { CommentComponent } from "./comment/comment.component";
import { CommentListComponent } from "./comment-list/comment-list.component";
import { CheckInsPostComponent } from './post/check-ins-post.component';
import { WrongCheckInComponent } from './wrong-check-in/wrong-check-in.component';

const mapRoutes: Routes = [
    { path: 'posts', component: CheckInsComponent, canActivate: [AuthGuard] },
    { path: 'comment/:id', component: CommentComponent, canActivate: [AuthGuard] },
    { path: 'post', component: CheckInsPostComponent, canActivate: [AuthGuard] },
    { path: 'wrong-checkin', component: WrongCheckInComponent, canActivate: [AuthGuard] },
];

export const checkInRouting: ModuleWithProviders = RouterModule.forChild(mapRoutes);
export const checkInNavigatableComponents=[
    CheckInsComponent,
    // PostCardComponent,
    CommentComponent,
    CommentListComponent,
    CheckInsPostComponent,
    WrongCheckInComponent
]