import { Component, OnInit, Input } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    moduleId: module.id,
    selector: 'comment-list',
    templateUrl: 'comment-list.component.html',
    styleUrls: ['comment-list.component.css']
})
export class CommentListComponent implements OnInit {
    @Input() public data;

    constructor(private _routerExtensions: RouterExtensions) {

    }

    public ngOnInit() {

    }

    public getAccountDetail(id) {
        this._routerExtensions.navigate(["/user-profile/" + id]);
    }
}
