import { Component, OnInit, ViewChildren, ElementRef, AfterViewInit, OnDestroy, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from "nativescript-angular/router";
import { LoopBackFilter, Comment } from '../../shared/sdk/models';
import { VisitApi } from '../../shared/sdk/services/custom/Visit';
import { AccountApi } from '../../shared/sdk/services/custom/Account';
import { topmost } from "ui/frame";
import * as frameModule from "ui/frame";
import { LoadingIndicator } from "nativescript-loading-indicator";
import { Page } from 'ui/page';
import { isIOS } from "platform";
import { TextField } from 'ui/text-field'
// import { PostCardComponent } from "../post-card/post-card.component";

import { KeyboardObserver } from '../../shared/keyboard-observer'
import { Subscription } from 'rxjs';

declare var UITableViewCellSelectionStyle: any;
@Component({
    moduleId: module.id,
    selector: 'comment-page',
    templateUrl: 'comment.component.html',
    styleUrls: ['comment.component.css'],
    providers: [KeyboardObserver]
})
export class CommentComponent implements OnInit, AfterViewInit, OnDestroy {
    // @ViewChildren(PostCardComponent) postcardcomponent: PostCardComponent;
    public currentPost = undefined;
    public commentList: Array<any>;
    public comment = new Comment();
    public visitId: string;
    public accountID: string;
    public showCard = false;
    private _commentTextField: TextField;
    public textviewLoadArgs: any;

    private _subscriptions: Array<Subscription> = [];
    //@ViewChildren('commentBox') commentBox: ElementRef;
    private loader: {
        instance: LoadingIndicator,
        options: any,
        loading: boolean
    } = {
            loading: false,
            instance: new LoadingIndicator(),
            options: {
                message: 'Loading...',
                progress: 0,
                android: {
                    indeterminate: true,
                    cancelable: false,
                    max: 100,
                    progressNumberFormat: "%1d/%2d",
                    progressPercentFormat: 0.53,
                    progressStyle: 1,
                    secondaryProgress: 1
                },
                ios: {
                    square: false,
                    margin: 10,
                    dimBackground: true,
                    color: "#fff",
                    mode: 'MBProgressHUDModeText'// see iOS specific options below
                }
            }
        };
    private iqKeyboard: IQKeyboardManager;
    public keyboardHeight: number = 0;

    constructor(
        private _page: Page,
        private routerExtensions: RouterExtensions,
        private visitApi: VisitApi,
        private accountAPi: AccountApi,
        private router: ActivatedRoute,
        private _keyboardObserver: KeyboardObserver,
        private _ngZone: NgZone
    ) {
        this.iqKeyboard = IQKeyboardManager.sharedManager();
        this.iqKeyboard.enable = false;
        this.router.params.subscribe((param) => {
            this.visitId = param['id'];
        });
    }

    public ngOnInit() {
        this.loader.instance.show(this.loader.options);
        if (topmost().ios) {
            // get the view controller navigation item
            const controller = frameModule.topmost().ios.controller;
            const navigationItem = controller.visibleViewController.navigationItem;
            // hide back button
            navigationItem.setHidesBackButtonAnimated(true, false);
            this._subscriptions.push(this._keyboardObserver.heightChange$().subscribe((height) => {
                // if(height < 80 || height > this.keyboardHeight)
                this._ngZone.run(() => {
                  this.keyboardHeight = height;
                });
            }));

        }
        this.getVisitByID();
        this.getcommentList();
        this.loader.instance.hide();
        // console.log(JSON.stringify(this.postcardcomponent.data));
    }

    public ngAfterViewInit(): void {
        /*setTimeout(function () {
            this.commentBox.nativeElement.focus();
        }, 10000);*/
        this._commentTextField = <TextField>this._page.getViewById('commentBox');
    }

    public goBack() {
        console.log("On TAP goBAck");
        this.routerExtensions.back();
    }

    public getVisitByID() {
        let filter: LoopBackFilter = {
            include: ["account", { "location": "establishment" },
                { "relation": "comments", "scope": { "fields": ["accountId", "comment"] } },
                { "relation": "likes", "scope": { "fields": ["accountId", "id"] } }],
        };
        this.visitApi.findById(this.visitId, filter).subscribe((visitData) => {
            this.currentPost = visitData;
            this.showCard = true;
        }, (error) => {
            alert(error.message);
            console.error("Error While Getting Current Post -> " + error.message);
            this.loader.instance.hide();
        });
    }

    public getcommentList() {
        this.commentList = [];
        const commentFilter: LoopBackFilter = {
            include: ["account"],
            order: "createdAt ASC",
        };
        this.visitApi
            .getComments(this.visitId, commentFilter)
            .subscribe((data) => {
                data.splice(index,1);
                this.commentList = data;
                this.loader.instance.hide();
            }, (error) => {
                alert(error.message);
                console.error("Error While Getting Comment List -> " + error.message);
                this.loader.instance.hide();
            });
    }

    public saveComment() {
        // console.log("save Comment"+this.accountAPi.getCurrentId());
        this.keyboardHeight = 0;
        if (this.comment.comment !== "" && this.comment.comment !== null && this.comment.comment !== "null") {
            this.comment.accountId = this.accountAPi.getCurrentId();
            this.comment.visitId = this.visitId;
            this.comment.createdAt = new Date();
            this.comment.updatedAt = new Date();
            this.visitApi
                .createComments(this.visitId, this.comment)
                .subscribe((data) => {
                    this.getcommentList();
                    // this.postcardcomponent.vistiCommentCount();
                    // console.log(JSON.stringify(data));
                }, (error) => {
                    alert(error.message);
                    console.error("Error While Creating Comment -> " + error.message);
                    this.loader.instance.hide();
                });
            this.comment.comment = "";
        }
    }

    hideKeyboard() {
        if (this._commentTextField) this._commentTextField.dismissSoftInput()
        this.keyboardHeight = 0;
    }

    public listViewItemLoading(args): void {
        if (isIOS) {

            args.ios.selectionStyle = UITableViewCellSelectionStyle.UITableViewCellSelectionStyleNone;
        }
    }

    public onTextViewLoaded(args) {
        if (topmost().ios) {
            this.textviewLoadArgs = args;
            // args.object.ios.becomeFirstResponder();
            args.object.ios.inputAccessoryView = UIView.alloc().init();

        }
    }
    public onTextViewFocus(args) {
        if (topmost().ios) {
            console.log("textviewFocus");
            this.textviewLoadArgs.object.ios.becomeFirstResponder();
            args.object.ios.inputAccessoryView = UIView.alloc().init();
            this.keyboardHeight = 258;

        }
    }

    public ngOnDestroy(): void {
        this._subscriptions.forEach((subscription) => {
            subscription.unsubscribe();
        })
    }
}
