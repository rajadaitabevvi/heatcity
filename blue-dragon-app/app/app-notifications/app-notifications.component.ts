import { Component, OnInit, OnDestroy, Input, NgZone } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AppNotificationsService } from './app-notifications.service';
import { Page } from 'ui/page';
import { View } from 'ui/core/view';
import view = require('ui/core/view');
import { AnimationCurve } from "ui/enums";
import { NotificationsService } from '../notifications/notifications.service';
import { RouterExtensions } from "nativescript-angular/router";
import * as moment from 'moment';

interface NotificationInterface {
    text: string;
    status: boolean;
}
@Component({
    moduleId: module.id,
    selector: "app-notifications",
    templateUrl: "app-notifications.component.html",
    styleUrls: ['app-notifications-common.css']
})
export class AppNotificationsComponent implements OnInit, OnDestroy {
    /** @type {Subscription[]} **/
    private subscriptions: Subscription[] = new Array<Subscription>();

    private show: boolean = false;

    @Input('custom') custom: boolean;

    @Input('name') name: string;

    private notification: NotificationInterface;

    private notificiationTimeout: any;

    private pushNotification: { instance: any, type: string, body: string };

    private pushNotificiationTimeout: any;

    private showPush: boolean = false;

    /**
     * @method constructor
     **/
    constructor(
        private appNotificationService: AppNotificationsService,
        private page: Page,
        private zone: NgZone,
        private notificationsService: NotificationsService,
        private routerExtensions: RouterExtensions
    ) { console.log("App Notification Component constructor"); }

    /**
     * @method ngOnInit
     **/
    ngOnInit() {
        console.log("App Notification Component oninit");
        this.subscriptions.push(
            this.appNotificationService.onDisplayNotification(this.name).subscribe(
                (notification: NotificationInterface) => {
                    if (this.custom) {
                        this.showNotification();
                    } else {
                        this.showNotification(notification);
                    }
                }
            )
        );
        this.subscriptions.push(
            this.notificationsService.onNewNotification().subscribe(
                (notification: { instance: any, type: string, body: string }) => {
                    this.zone.run(() => {
                        if (notification.type !== (undefined || "undefined" || null || "")) {
                            // console.log("FireBase Message on Sunscripbe" + JSON.stringify(notification.body));
                            this.showPushNotification(notification);
                        }
                    });
                }
            )
        )
    }

    /**
     * @method showNotification
     **/
    showNotification(notification?: NotificationInterface): void {
        if (this.notificiationTimeout) {
            clearTimeout(this.notificiationTimeout);
            this.notificiationTimeout = null;
        }
        if (this.show) {
            this.showNewNotification(notification);
        } else {
            if (notification) {
                this.notification = notification;
            }
            this.show = true;
            this.openNotification();
        }

    }

    /**
     * @method showNewNotification
     **/
    showNewNotification(notification?: any): void {
        let notificationId = this.custom ? 'custom-' + this.name : 'app-notification-' + this.name;
        let stackLayout = <View>this.page.getViewById(notificationId);
        if (stackLayout) {
            stackLayout.animate({
                opacity: 0,
                duration: 200
            }).then(() => {
                this.openNotification(notification);
            });
        }
    }

    /**
     * @method openNotification
     **/
    openNotification(notification?: any): void {
        let notificationId = this.custom ? 'custom-' + this.name : 'app-notification-' + this.name;
        let stackLayout = <View>this.page.getViewById(notificationId);
        if (stackLayout) {
            if (notification) {
                this.notification = notification;
            }
            stackLayout.translateY = -30;
            stackLayout.animate({
                translate: { x: 0, y: 0 },
                opacity: 1,
                duration: 1000,
                curve: AnimationCurve.easeOut
            });
            this.notificiationTimeout = setTimeout(() => {
                this.notificiationTimeout = null;
                this.closeNotification();
            }, 10000);
        }
    }

    /**
     * @method closeNotification
     **/
    closeNotification(): void {
        let notificationId = this.custom ? 'custom-' + this.name : 'app-notification-' + this.name;
        let stackLayout = <View>this.page.getViewById(notificationId);
        if (stackLayout) {
            stackLayout.animate({
                opacity: 0,
                duration: 200
            }).then(() => {
                this.zone.run(() => {
                    this.show = false;
                });
            });
        }
    }

    /**
     * @method closePushNotification
     **/
    showPushNotification(notification: { instance: any, type: string, body: string }): void {
        if (this.pushNotificiationTimeout) {
            clearTimeout(this.pushNotificiationTimeout);
            this.pushNotificiationTimeout = null;
        }
        if (this.showPush) {
            this.showNewPushNotification(notification);
        } else {
            this.updatePushNotification(notification);
            this.openPushNotification(notification);
        }
    }

    /**
     * @method closePushNotification
     **/
    showNewPushNotification(notification: { instance: any, type: string, body: string }): void {
        let notificationId = 'push-notification-' + this.name;
        let stackLayout = <View>this.page.getViewById(notificationId);
        if (stackLayout) {
            stackLayout.animate({
                opacity: 0,
                duration: 200
            }).then(() => {
                this.updatePushNotification(notification);
                this.openPushNotification(notification);
            });
        }
    }

    /**
     * @method closePushNotification
     **/
    closePushNotification(): void {
        let notificationId = 'push-notification-' + this.name;
        let stackLayout = <View>this.page.getViewById(notificationId);
        if (stackLayout) {
            stackLayout.animate({
                opacity: 0,
                duration: 200
            }).then(() => {
                this.zone.run(() => {
                    this.showPush = false;
                });
            });
        }
    }

    /**
     * @method openPushNotification
     **/
    openPushNotification(notification: { instance: any, type: string, body: string }): void {
        let notificationId = 'push-notification-' + this.name;
        let stackLayout = <View>this.page.getViewById(notificationId);
        if (stackLayout) {
            stackLayout.translateY = -30;
            stackLayout.animate({
                translate: { x: 0, y: 0 },
                opacity: 1,
                duration: 1000,
                curve: AnimationCurve.easeOut
            });
            let timeout: number = 5000;
            if (notification.type == 'FRIEND_VISIT' || notification.type == 'ESTABLISHMENT_SHARE') {
                timeout = 25000;
            }
            this.pushNotificiationTimeout = setTimeout(() => {
                this.pushNotificiationTimeout = null;
                this.closePushNotification();
            }, timeout);
        }
    }

    /**
     * @method dismissPushNotification
     **/
    dismissPushNotification(): void {
        if (this.pushNotificiationTimeout) {
            clearTimeout(this.pushNotificiationTimeout);
            this.pushNotificiationTimeout = null;
        }
        this.closePushNotification();
    }

    /**
     * @method updatePushNotification
     **/
    updatePushNotification(notification: { instance: any, type: string, body: string }): void {
        this.pushNotification = notification;
        this.showPush = true;
    }

    private isJSON(data: string) {
        try {
            JSON.parse(data);
        } catch (e) {
            return false;
        }
        return true;
    }

    /**
     * @method goToEstablishment
     **/
    goToEstablishment(id: string): void {
        this.routerExtensions.navigate(['/establishment', id], {
                transition: {
                    name: 'slideLeft',
                    duration: 500,
                    curve: 'linear'
                }
            });
    }

    public goToCheckInSelf(args) {
        const data = JSON.stringify({ type: 'self' });
        this.routerExtensions.navigate(['/posts', { data: data }], {
                transition: {
                    name: 'slideLeft',
                    duration: 500,
                    curve: 'linear'
                }
            });
    }

    public goToCheckInFollowing(args) {
        const data = JSON.stringify({ type: 'following' });
        this.routerExtensions.navigate(['/posts', { data: data }], {
                transition: {
                    name: 'slideLeft',
                    duration: 500,
                    curve: 'linear'
                }
            });
    }

    public goToComment(id) {
        // this.routerExtensions.navigate(['/comment', id], {
        //         transition: {
        //             name: 'slideLeft',
        //             duration: 500,
        //             curve: 'linear'
        //         }
        //     });
        
        this.routerExtensions.navigate(['/notification'], {
            transition: {
                name: 'slideLeft',
                duration: 500,
                curve: 'linear'
            }
        });

    }

    public goToNotification(args) {
        this.routerExtensions.navigate(['/notification'], {
                transition: {
                    name: 'slideLeft',
                    duration: 500,
                    curve: 'linear'
                }
            });
    }

    /**
     * @method getTime
     **/
    getTime(date: string): string {
        return moment(date).fromNow();
    }

    /**
     * @method ngOnDestroy
     **/
    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
    }
}
