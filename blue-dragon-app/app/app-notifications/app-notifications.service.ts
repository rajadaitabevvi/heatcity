import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

interface NotificationInterface {
    text: string;
    status: boolean;
}

@Injectable()
export class AppNotificationsService {
    private appNotificationsSubjects: any = {};

    displayNotification(eventName: string, notification?: NotificationInterface, ): void {
        if(this.appNotificationsSubjects[eventName]) {
            console.log('Notification name: ', eventName);
            this.appNotificationsSubjects[eventName].next(notification);
        } else {
            console.log('There is no notification component with the name: ', eventName);
        }
    }

    onDisplayNotification(eventName: string): Observable<any> {
        if(!this.appNotificationsSubjects[eventName]) {
            this.appNotificationsSubjects[eventName] = new Subject();
        }
        return this.appNotificationsSubjects[eventName].asObservable();
    }
}