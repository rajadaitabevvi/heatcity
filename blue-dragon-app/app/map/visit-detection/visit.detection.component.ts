import { Component, OnDestroy, NgZone } from '@angular/core';
import { RealTime, VisitApi, AccountApi, EstablishmentApi } from '../../shared/sdk/services';
import { Visit, Establishment, FireLoopRef } from '../../shared/sdk/models';
import { DistancesInterface } from '../map.component';
import { Position } from 'nativescript-google-maps-sdk';
import geolocation = require('nativescript-geolocation');
import { Location } from 'nativescript-geolocation';
import { Accuracy } from "ui/enums";
import timer = require('timer');
import * as moment from 'moment';
import { Subscription } from 'rxjs/Subscription';
import * as googleAnalytics from "nativescript-google-analytics";
import { VisitDetectionService } from './visit.detection.service';
import * as utils from "utils/utils";
import { LogoutService } from '../../shared/logout.service';
import { StorageNative } from '../../shared/sdk/storage/storage.native';

import { BackgroundGeolocation } from "nativescript-background-geolocation-lt";
import { BackgroundFetch } from "nativescript-background-fetch";
import {CheckInsService}from "../../check-ins/check-ins.service"

export interface VisitReferences {
  alreadyvisited: number,
  timer: number,
  current: Position,
  intervalId: number,
  establishments: Establishment[],
  establishmentSubscription?: Subscription
}

export interface VisitSettings {
  checkinAfter: number,
  reCheckinAfter: number,
  checkinRadius: number
}

@Component({
  moduleId: module.id,
  selector: 'VisitDetection',
  templateUrl: 'visit.detection.component.html',
  styleUrls: ['visit.detection.component.css']
})

export class VisitDetectionComponent implements OnDestroy {
  /**
   * @type {Visit}
   **/
  public visit: Visit = new Visit();
  /**
   * @type {Visit}
   **/
  public lastVisit: Visit = new Visit();
  /**
   * @type {Establishment}
   **/
  public establishment: Establishment = new Establishment();
  /**
   * @type {VisitReferences}
   **/
  private references: VisitReferences;
  /**
   * @type {VisitSettings}
   **/
  private settings: VisitSettings = {
    checkinAfter: 5, // Checkin after 10 minutes in venue
    reCheckinAfter: 90, // Only re-checkin after 90 minutes
    checkinRadius: 100 // Will automatically checkin if user is within 80 meters of the venue
  }

  public around: Location;

  private watchId: any;

  /**
   * @method constructor
   * @param {VisitApi} visitApi description
   * @param {AccountApi} accountApi description
   **/
  constructor(
    private visitApi: VisitApi,
    private accountApi: AccountApi,
    private establishmentApi: EstablishmentApi,
    private zone: NgZone,
    private logoutService: LogoutService,
    private visitDetectionService: VisitDetectionService,
    private storage: StorageNative,
    private checkInsService:CheckInsService,
  ) {
    console.log('VISIT DETECTOR LOADED');
    googleAnalytics.logView('visit-detection');
    this.reset();
    this.getLocation();
    let sub = this.visitDetectionService.onStartVisitDetector().subscribe(
      () => {
        //if(!this.references.intervalId) {
        //this.setupVisitDetector();
        // this.startGeoLocation();
        //this.getLocation();
        //}
        sub.unsubscribe();
      }
    );
  }

  /* private startGeoLocation(): void {
    BackgroundGeolocation.on("location", this.onLocation.bind(this));
    // BackgroundFetch.configure({stopOnTerminate: false}, () => {
       //    console.log('[event] BackgroundFetch');
         // BackgroundFetch.finish(UIBackgroundFetchResult.NewData);
       // });

    // 2. Configure it.
    BackgroundGeolocation.configure({
      debug: true,
      desiredAccuracy: 0,
      distanceFilter: 50,
      activityRecognitionInterval: 10000,
      //useSignificantChangesOnly: true,
      //heartbeatInterval: 60,
      //preventSuspend: true,
      stopOnTerminate: false,
      autoSync: true
    }, (state) => {
      // 3. Plugin is now ready to use.
      //alert("state: " + state.stopOnTerminate);
      //state.stopOnTerminate = false;
      //console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa => " + (state.stopOnTerminate));
      if (!state.enabled) {
        BackgroundGeolocation.start();
      }
      //Add a comment to this line
    });
  }  */

  public getLocation() {
    this.visitDetectionService.onNewUserCurrentPosition()
      .subscribe((res: { position: Location }) => {
        this.onLocation(res.position);
      })
  }

  private onLocation(location: any) {
    if (!location || !location.coords) {
      return;
    }

    let position: Location = new Location()
     position.latitude = location.coords.latitude;
     position.longitude = location.coords.longitude;
    //position.latitude = 23.0424852;
    //position.longitude = 72.5143736;
    //position.latitude = 23.03834499;
    //position.longitude = 72.63087407;

    position.speed = location.coords.speed;
    // We don't get the horizontalAccuracy from the geo-location plugin,
    // So setting it to 0
    position.horizontalAccuracy = 0
    position.verticalAccuracy = 0

    this.storage.set('userPosition', {
      latitude: position.latitude,
      longitude: position.longitude,
      horizontalAccuracy: position.horizontalAccuracy,
      verticalAccuracy: position.verticalAccuracy,
      speed: position.speed,
      direction: position.direction
    });

    console.log('\x1b[33mVISIT DETECTOR - GOT LOCATION FROM DEVICE\x1b[0m');
    console.log(JSON.stringify(position));
    this.visitDetectionService.newUserPosition(position, true);
    this.around = position;
    if (!this.references.intervalId) {
      this.setupVisitDetector();
    }
  }


  /**
    * @method watchLocation
    * @return {void}
    **/
  /*  watchLocation(): void {

       // Default options
       let options: any = {
           desiredAccuracy   : Accuracy.high,
           updateDistance    : 5,
           minimumUpdateTime : 5000,
           iosAllowsBackgroundLocationUpdates: true,
           iosPausesLocationUpdatesAutomatically: false
       };

       // If there is a user position, lets use that to change update distance and min update time
       if (this.around && this.around.speed && this.around.horizontalAccuracy) {
           options.updateDistance    = this.around.horizontalAccuracy < 20 ? 5  :
                                       this.around.horizontalAccuracy < 50 ? 20 :
                                       this.around.horizontalAccuracy > 50 ? 40 : 50;
           options.minimumUpdateTime = this.around.speed === 0  ? 10000 :
                                       this.around.speed < 5    ? 6000  :
                                       this.around.speed < 10   ? 3000  :
                                       this.around.speed > 10   ? 500   : 5000;
       }
       if (this.watchId) {
           geolocation.clearWatch(this.watchId);
       }
       console.log('\x1b[33mSETTING UP WATCH LOCATION\x1b[0m');
       this.watchId = geolocation.watchLocation(
         (position: geolocation.Location) => {
               console.log('\x1b[33mVISIT DETECTOR - GOT LOCATION FROM DEVICE\x1b[0m');
               // console.log(position);
               // console.log(JSON.stringify(position));
               this.around = position;
               // if(!this.references.intervalId) {
               //   this.setupVisitDetector();
               // }
               if (
                   (this.around &&
                     (
                       (this.around.horizontalAccuracy - position.horizontalAccuracy) >  10 ||
                       (this.around.horizontalAccuracy - position.horizontalAccuracy) < -10
                     )
                     ||
                     (
                       (this.around.speed - position.speed) >  3 ||
                       (this.around.speed - position.speed) < -3
                     )
                   )
               ) {
                   this.visitDetectionService.newUserPosition(position, true);
                   this.watchLocation(); // Run again to use new settings
               } else {
                   // console.log('\x1b[33mUser is not Moving\x1b[0m');
                   this.visitDetectionService.newUserPosition(position, false);
               }
           },
           (err: Error) => {
               console.log('VISIT DETECTOR - GEOLOCATION ERROR:', err)
           },
           options
       )
   } */

  /**
   * @method setupVisitDetector
   * @description It will create a visitor dector, once detects a user has been in a place
   * for 10 minutes it will try to start a new visit, but if user moved more than 50 METERS
   * then the timer will start again from 0.
   **/

  setupVisitDetector(): void {

    console.log('SETTING UP VISIT DECTOR');
    this.references.intervalId = timer.setInterval(() => {
      //if(!this.watchId) {
      //this.watchLocation();
      //}
      let distance: number;

      // If there is no current location, then we set a far distance
      // So we start a new visit detection process
      if (!this.references.current) {
        distance = 100;
      } else {
        distance = this.calculateDistance(this.references.current, this.around);
      }

      // console.log('DISTANCE: ', distance, ' CHECKINRADIUS :', this.settings.checkinRadius);
      if (distance > this.settings.checkinRadius) {
        console.log('RESETING VISITOR COUNTER');
        //alert('RESETING VISITOR COUNTER');
        // Set current and reset timer
        if (this.visit && this.visit.id && !this.visit.exit) {
          this.registerExit();
        }
        if (this.around) {
          this.references.current = this.around;
        }
        this.references.timer = 0;
        this.references.alreadyvisited = 0;
      } else {
        this.references.timer = this.references.timer + 1000;
        console.log('TIMER: ', this.references.timer);
        console.log('checkinAfter TIMER: ', this.settings.checkinAfter);
        console.log('checkinAfter new TIMER: ', this.settings.checkinAfter * 6* 1000);
        // console.log(" ");
        // console.log(" ");
        //alert('TIMER: '+this.references.timer);
        if (this.references.timer % 6000 === 0) {
          let hour = moment().hours();
          let shift = hour >= 6 && hour <= 17 ? 'day' : 'night';
          console.log('CHECKING TIME: ', this.references.timer, shift);
          this.visitDetectionService.hourChange(shift);
        }
        if (this.references.timer >= (this.settings.checkinAfter * 6 * 1000)) {
          //if (this.references.timer >= 10000) {
          console.log('VISIT: FINDING NEARBY ESTABLISHMENT NOW');
          //alert('VISIT: FINDING NEARBY ESTABLISHMENT NOW');
          this.references.timer = 0;
          if (this.references.alreadyvisited == 0) {
            this.findNearByEstablishment()
          }
          else {
            console.log('NOT FINDING NEARBY ESTABLISHMENT NOW');
            //alert('NOT FINDING NEARBY ESTABLISHMENT NOW');
          }
        }
      }
    }, 10000);
  }

  findNearByEstablishment(): void {

    let query: any = {
      where: {
        geoLocation: {
          nearSphere: {
            $geometry: {
              type: "Point",
              coordinates: [this.around.longitude, this.around.latitude]
            },
            $maxDistance: this.settings.checkinRadius,
            $minDistance: 0
          }
        }
      },
      include: 'photos'
    };

    console.log('VISIT QUERY: ', JSON.stringify(query));

    if (this.references.establishmentSubscription) {
      this.references.establishmentSubscription.unsubscribe();
    }

    this.references.establishmentSubscription = this.establishmentApi.find(query).subscribe((establishments: Establishment[]) => {
      console.log("es-----------============>", establishments.length);
      if (establishments.length > 0) {
        // Registering visit right away
        console.log("Registering visit right away");
        let establishment = this.getClosestEstablishment(establishments);
        this.registerVisit(establishment);
      }
    },
      error => this.errorHandler(error))
  }

  getClosestEstablishment(establishments: Establishment[]): Establishment {
    let result: Establishment = establishments[0];
    let closestDistance = this.settings.checkinRadius + 1;
    establishments.forEach((establishment: Establishment) => {
      let position = new Position();
      position.latitude = establishment.geoLocation.coordinates[1];
      position.longitude = establishment.geoLocation.coordinates[0];
      let distance = this.calculateDistance(position, this.around);
      if (distance < closestDistance) {
        result = establishment;
        closestDistance = distance;
      }
    });
    return result;
  }

  registerVisit(establishment: Establishment): void {
    console.log('TRYING TO REGISTER VISIT NOW');
    let distance: number = this.calculateDistance(Position.positionFromLatLng(
      establishment.geoLocation.coordinates[1],
      establishment.geoLocation.coordinates[0]
    ), this.around);
    if (distance <= this.settings.checkinRadius) {
      let filter: any = {
        where: {
          accountId: this.accountApi.getCurrentId(),
          locationId: establishment.locationId,
          entry: {
            gt: moment().subtract(this.settings.reCheckinAfter, 'minutes').toISOString()
          }
        }
      };
      console.log("reCheckinAfter :: ", moment().subtract(this.settings.reCheckinAfter, 'minutes').toISOString());
      this.visitApi.find(filter).subscribe((visits: Visit[]) => {
        if (visits.length > 0) {
          console.log('User is already registered within the venue.');
          //alert('User is already registered within the venue.');
          this.visit = visits.shift();
        } else {
          if (!this.visit.id || this.visit.locationId != establishment.locationId) {
            if (this.visit.id && !this.visit.exit) {
              console.log("this.registerExit();");
              this.registerExit();
            }
            this.visit = new Visit();
            var date = new Date();
            var dd = date.setMinutes(date.getMinutes() - this.settings.checkinAfter);
            this.visit.entry = new Date(dd);
            this.visit.accountId = this.accountApi.getCurrentId();
            this.visit.locationId = establishment.locationId;
            this.visit.geoLocation = establishment.geoLocation;
            this.visit.validity = 50;

            this.visitApi.create(this.visit).subscribe((visit: Visit) => {
              this.zone.run(() => {
                this.visit = visit;
                this.establishment = establishment;
                this.checkInsService.checkInPostcall();
                this.visitDetectionService.newVisit(this.visit, this.establishment);
              });
              console.log('VISIT REGISTERED: ', JSON.stringify(visit));
            }, (error: Error) => {
              console.log('NOOO ERROR: ', error.message);
              this.errorHandler(error)
            });
          } else {
            console.log('User is already registered within the venue.2');
            //alert('User is already registered within the venue.2');
          }
        }
      }, (error: any) => {
        console.log('ERROR WHILE FINDING CLOSEST VISIT: ', JSON.stringify(error));
        this.errorHandler(error);
      });
      this.references.alreadyvisited = 1;
    } else {
      console.log('ESTABLISHMENT TOO FAR TO REGISTER A VISIT: ', distance);
      this.reset();
    }

    timer.clearInterval(this.references.intervalId);
    this.references.intervalId = null;
  }

  registerExit(): void {
    console.log('ENTERING EXIT REGISTRATION');
    this.references.alreadyvisited = 0;
    let id = this.visit.id;
    let data = {
      exit: new Date()
    }
    this.visitApi.updateAttributes(id, data).subscribe((visit: Visit) => {
      this.visit = visit;
    },
      error => this.errorHandler(error))
    // (err: Error) => console.log(err.message));
  }

  reset(): void {
    this.visit = new Visit();
    this.establishment = new Establishment();
    this.references = {
      alreadyvisited: 0,
      timer: 0,
      current: null,
      intervalId: null,
      establishments: null
    };
  }

  /**
   * @calculateDistance
   * @param {Position} a Location A
   * @param {Position} b Location B
   * @return {number} Delta distance between location A and B
   **/
  calculateDistance(a: Position, b: Position): number {
    let aLocation = new Location();
    let bLocation = new Location();
    aLocation.latitude = a.latitude || 0;
    aLocation.longitude = a.longitude || 0;
    bLocation.latitude = b.latitude || 0;
    bLocation.longitude = b.longitude || 0;
    return geolocation.distance(bLocation, aLocation);
  }

  ngOnDestroy() {
    //if(this.watchId) {
    //geolocation.clearWatch(this.watchId);
    //}
    timer.clearInterval(this.references.intervalId);
  }

  errorHandler(error: any): void {
    console.log('Visit detection Error', JSON.stringify(error));
    if (error && error.statusCode === 401) {
      this.logoutService.logout();
    }
  }
}
