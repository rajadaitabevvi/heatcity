import { Component, OnInit, AfterViewInit } from '@angular/core';
import { RouterExtensions } from "nativescript-angular/router";
@Component({
    moduleId: module.id,
    templateUrl: 'map-redirect.component.html',
})

export class MapRedirectComponent implements OnInit {
    constructor(private routerExtensions: RouterExtensions) {
        this.routerExtensions.navigate(['/posts'], {
            clearHistory: true,animated:false
        });
    }
    public ngOnInit(){}
}
