import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'LoadingEstablishments',
    templateUrl: 'loading-establishments.component.html'
})
export class LoadingEstablishmentsComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}