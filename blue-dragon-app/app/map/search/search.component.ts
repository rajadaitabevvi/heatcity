import { Component, OnInit, EventEmitter, Input, Output, NgZone, OnChanges, SimpleChange, AfterViewInit } from '@angular/core';
import { Page } from 'ui/page';
import { Location } from '../../shared/sdk/models';
import { Marker } from 'nativescript-google-maps-sdk';
import { LoadingIndicator } from "nativescript-loading-indicator";
import { ListView, ItemEventData } from 'ui/list-view';
import { AnimationCurve } from "ui/enums";
import { Http } from '@angular/http';
import * as googleAnalytics from "nativescript-google-analytics";
import { View } from 'ui/core/view';
import { SearchBar } from "ui/search-bar";
import { EventData } from "data/observable";
import { Button } from "ui/button";
import { isIOS } from "platform";

declare var UITableViewCellSelectionStyle: any;

export interface CriteriaInterface {
    text: string,
    area: string,
    lastArea: string,
    category: string,
    fulltext: string,
}

@Component({
    moduleId: module.id,
    selector: 'SearchComponent',
    templateUrl: 'search.component.html',
    styleUrls: ['search-common.css', 'search.component.css']
})

export class SearchComponent implements AfterViewInit {
    showAreaList: boolean = false;
    fromTap: boolean = false;

    private criteria: CriteriaInterface = {
        text: '',
        area: '',
        category: '',
        lastArea: '',
        fulltext: ''
    };

    private showMarkers: boolean = true;

    private counter: { typed: boolean, number: number } = {
        typed: false,
        number: 0
    };

    private counterInterval: any;

    @Input() private criteriaText: string;
    @Input() private markers: any;
    @Input() private lastArea: string;
    @Input() private invalidArea: boolean;
    @Input() private emptyArea: boolean;
    @Input() private emptySearch: boolean;
    @Input() private lastPosition: any;
    @Output('onType') private onType: EventEmitter<CriteriaInterface> = new EventEmitter<CriteriaInterface>();
    @Output('onSelect') private onSelect: EventEmitter<{ marker: Marker }> = new EventEmitter<{ marker: Marker }>();
    @Output('onExpand') private onExpand: EventEmitter<void> = new EventEmitter<void>();
    @Output('onBack') private onBack: EventEmitter<void> = new EventEmitter<void>();
    @Output('onNearBy') private onNearBy: EventEmitter<void> = new EventEmitter<void>();

    /* @type {{ name: string, icon: string }[]} */
    private categories: { name: string, icon: string }[] = [
        { name: 'Coffee', icon: '•' },
        { name: 'Breakfast', icon: '¢' },
        { name: 'Dinner', icon: '§' },
        { name: 'Lunch', icon: '£' },
        { name: 'Nigth Club', icon: 'ß' },

    ];

    /* @type {number} */
    private height: number;

    public area: string;

    constructor(
        private http: Http,
        private page: Page, private zone: NgZone
    ) {
        googleAnalytics.logView('search');
    }

    ngOnInit() {
        console.error("Search called!!");
        this.getLocationName();
        if (this.invalidArea) {
            this.showMarkers = false;
        }
    }

    getLocationName(): void {
        if (
            !(this.lastPosition &&
                this.lastPosition.latitude &&
                this.lastPosition.longitude)
        ) {
            return;
        }
        let city: string;
        this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${this.lastPosition.latitude},${this.lastPosition.longitude}&key=AIzaSyAaGsoyNTNWIgJJwCUn9sagg3bvyvQbRfc`)
            .subscribe(
            (res: any) => {
                let result = res.json();
                if (result.results && Array.isArray(result.results) && result.results.length > 0) {
                    result.results[0].address_components.forEach((addressComponent: any) => {
                        addressComponent.types.forEach((type: string) => {
                            if (type == "locality") {
                                city = addressComponent.long_name;
                            }
                        });
                    });
                }
                this.criteria.area = city;
                this.area = city;
            },
            (err: Error) => {
                console.log('error getLocation: ', err.name);
            }
            )

    }

    swipe(data: any): void {
        console.log('swipe');
        this.dismissKeyboard();
    }

    ngAfterViewInit(): void {
        let size = this.page.getActualSize();
        let search: any = this.page.getViewById('search');
        console.log('before', search ? 'yes' : 'no', JSON.stringify(size));
        if (search) {
            search.translateY = size.height;
            search.animate({
                translate: { x: 0, y: 0 },
                duration: 500,
                curve: AnimationCurve.easeOut
            }).then(() => {
                this.focusSearchInput();
            })
        }
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['criteriaText']) {
            if (changes['criteriaText'].currentValue != '') {
                this.criteria.text = changes['criteriaText'].currentValue;
            }
        }
    }

    focusSearchInput(): void {
        let searchTextField = <View>this.page.getViewById('searchInput');
        searchTextField.focus();
    }

    search() {
        console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++ search 666");
        console.log('2--SEARCH - Criteria: ', JSON.stringify(this.criteria));
        if (!this.criteria.area && this.lastArea != '') {
            this.criteria.area = this.lastArea;
        }
        console.log('2--SEARCH - Criteria: ', JSON.stringify(this.criteria));
        this.zone.run(() => this.onType.next(this.criteria));
    }

    nearBy() {
        console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++ nearBy 444");
        this.area = '';
        this.criteria.area = '';
        this.lastArea = '';
        if (this.invalidArea) {
            this.invalidArea = false;
            this.showMarkers = true;
        }
        if (this.emptyArea) {
            this.emptyArea = false;
            this.showMarkers = true;
        }
        this.onNearBy.next();
        this.search();
    }

    handleLocationSearch() {
        console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++handleLocationSearch 222");
        this.showAreaList = false;
        console.log('1--Criteria', JSON.stringify(this.criteria))
        if (this.criteria.area == '' && this.lastArea == '') {
            this.nearBy();
        } else {
            this.search();
        }
    }

    onEstablishmentSearchTapped() {
        this.hideAreaList();
    }

    dismissKeyboard() {
        let searchTextField: any = this.page.getViewById('searchInput');
        let areaTextField: any = this.page.getViewById('areaInput');
        if (searchTextField) {
            searchTextField.dismissSoftInput();
        }
        if (areaTextField) {
            areaTextField.dismissSoftInput();
        }
    }

    onModelChange(e) {
        console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++onModelChange 111");
        /*
        if (this.fromTap) {
            this.fromTap = false;
            console.log('2--model chaneh', e);
            return;
        }
        */
        this.criteria.area = e || '';
        if (e != '' && e != this.lastArea) {
            this.lastArea = '';
        }

        if (this.criteria.area == '') this.showAreaList = false;
    }

    hideAreaList() {
        this.showAreaList = false;
    }

    onAreaSelected(cityName: string): void {
        this.fromTap = true;
        console.log('1--cityNAme', cityName);
        if (cityName === '') {
            this.criteria.lastArea = '';
            this.lastArea = '';
        }
        this.criteria.area = cityName;
        this.area = cityName;
        this.handleLocationSearch();
    }

    /**
     * @method startCounter
     **/
    startCounter() {
        if (!this.counterInterval) {
            this.counterInterval = setInterval(() => {
                if (this.counter.typed && this.counter.number < 4) {
                    this.counter.number += 1;
                } else {
                    this.counter.typed = false;
                    this.search();
                    clearInterval(this.counterInterval);
                    this.counterInterval = null;
                }
            }, 333);
        }
    }

    onSearchCriteriaChanged() {
        console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++ onSearchCriteriaChanged 555");
        this.counter.number = 0;
        this.counter.typed = true;
        if (this.criteria.text) {

            this.startCounter();
        }
    }

    onSearchTextboxChanged() {
        console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++ onSearchTextboxChanged 777");
        this.criteria.text = this.criteria.fulltext.split(/,(.+)/)[0];
        this.criteria.area = this.criteria.fulltext.split(/,(.+)/)[1];
        console.log(JSON.stringify(this.criteria));

        this.counter.number = 0;
        this.counter.typed = true;

        this.startCounter();

    }

    onTapCategory(category: EventData) {
        console.log('TAP CATEGORY DETECTED: ');

        let button = <Button>category.object;

        let drinkBtn = <Button>this.page.getViewById('Drinks');
        let dinnerBtn = <Button>this.page.getViewById('Dinner');
        let danceBtn = <Button>this.page.getViewById('Dance');

        drinkBtn.backgroundColor = "transparent";
        dinnerBtn.backgroundColor = "transparent";
        danceBtn.backgroundColor = "transparent";

        var drinkClass = drinkBtn.className.split(' ');
        var drinkIndex = drinkClass.indexOf('active')
        if (drinkIndex > -1) {
            drinkClass.splice(drinkIndex, 1);
        }
        drinkBtn.className = drinkClass.join(' ');

        var dinnerClass = dinnerBtn.className.split(' ');
        var dinnerIndex = dinnerClass.indexOf('active')
        if (dinnerIndex > -1) {
            dinnerClass.splice(dinnerIndex, 1);
        }
        dinnerBtn.className = dinnerClass.join(' ');

        var danceClass = danceBtn.className.split(' ');
        var danceIndex = danceClass.indexOf('active')
        if (danceIndex > -1) {
            danceClass.splice(danceIndex, 1);
        }
        danceBtn.className = danceClass.join(' ');
        if (button.id == this.criteria.category) {
            this.criteria.category = '';

            var classes = button.className.split(' ');
            var index = classes.indexOf('active')
            if (index > -1) {
                classes.splice(index, 1);
            }
            button.className = classes.join(' ');

            if (button.id == "Drinks") {
                drinkBtn.backgroundColor = "transparent";
            } else if (button.id == "Dinner") {
                dinnerBtn.backgroundColor = "transparent";
            } else if (button.id == "Dance") {
                danceBtn.backgroundColor = "transparent";
            }
        } else {
            this.criteria.category = button.id;
            var classes = button.className.split(' ');
            var index = classes.indexOf('active')
            if (index > -1) {
                classes.splice(index, 1);
            }
            classes.push("active");
            button.className = classes.join(' ');

            if (button.id == "Drinks") {
                drinkBtn.backgroundColor = "#fc0682";
            } else if (button.id == "Dinner") {
                dinnerBtn.backgroundColor = "#fc0682";
            } else if (button.id == "Dance") {
                danceBtn.backgroundColor = "#fc0682";
            }
        }

        this.onType.next(this.criteria);
    }

    onTapItem(marker: Marker) {
        this.dismissKeyboard();
        this.onExpand.next()
        this.onSelect.next({ marker: marker });
    }

    addLastArea() {
        if (this.lastArea != '') {
            this.criteria.area = this.lastArea;
            this.area = this.lastArea;
        }
        this.showAreaList = true;
    }

    templateSelector = (item: any, index: number, items: any[]) => {
        return 'SEARCHITEM';
    }

    back(): void {
        this.dismissKeyboard();
        let size = this.page.getActualSize();
        let search: any = this.page.getViewById('search');
        if (search) {
            search.animate({
                translate: { x: 0, y: size.height },
                duration: 500,
                curve: AnimationCurve.easeIn
            }).then(() => {
                this.onBack.next()
            })
        }
    }

    public listViewItemLoading(args): void {
        if (isIOS) {
            args.ios.selectionStyle = UITableViewCellSelectionStyle.UITableViewCellSelectionStyleNone;
        }
    }
}
