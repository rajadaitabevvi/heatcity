import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth-guard.service';
import { MapComponent } from './map.component';

const mapRoutes: Routes = [
    {path: 'map', component: MapComponent, canActivate: [AuthGuard]}
];

export const mapRouting: ModuleWithProviders = RouterModule.forChild(mapRoutes);
