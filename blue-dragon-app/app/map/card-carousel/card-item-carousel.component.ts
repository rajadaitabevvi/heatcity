import { Component, OnInit, Input } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'CardItemCarousel',
  templateUrl: 'card-item-carousel.component.html',
  styleUrls: ['card-item-carousel.component.css']
})

export class CardItemCarousel implements OnInit {

  @Input('item') item: any;

  constructor() { }

  ngOnInit() { }
}