import { Component, Input, Output, OnChanges, OnDestroy, EventEmitter, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { RealTime, VisitApi, AccountApi } from '../../shared/sdk/services';
import { Subscription } from 'rxjs/Subscription';
import { Visit, Establishment, FireLoopRef, LoopBackFilter } from '../../shared/sdk/models';
import { DistancesInterface } from '../map.component';
import { RouterExtensions } from "nativescript-angular/router";
import * as moment from 'moment';
import { isIOS } from "platform";
import * as application from "application";
import { Page } from 'ui/page';
import { ListView } from 'ui/list-view';
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as googleAnalytics from "nativescript-google-analytics";
import { HistoryService } from './history.service';
import { LogoutService } from '../../shared/logout.service';
import { LoadingIndicator } from "nativescript-loading-indicator";

declare var city: any;

@Component({
  moduleId: module.id,
  selector: 'HistoryComponent',
  templateUrl: 'history.component.html',
  styleUrls: ['history.component.css']
})

export class HistoryComponent implements OnDestroy {

  private current: { visit?: Visit, index:  number } = {  index: 0 };

  /** @type { mode: string } */
  private history: { mode: string } = { mode: 'list' };

  private subscriptions: Array<Subscription> = [];

  private title: string;
  
  private isCurrentUser: boolean;

  private visits: Visit[];

  private loader: {
        instance?: LoadingIndicator,
        options: any,
        loading: boolean
    } = {
        loading: false,
        options: {
            message: 'Loading...',
            progress: 0,
            android: {
                indeterminate: true,
                cancelable: false,
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            },
            ios: {
                square: false,
                margin: 10,
                dimBackground: true,
                color: "#fff",
                mode: 'MBProgressHUDModeText'// see iOS specific options below
            }
        },
        instance: new LoadingIndicator()
    };

  @Input('selectedDistance') selectedDistance: number = 0;
  @Input('accountId')accountId: string;
  @Input('visitId')visitId: string;
  @Output('onBack') onBack: EventEmitter<string> = new EventEmitter<string>();
  @Output('onExpand') onExpand: EventEmitter<null> = new EventEmitter<null>();
  @Output('onVisitSelect') onVisitSelect: EventEmitter<Visit> = new EventEmitter<Visit>();
  @Output('confirm') onConfirm: EventEmitter<Visit> = new EventEmitter<Visit>();
  @Output('reject') onReject: EventEmitter<Visit> = new EventEmitter<Visit>();
  @Output('onVisits') onVists: EventEmitter<Visit[]> = new EventEmitter<Visit[]>();

  /**
   * @method constructor
   */
  constructor(
      //private realTime: RealTime,
      private visitApi: VisitApi,
      private accountApi: AccountApi,
      private logoutService: LogoutService,
      private router: Router,
      private zone: NgZone,
      private routerExtensions: RouterExtensions,
      private page: Page,
      private historyService: HistoryService
  ) {
    /**this.realTime.onReady().subscribe(() => {
        this.visitApi = this.realTime.FireLoop.ref<Visit>(Visit);
    });**/
    googleAnalytics.logView('history');

    if(!isIOS) {
        application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
            if (this.router.isActive("/map", false)) {
                data.cancel = true;
                if(this.history.mode == 'map') {
                    this.zone.run(() => {
                        this.history.mode = 'list';
                    })
                } else {
                  this.zone.run(() => {
                      this.onBack.next();
                  })
                }
                console.log('going back in history!');
            }
        });
    }
    this.subscriptions.push(this.historyService.onRefreshHistory().subscribe(() => {
      this.history.mode = 'list';
      this.zone.run(() => {
        this.getVisits()
      });
    }));
    this.subscriptions.push(this.historyService.onGetMarkers().subscribe(
      () => {
        this.getVisits()
      }
    ))
  }

  ngOnInit() {
      this.getAccountName(this.accountId);
      this.isCurrentUser = this.isCurrent();
      this.getVisits();
  }

  getVisits() {
    let filter: LoopBackFilter = {
        where: {
          validity: { gt: this.isCurrentUser ? 49 : 99 },
          accountId: this.accountId || this.accountApi.getCurrentId(),

        },
        limit: 50,
        fields: [
          'id',
          'validity',
          'locationId',
          'entry',
          'exit',
          'geoLocation',
          'createdAt'
        ],
        order: 'entry DESC',
        include: {
            location: {
                relation: 'establishment',
                scope: {
                    fields: [
                        'id',
                        'name',
                        'description',
                        'geoLocation',
                        'address',
                        'categories',
                        'locationId'
                    ]
                }
          }
        }
    };
    this.loader.instance.show(this.loader.options);
    this.subscriptions.push(this.visitApi.find(filter).subscribe(
      (visits: Visit[]) => {
        this.onVists.next(visits);
        this.setCurrentVisit();
        this.zone.run(() => {
          this.visits = visits && visits.length ? visits : [];
          this.visits = this.visits.slice();
          this.loader.instance.hide();
        })
        if(this.visitId) {
          this.scrollToVisit();
        }
      },
      (err: Error) => {
        this.loader.instance.hide();
        this.errorHandler(err);
      }
    ))
  }

  scrollToVisit() {
    let listview: ListView = this.page.getViewById<ListView>('history-list');
    if(listview && this.visits && this.visits.length) {
      let index = 0;
      this.visits.forEach((visit: Visit, i: number) => {
        if(visit.id == this.visitId) {
          index = i;
        }
      })
      setTimeout(() => {
        listview.scrollToIndex(index);
      }, 100)
    }
  }

  setCurrentVisit() {
    if (Array.isArray(this.visits) && this.visits.length > 0 && !this.current.visit) {
      this.current.index = 0;
      this.current.visit = this.visits[this.current.index];
      console.log('YO YO YO', JSON.stringify(this.current));
      this.onVisitSelect.next(this.current.visit);
    }
  }

  visitSelect(visit: Visit) {
    this.onVisitSelect.next(visit);
    this.onBack.next();
  }

  goToEstablishment(visit: Visit) {
    this.routerExtensions.navigate(['/establishment/', visit.location.establishment.id]);
  }

  onListViewLoaded(event) {
      if (event.object.android) {
          event.object.android.setId(city.heat.RedDragon.R.id.my_list_view);
      }
  }

  getAccountName(accountId) {
    let id = this.accountApi.getCurrentId();
    if(accountId && id != accountId) {
      this.subscriptions.push(this.accountApi.findById(accountId).subscribe(
        (account: any) => {
          this.title = (account.firstName || account.displayName)+"'s History"
        },
        error => this.errorHandler(error)
      ));
    } else {
      this.title = 'My History';
    }
  }

  isCurrent():boolean {
    let id = this.accountApi.getCurrentId();
    if(this.accountId) {
      return (id == this.accountId);
    }
    return true;
  }

  back(): void {
    this.onBack.next(this.history.mode);
  }

  templateSelector = (item: any, index: number, items: any[]) => {
    return 'HISTORY';
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
    this.loader.instance = null;
  }
  
  errorHandler(error: any): void {
    console.log('History Error', JSON.stringify(error));
    if (error && error.statusCode === 401) {
      this.logoutService.logout();
    }
  }
}