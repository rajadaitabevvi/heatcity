import { NgModule } from '@angular/core';
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { MapComponent }   from './map.component';
import { LoadingEstablishmentsComponent } from './loading-establishments/loading-establishments.component';
import { mapRouting } from './map.routing';
import { SharedModule } from '../shared/shared.module';
import { StorageNative } from '../shared/sdk/storage/storage.native';

import { SearchComponent } from './search/search.component';
import { AreaListComponent } from './area-list/area-list.component';
// import { FormatDistancePipe } from '../shared/format.pipe';
import { VisitDetectionComponent } from './visit-detection/visit.detection.component';
import { HistoryComponent } from './history/history.component';
import { MenuComponent } from './menu/menu.component';
import { RectifyVisitComponent } from './rectify-visit/rectify-visit.component';

import { AppNotificationsModule } from '../app-notifications/app-notifications.module';
import { FriendsListModule } from '../friends-list/friends-list.module';
import { NotificationsModule } from '../notifications/notifications.module';
import { CardCarousel } from './card-carousel/card-carousel.component';
import { CardItemCarousel } from './card-carousel/card-item-carousel.component';
import { ShareModule } from '../share/share.module';

import { SlideComponent } from './slides/slide.component';
import { SlidesComponent } from './slides/slides.component';
import { HistoryService } from './history/history.service';

import { NativeScriptUISideDrawerModule } from "nativescript-pro-ui/sidedrawer/angular";

@NgModule({
    imports: [
        NativeScriptModule,
        NativeScriptFormsModule,
        SharedModule,
        mapRouting,
        AppNotificationsModule,
        FriendsListModule,
        NotificationsModule,
        ShareModule,
        NativeScriptUISideDrawerModule
    ],
    exports: [
        VisitDetectionComponent
    ],
    declarations: [
        MapComponent,
        LoadingEstablishmentsComponent,
      //  FormatDistancePipe,
        SearchComponent,
        AreaListComponent,
        VisitDetectionComponent,
        HistoryComponent,
        MenuComponent,
        CardCarousel,
        CardItemCarousel,
        RectifyVisitComponent,
        SlidesComponent,
        SlideComponent
    ],
    providers: [StorageNative, HistoryService]
})
export class MapModule { }
