import { Component, OnInit, Input, Output, EventEmitter, SimpleChange, OnChanges, OnDestroy } from '@angular/core';
import { LoopBackFilter, Area } from '../../shared/sdk/models';
import { AreaApi } from '../../shared/sdk/services';
import { BASE_URL, API_VERSION } from '../../shared/base.api';
import { LoopBackConfig } from '../../shared/sdk';
import { Subscription } from 'rxjs/Subscription';
import { isIOS } from "platform";

declare var UITableViewCellSelectionStyle: any;

@Component({
    moduleId: module.id,
    selector: 'area-list',
    templateUrl: 'area-list.component.html',
    styleUrls: ['area-list-common.css']
})

export class AreaListComponent implements OnInit, OnChanges, OnDestroy {
    areas: Array<Area> = [];
    nearBy: Area = new Area();
    subscriptions: Array<Subscription> = [];

    private counter: { typed: boolean, number: number} = {
        typed: false,
        number: 0
    };

    private counterInterval: any;

    templateSelector = (item: Area, index: number, items: any[]) => {
        return 'AREA'
    }

    @Input() private area: string;
    @Input() private showList: boolean;
    @Output('onAreaSelected') private onAreaSelected : EventEmitter<string> = new EventEmitter<string>();
    @Output('onDismissKeyboard') private onDismissKeyboard : EventEmitter<void> = new EventEmitter<void>();

    constructor(
        private areaApi: AreaApi
    ) {
        LoopBackConfig.setBaseURL(BASE_URL);
        LoopBackConfig.setApiVersion(API_VERSION);
    }

    ngOnInit() {
        this.getAvailableAreas();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    getAvailableAreas(): void {
        this.nearBy.name = '';
        let filter: LoopBackFilter = {
            limit: 30,
            fields: ['id', 'name', 'updates'],
            order: 'updates DESC'
        }
        if (this.area) {
            filter.where = {
                name: {
                    'like': '.*' + this.area + '.*',
                    'options': 'i'
                }
            }
        }
        console.log('final filter', JSON.stringify(filter));
        this.subscriptions.push(this.areaApi.find(filter).subscribe(
            (areas: Array<Area>) => {
                this.areas = [this.nearBy, ...areas];
            },
            (err: Error) => {
                console.log('Error: ', err.message);
            }
        ))
    }

    onAreaChanged() {
        this.counter.number = 0;
        this.counter.typed  = true;
        this.startCounter();
    }

    startCounter() {
        if (!this.counterInterval) {
            this.counterInterval = setInterval(() => {
            if (this.counter.typed && this.counter.number < 4) {
                this.counter.number += 1;
            } else {
                this.counter.typed = false;
                this.getAvailableAreas();
                clearInterval(this.counterInterval);
                this.counterInterval = null;
            }
            }, 333);
        }
    }

    ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
        if(changes['area']){
            console.log('3--area changed', changes['area'].currentValue);
            if(changes['area'].currentValue != '') {
                this.area = changes['area'].currentValue;
            }else {
                this.area = null;
            }
            this.onAreaChanged();
        }
        if(changes['showlist']){
            this.showList = changes['showlist'].currentValue;
            if (this.showList) {
                this.getAvailableAreas();
            }
        }
    }

    dismissKeyboard(): void {
        console.log('1--dismiss');
        this.onDismissKeyboard.emit();
    }

    onTapItem(area: Area): void {
        console.log('1--tapped');
        this.onAreaSelected.emit(area.name);
    }

    public listViewItemLoading(args): void {
        if (isIOS) {
            args.ios.selectionStyle = UITableViewCellSelectionStyle.UITableViewCellSelectionStyleNone;
        }
    }
}
