import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Router } from "@angular/router";
import { Http } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { LoopBackConfig } from '../shared/sdk';
import { BASE_URL, API_VERSION } from '../shared/base.api';
import { AccountApi } from '../shared/sdk/services';
import { Account } from '../shared/sdk/models';
import { Accuracy } from "ui/enums";
import { Page }  from "ui/page";
import { topmost } from "ui/frame";
import { isIOS } from "platform";
import * as appSettings from "application-settings";
import * as googleAnalytics from "nativescript-google-analytics";

import geolocation = require('nativescript-geolocation');
import { Ratings } from "nativescript-ratings";

@Component({
    selector: "AccountSetupComponent",
    templateUrl: "account-setup/account-setup.component.html",
    styleUrls: ['account-setup/account-setup-common.css', 'account-setup/account-setup.component.css']
})
export class AccountSetupComponent implements OnInit, OnDestroy {
    /** @type {Subscription[]} **/
    private subscriptions: Subscription[] = new Array<Subscription>();

    /** @type {Account} **/
    public account: Account;

    /** @type {number} **/
    public step: number = 0;

    private ratings: Ratings

    /**
     * @method constructor
     * @param {Account} account Fireloop Account Service
     **/
    constructor(
        private accountApi: AccountApi,
        private http: Http,
        private page: Page,
        private zone: NgZone,
        private router: Router,
    ) {
        LoopBackConfig.setBaseURL(BASE_URL);
        LoopBackConfig.setApiVersion(API_VERSION);
        googleAnalytics.logView('account-setup');

        if(isIOS) {
            topmost().ios.controller.navigationBar.barStyle = 1;
        }
    }

    /**
     * @method ngOnInit
     **/
    ngOnInit() {
        let id  = this.accountApi.getCurrentId();
        this.page.actionBarHidden = true;
        this.subscriptions.push(
            this.accountApi.findById(id).subscribe(
                (account: Account) => {
                    this.zone.run(() => {
                        this.account = account;
                        this.step = 1;
                    });
                },
                (err: Error) => console.log(err)
            )
        );
    }
    /**
     * @method nextScreen
     **/
    nextScreen(): void {
        if (this.step === 2) {
            this.router.navigate(['/map'])
        }else {
            this.step += 1;
        }
    }
    /**
     * @method currentLocation
     **/
    currentLocation() {
        this.account.settings = {
            locationServices: true,
            shareLocation: true
        };
        geolocation.getCurrentLocation({
            desiredAccuracy: Accuracy.high
        }).then((location: geolocation.Location) => {
            this.account.location = {
                lat: location.latitude,
                lng: location.longitude
            };
            this.getTownAndCountryName();
        });
    }

    /**
     * @method getTownAndCountryName
     **/
    getTownAndCountryName(): void {
        this.subscriptions.push(
            this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${this.account.location.lat},${this.account.location.lng}&key=AIzaSyAaGsoyNTNWIgJJwCUn9sagg3bvyvQbRfc`)
                .subscribe(
                    (res: any) => {
                        let result = res.json();
                        if(result.results && Array.isArray(result.results) && result.results.length > 0) {
                            result.results[0].address_components.forEach((addressComponent: any) => {
                                addressComponent.types.forEach((type: string) => {
                                    if(type == "administrative_area_level_1") {
                                        this.account.hometown = addressComponent.long_name;
                                    }
                                    if(type == "country") {
                                        this.account.homecountry = addressComponent.long_name;
                                    }
                                });
                            });
                        }
                        this.zone.run(() => {
                            this.step = 2;
                        })
                    },  
                    (err: Error) => {
                        console.log('error getTownAndCountryName: ', err.name);
                    }
                )
        );
    }

    /**
     * @method updateAccount
     **/
    updateAccount() {
        this.enableAccountNotifications();
        this.subscriptions.push(
            this.accountApi.updateAttributes(this.account.id, this.account)
                .subscribe(
                    (account: Account) => {
                        this.zone.run(() => {
                            this.router.navigate(['/map'])
                        });
                    }
                )
        );
    }

    enableAccountNotifications(): void {
        if (
            this.account.settings &&
            (this.account.settings.hasOwnProperty('locationServices') ||
            this.account.settings.hasOwnProperty('shareLocation'))
        ) {
            this.account.settings.notifications = true;
        }else {
            this.account.settings = {
                notifications: true
            }
        }
    }

    /**
     * @method rateApp
     **/
    rateApp() {
        appSettings.setNumber('bluedragon-app-rating', 0);
        this.ratings = new Ratings({
            id: 'bluedragon-app-rating',
            text: 'If you like this app, please take a moment to leave a positive rating.',
            title: 'What do you think?',
            agreeButtonText: "Rate Now",
            remindButtonText: "Remind Me Later",
            declineButtonText: "No Thanks",
            showOnCount: 1,
            androidPackageId: "city.heat.RedDragon",
            iTunesAppId: "1049602225"
        });
        this.ratings.init();
        this.router.navigate(['/map']).then(() => {
            this.ratings.prompt();
        }, () => {
            console.log('navigate fail');
        })
    }

    /**
     * @method ngOnDestroy
     **/
    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
    }
}