import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth-guard.service';
import { AccountSetupComponent } from './account-setup.component';

const accountSetupRoutes: Routes = [
    {path: 'account-setup', component: AccountSetupComponent, canActivate: [AuthGuard]}
];

export const accountSetupRouting: ModuleWithProviders = RouterModule.forChild(accountSetupRoutes);