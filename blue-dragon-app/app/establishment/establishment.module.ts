import { NgModule, NgModuleFactoryLoader } from '@angular/core';
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { ModalDialogService } from "nativescript-angular/modal-dialog";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptUIListViewModule } from "nativescript-pro-ui/listview/angular";

import { establishmentProfileRouting } from './establishment.routing';
import { EstablishmentComponent } from './establishment.component';
import { SharedModule } from '../shared/shared.module';
import { CHART_DIRECTIVES } from 'nativescript-pro-ui/chart/angular';
import { StorageNative } from '../shared/sdk/storage/storage.native';
import { AppNotificationsModule } from '../app-notifications/app-notifications.module';
import { ShareModule } from '../share/share.module';

@NgModule({
    imports: [
        NativeScriptUIListViewModule,
        SharedModule,
        establishmentProfileRouting,
        NativeScriptModule,
        NativeScriptFormsModule,
        AppNotificationsModule,
        ShareModule
    ],
 declarations: [
        EstablishmentComponent
    ],
    providers: [
        ModalDialogService,
        StorageNative
    ]
})
export class EstablishmentModule { }
