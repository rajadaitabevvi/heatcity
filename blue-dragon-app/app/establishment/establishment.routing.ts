import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth-guard.service';

// Internal import
import { EstablishmentComponent } from './establishment.component';

const establishmentProfileRoutes: Routes = [
    { path: 'establishment/:id', component: EstablishmentComponent, canActivate: [AuthGuard] }
];

export const establishmentProfileRouting: ModuleWithProviders = RouterModule.forChild(establishmentProfileRoutes);
