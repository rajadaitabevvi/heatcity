import * as moment from 'moment';

// Internal import
import { LoopBackFilter } from '../../shared/sdk/models';

export class EstablishmentHelper {

    public getFilter(locationId:any): LoopBackFilter {
        let today = moment().toDate()
        let sevenDaysBefore = moment().subtract(700, 'days').toDate();

        return {
            where: {
                and: [
                    { locationId: locationId }//,
                    /*{ validity: 100 },
                    {
                        createdAt: {
                            between: [
                                sevenDaysBefore,
                                today
                            ]
                        }
                    }*/
                ]
            },
            include: ['account'],
            order: 'createdAt DESC'//,
           // limit: 5
        }
    }
}