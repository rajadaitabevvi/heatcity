# How install Nativescript UI PRO.

Make sure to delete the lib/iOS/TelerikUI.framework file at the root level of your app if you have a previous version of Telerik UI for NativeScript.

  - You can install it in your {N} application by using __tns plugin add <path-to-tgz>.__
  - The tgz file is located in: __bluedragon/packages__
  - After installing the package, you can access the components in it by using the following code:

```sh
import listViewModule = require("nativescript-telerik-ui-pro/listview");
import drawerModule = require("nativescript-telerik-ui-pro/sidedrawer");
import calendarModule = require("nativescript-telerik-ui-pro/calendar");
import chartModule = require("nativescript-telerik-ui-pro/chart");
import dataFormModule = require("nativescript-telerik-ui-pro/dataform");
```